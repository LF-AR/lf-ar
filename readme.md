# Little Fighter - Augmented Reality

COMP7506 Smart Phone Apps Development

Group Project 

## Getting Started

## Prerequisite

iOS device with iOS ver.12

MacOS version Catalina 

## Packages versioning 

### Unity 

* Ads - 2.0.8
* Analytics Library - 3.3.5
* AR Foundation - preview.6 - 2.2.0
* ARKit XR Plugin - preview.6 - 2.2.0
* Cloud Firestore for Firebase - 6.14.0
* Firebase Authentication - 6.14.0
* In App Purchasing - 2.0.6
* Package Manager UI - 2.1.2
* TextMesh Pro - 2.0.1
* Unity Collaborate - 1.2.16
* Unity Timeline - 1.0.0

### Node.js 

* @types/cors - ^2.8.6
* body-parser - ^1.19.0
* cors - ^2.8.5
* express - ^4.17.1
* firebase-admin - ^8.6.0
* firebase-functions - ^3.3.0
* request-promise - ^4.2.5

### Installing

#### Unity
1.	Add Unity application to our firebase project -> Register as iOS app
2.	Bundle ID com.unity.xxxxx
3.	Register your application
4.	Download GoogleService-Info.plist
5.	Add the downloaded file to the unity /Asset directory -> Press OK
6.	File -> Build settings 
7.	Switch platform to iOS
8.	Press Build and select your desired directory 
9.	Go to your build directory -> open Unity-iPhone.xcworkspace
10. Go to Signing and Capabilities
11. Select personal team in the team section
12. Connect your iOS device to your Mac though a USB cable
13. Select the build target Unity-iPhone
14. Press Run
15. XCode will prompt a warning which requires you to allow this iOS device to trust this developer
16. Go to settings on your iOS device -> General -> Device Management 
17. Select your own Apple ID -> trust
18. Press Run on your Xcode again

#### Backend (Node.js)

```
cd {project_dir}/Backend/functions

npm install
```

### And coding style tests

For backend functions, TypeScript linting is used 

```
cd {project_dir}/Backend/functions

node lint
```

## Deployment

API documentation is deployed to the Firebase hosting server

```
cd {project_dir}/Backend/functions

// generate webpage for the API documentation
node apidoc 

// deploy to firebase hosting server
node apidocDeploy
```

## Built With

* [Unity](https://unity3d.com/get-unity/download) - The development software used (ver. 2019.1.2f1)
* [Xcode](https://apps.apple.com/hk/app/xcode/id497799835?mt=12) - The installation software for iOS (ver. 11.4.1)
* [Firebase CLI](https://firebase.google.com/docs/cli) - The command line tools used to deploy backend functions
* [Node.js](https://nodejs.org/en/download/) - Dependency Management
* [Express.js](https://expressjs.com/zh-tw/) - Framework used for backend functions

## Authors

* **Chung Wing Nam** - 3035197096

* **Law Chi Ho** - 3035199044

* **Li Yui Sing** - 3035198521

## API documentation

Visit the hosting website for details. (https://lf-ar-c0dfb.firebaseapp.com/)

## Project Location 

https://gitlab.com/LF-AR/lf-ar
