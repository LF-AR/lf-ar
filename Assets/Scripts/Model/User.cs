using System.Collections.Generic;
using Firebase.Firestore;

[FirestoreData]
public class User
{
    [FirestoreProperty]
    public string id { get; set; }
    [FirestoreProperty]
    public string name { get; set; }
    [FirestoreProperty]
    public int score { get; set; }
    [FirestoreProperty]
    public int money { get; set; }
    [FirestoreProperty]
    public string status { get; set; }
    [FirestoreProperty]
    public List<UserHero> heros { get; set; }

    User() { }

    User(string id = "", string name = "", int score = 0, int money = 1000, List<UserHero> heros = default)
    {
        this.id = id;
        this.name = name;
        this.score = score;
        this.money = money;
        this.heros = heros;
    }

    public static User fromDAO(string id, DocumentSnapshot snapshot)
    {
        User user = snapshot.ConvertTo<User>();
        // List<UserHero> herosArray = new List<UserHero>();

        // foreach (Dictionary<string, object> userHero in herosObj)
        // {
        //     herosArray.Add(UserHero.fromDAO((string)userHero["id"], userHero));
        // }

        return user;
        // new User(id, (string)data["name"], (int)data["score"], (int)data["money"], herosArray);
    }
}

[FirestoreData]
public class UserHero
{
    [FirestoreProperty]
    public string id { get; set; }
    [FirestoreProperty]
    public DocumentReference heroRef { get; set; }
    [FirestoreProperty]
    public int level { get; set; }
    [FirestoreProperty]
    public int exp { get; set; }

    public UserHero() { }

    public UserHero(string id = "", DocumentReference heroRef = default, int level = 1, int exp = 0)
    {
        this.id = id;
        this.heroRef = heroRef;
        this.level = level;
        this.exp = exp;
    }

    public static UserHero fromDAO(string id, Dictionary<string, object> data)
    {
        return new UserHero(id, (DocumentReference)data["hero_ref"], (int)data["level"], (int)data["exp"]);
    }
}

