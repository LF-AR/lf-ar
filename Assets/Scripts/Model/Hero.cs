using SimpleJSON;
using Firebase.Firestore;

[FirestoreData]
public class Hero
{
	public string id { get; set; }
	[FirestoreProperty]
	public string name { get; set; }
	[FirestoreProperty]
	public int health_point { get; set; }
	[FirestoreProperty]
	public int magic_point { get; set; }
	[FirestoreProperty]
	public int magic_point_recover_speed { get; set; }
	[FirestoreProperty]
	public int move_speed { get; set; }
	[FirestoreProperty]
	public int attack_damage { get; set; }
	[FirestoreProperty]
	public string description { get; set; }
	[FirestoreProperty]
	public int level_factor { get; set; }
	[FirestoreProperty]
	public string asset_name { get; set; }
	[FirestoreProperty]
	public string type { get; set; }
	[FirestoreProperty]
	public int max_level { get; set; }
	[FirestoreProperty]
	public int max_exp { get; set; }
	[FirestoreProperty]
	public int price { get; set; }
	[FirestoreProperty]
	public FighterColor fighter_color { get; set; }
	[FirestoreProperty]
	public object[] magic_attack_list { get; set; }

	Hero() { }
	Hero(
		string id = "",
		string name = "",
		int health_point = 100,
		int magic_point = 100,
		int magic_point_recover_speed = 3,
		int move_speed = 10,
		int attack_damage = 25,
		string description = "",
		int level_factor = 5,
		string asset_name = "",
		string type = "",
		int max_level = 10,
		int max_exp = 100,
		int price = 100,
		FighterColor fighter_color = FighterColor.Red,
		object[] magic_attack_list = default
	)
	{
		this.id = id;
		this.name = name;
		this.health_point = health_point;
		this.magic_point = magic_point;
		this.magic_point_recover_speed = magic_point_recover_speed;
		this.move_speed = move_speed;
		this.attack_damage = attack_damage;
		this.description = description;
		this.level_factor = level_factor;
		this.asset_name = asset_name;
		this.type = type;
		this.max_level = max_level;
		this.max_exp = max_exp;
		this.price = price;
		this.fighter_color = fighter_color;
		this.magic_attack_list = magic_attack_list;
	}

	public static Hero fromDAO(
		string id,
		string json,
		object[] magic_attack_list
	)
	{
		dynamic data = JSON.Parse(json);
		return new Hero(
			id,
			data.name,
			data.health_point,
			data.magic_point,
			data.magic_point_recover_speed,
			data.move_speed,
			data.attack_damage,
			data.description,
			data.level_factor,
			data.asset_name,
			data.type,
			data.max_level,
			data.max_exp,
			data.price,
			data.fighter_color,
			magic_attack_list
		);
	}
}