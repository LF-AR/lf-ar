using SimpleJSON;
using Firebase.Firestore;

[FirestoreData]
public class MagicAttack
{
    public string id { get; set; }
    [FirestoreProperty]
    public string name { get; set; }
    [FirestoreProperty]
    public string asset_name { get; set; }
    [FirestoreProperty]
    public int magic_point { get; set; }
    [FirestoreProperty]
    public int damage { get; set; }
    [FirestoreProperty]
    public int duration { get; set; }
    [FirestoreProperty]
    public string attack_effect { get; set; }
    [FirestoreProperty]
    public int attack_effect_duration { get; set; }
    [FirestoreProperty]
    public string magic_spawn_sound { get; set; }
    [FirestoreProperty]
    public string magic_move_sound { get; set; }
    [FirestoreProperty]
    public int magic_move_speed { get; set; }
    [FirestoreProperty]
    public string type { get; set; }
    [FirestoreProperty]
    public int size { get; set; }
    [FirestoreProperty]
    public int speed { get; set; }
    [FirestoreProperty]
    public string source_id { get; set; }

    MagicAttack() { }
    MagicAttack(
        string id = "",
        string name = "",
        string asset_name = "",
        int magic_point = 0,
        int damage = 0,
        int duration = 5,
        string attack_effect = "",
        int attack_effect_duration = 5,
        string magic_spawn_sound = "",
        string magic_move_sound = "",
        int magic_move_speed = 5,
        string type = "",
        int size = 1,
        int speed = 5,
        string source_id = ""
    )
    {
        this.id = id;
        this.name = name;
        this.asset_name = asset_name;
        this.magic_point = magic_point;
        this.damage = damage;
        this.duration = duration;
        this.attack_effect = attack_effect;
        this.attack_effect_duration = attack_effect_duration;
        this.magic_spawn_sound = magic_spawn_sound;
        this.magic_move_sound = magic_move_sound;
        this.magic_move_speed = magic_move_speed;
        this.type = type;
        this.size = size;
        this.speed = speed;
        this.source_id = source_id;
    }

    public static MagicAttack fromDAO(
        string id,
        string json
    )
    {
        dynamic data = JSON.Parse(json);
        return new MagicAttack(
            data.id,
            data.name,
            data.asset_name,
            data.magic_point,
            data.damage,
            data.duration,
            data.attack_effect,
            data.attack_effect_duration,
            data.magic_spawn_sound,
            data.magic_move_sound,
            data.magic_move_speed,
            data.type,
            data.size,
            data.speed,
            data.source_id
        );
    }
}