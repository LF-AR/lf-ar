﻿using Photon.Pun;
using UniRx;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Collections;
using System.Collections.Generic;
using System;
using Unity.Collections;
#if UNITY_IOS
using UnityEngine.XR.ARKit;
#endif
using UnityMultipeerConnectivity;


public class MultipeerWorldMapReceiver : MonoBehaviourPunCallbacks
{
    ARSession aRSession;

    [SerializeField]
    private SceneManagerBattle sceneManagerBattle;

    void Start()
    {
        UnityMCSessionNativeInterface.GetMcSessionNativeInterface()
            .DataReceivedAsObservable()
            .Subscribe(LoadAndApplyWorldMap)
            .AddTo(this);

        aRSession = GameObject.Find("AR Session").GetComponent<ARSession>();


        if (GameManager.GameMode == GameMode.Shared)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                sceneManagerBattle = GameObject.Find("SceneManagerBattleSharedHost").GetComponent<SceneManagerBattleSharedHost>();
            }
            else
            {
                sceneManagerBattle = GameObject.Find("SceneManagerBattleSharedClient").GetComponent<SceneManagerBattleSharedClient>();
            }
        }
        else if (GameManager.GameMode == GameMode.Detached)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                sceneManagerBattle = GameObject.Find("SceneManagerBattleDetachedHost").GetComponent<SceneManagerBattleDetachedHost>();
            }
            else
            {
                sceneManagerBattle = GameObject.Find("SceneManagerBattleDetachedClient").GetComponent<SceneManagerBattleDetachedClient>();
            }
        }




        //RestartARSession();

    }

    void LoadAndApplyWorldMap(byte[] receivedData)
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.Log("LoadAndApplyWorldMap: " + receivedData);
            StartCoroutine(LoadAndApplyWorldMapRoutine(receivedData));
        }
    }


    IEnumerator LoadAndApplyWorldMapRoutine(byte[] receivedData)
    {
        ARKitSessionSubsystem sessionSubsystem = (ARKitSessionSubsystem)aRSession.subsystem;
        if (sessionSubsystem == null)
        {
            Debug.Log("No session subsystem available. Could not load.");
            yield break;
        }
        List<byte> allBytes = new List<byte>(receivedData);

        NativeArray<byte> data = new NativeArray<byte>(allBytes.Count, Allocator.Temp);
        data.CopyFrom(allBytes.ToArray());

        ARWorldMap worldMap;
        if (ARWorldMap.TryDeserialize(data, out worldMap))
            data.Dispose();

        if (worldMap.valid)
        {
            Debug.Log("Deserialized successfully.");
        }
        else
        {
            Debug.LogError("Data is not a valid ARWorldMap.");
            yield break;
        }

        Debug.Log("Reset ARSession");
        sessionSubsystem.Reset();

        Debug.Log("Apply ARWorldMap to current session.");
        sessionSubsystem.ApplyWorldMap(worldMap);

        Debug.Log("ARWorldMap applied. Calling ISceneManagerBattleClient.MapReceived()");
        ((ISceneManagerBattleClient)sceneManagerBattle).MapReceived();
    }
}
