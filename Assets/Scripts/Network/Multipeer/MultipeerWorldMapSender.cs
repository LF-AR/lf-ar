﻿using Photon.Pun;
using UniRx;
using UniRx.Async;
using UnityARKitPluginExtensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Unity.Collections;
using System.Collections;
#if UNITY_IOS
using UnityEngine.XR.ARKit;
#endif
using UnityMultipeerConnectivity;

public class MultipeerWorldMapSender : MonoBehaviourPunCallbacks
{
    ARSession aRSession;

    [SerializeField] Button sendWorldMapButton;

    void Start()
    {
        GameMode gameMode = GameManager.GameMode;
        if (PhotonNetwork.IsMasterClient && gameMode == GameMode.Shared)
        {
            aRSession = GameObject.Find("AR Session").GetComponent<ARSession>();
            sendWorldMapButton.onClick.AddListener(SaveAndSendWorldMapToAllPeers);
            // sendWorldMapButton.OnClickAsObservable()
            // .Subscribe(async _ => await SendCurrentARWorldMapToAllPeersAsync())
            // .AddTo(this);
        }
    }

    void SaveAndSendWorldMapToAllPeers()
    {
#if UNITY_IOS
        StartCoroutine(SendCurrentARWorldMapToAllPeersRoutine());
#endif
    }

    IEnumerator SendCurrentARWorldMapToAllPeersRoutine()
    {
        Debug.Log("World Map Loading");
        // var arSessionNativeInterface = UnityARSessionNativeInterface.GetARSessionNativeInterface();
        // var arWorldMap = await arSessionNativeInterface.GetCurrentWorldMapAsnyc();
        var sessionSubsystem = (ARKitSessionSubsystem)aRSession.subsystem;
        if (sessionSubsystem == null)
        {
            Debug.Log("No session subsystem available. Could not save.");
            yield break;
        }

        var request = sessionSubsystem.GetARWorldMapAsync();

        while (!request.status.IsDone())
            yield return null;

        if (request.status.IsError())
        {
            Debug.Log(string.Format("Session serialization failed with status {0}", request.status));
            yield break;
        }

        var worldMap = request.GetWorldMap();
        request.Dispose();

        SendAndDisposeWorldMap(worldMap);
    }

    void SendAndDisposeWorldMap(ARWorldMap aRWorldMap)
    {
        NativeArray<byte> data = aRWorldMap.Serialize(Allocator.Temp);

        aRWorldMap.Dispose();

        UnityMCSessionNativeInterface.GetMcSessionNativeInterface().SendToAllPeers(data.ToArray());

        data.Dispose();
    }
}
