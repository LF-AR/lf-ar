﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public enum ConnectionState
{
    Initial,
    ConnectingMaster,
    ConnectedMaster,
    ConnectedLobby,
    Joined
}

public class NetworkConnectionManager : MonoBehaviourPunCallbacks
{
    public GameManager gameManager;

    private ConnectionState connectionState;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        connectionState = ConnectionState.Initial;
    }

    // Getter

    public ConnectionState GetConnectionState()
    {
        return connectionState;
    }

    // Getter (end)


    // Connection

    public void ConnectToMaster(string playerName)
    {
        PhotonNetwork.OfflineMode = false;
        PhotonNetwork.NickName = playerName;
        PhotonNetwork.GameVersion = GameManager.GameVersion;
        // REMARKS: maybe useful for 1st stage
        //PhotonNetwork.AutomaticallySyncScene = true;

        // REMARKS:
        // Photon/PhotonUnityNetworking/Resources/PhotonServerSettings
        PhotonNetwork.ConnectUsingSettings();

        connectionState = ConnectionState.ConnectingMaster;
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        connectionState = ConnectionState.ConnectedMaster;

        Debug.Log("Connected To Master");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        connectionState = ConnectionState.Initial;

        // REMARKS: lost connection to PUN
        // while still signed in to firebase
        if (GameManager.User != null)
        {
            FirebaseUtil.SignOut();
        }

        Debug.Log(cause);
    }

    // Conection (end)


    // Lobby

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        connectionState = ConnectionState.ConnectedLobby;

        Debug.Log("Connected To Lobby");
    }

    // Lobby (end)


    // Join Room

    public void JoinRoom(string roomName)
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRoom(roomName);
        }
        else
        {
            Debug.Log("Not connected to PUN!");
        }
    }

    // REMARKS: this is called also after CreateRoom
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        connectionState = ConnectionState.Joined;

        Debug.Log("Master: " + PhotonNetwork.IsMasterClient + " | Player In Room: " + PhotonNetwork.CurrentRoom.PlayerCount + " | RoomName: " + PhotonNetwork.CurrentRoom.Name + " | Region: " + PhotonNetwork.CloudRegion);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);

        Debug.Log(message);
    }

    // Join Room (end)


    // Create Room

    public void CreateRoom(string roomName, RoomOptions roomOptions)
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);
        }
        else
        {
            Debug.Log("Not connected to PUN!");
        }
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);

        Debug.Log(message);
    }

    // Create Room (end)


    // Leave Room

    public void LeaveRoom()
    {
        Debug.Log("Leaving room");

        PhotonNetwork.LeaveRoom();
    }

    // Leave Room (end)
}
