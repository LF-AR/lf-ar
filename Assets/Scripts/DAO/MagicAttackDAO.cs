using System.Threading.Tasks;
using Firebase.Firestore;

public static class MagicAttackDAO
{
    public static DocumentReference getMagicAttackRef(string magicAttackId)
    {
        return DB._db.Collection("MagicAttack").Document(magicAttackId);
    }

    public async static Task<MagicAttack> getMagicAttack(string magicAttackId)
    {
        DocumentSnapshot magicAttackSnapshot = await MagicAttackDAO.getMagicAttackRef(magicAttackId).GetSnapshotAsync();
        return MagicAttackDAO.getMagicAttack(magicAttackSnapshot);
    }

    public static MagicAttack getMagicAttack(DocumentSnapshot magicAttackSnapshot)
    {
        MagicAttack magicAttack = magicAttackSnapshot.ConvertTo<MagicAttack>();
        magicAttack.id = magicAttackSnapshot.Id;
        return magicAttack;
    }
}