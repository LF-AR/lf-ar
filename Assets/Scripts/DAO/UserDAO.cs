using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Firestore;

public static class UserDAO
{
    public static DocumentReference getUserRef(string userId)
    {
        return DB._db.Document("User/" + userId);
    }

    public static CollectionReference getHerosCol(string userId)
    {
        return DB._db.Collection("User/" + userId + "/heros");
    }

    public static DocumentReference getUserHeroRef(string userId, string herosId)
    {
        return DB._db.Document("User/" + userId + "/heros/" + herosId);
    }

    public async static Task<User> getUser(string userId)
    {
        DocumentSnapshot userSnapshot = await UserDAO.getUserRef(userId).GetSnapshotAsync();
        return await UserDAO.getUser(userSnapshot);
    }

    public async static Task<User> getUser(DocumentSnapshot userSnapshot)
    {
        QuerySnapshot herosSnapshot = await UserDAO.getHerosCol(userSnapshot.Id).GetSnapshotAsync();
        User user = userSnapshot.ConvertTo<User>();
        user.id = userSnapshot.Id;
        user.heros = new List<UserHero>();
        foreach (DocumentSnapshot userHeroSnapshot in herosSnapshot.Documents)
        {
            UserHero userHero = userHeroSnapshot.ConvertTo<UserHero>();
            userHero.id = userHeroSnapshot.Id;
            user.heros.Add(userHero);
        }

        return user;
    }

    public async static Task<UserHero> GetUserHero(string userId, string herosId)
    {
        DocumentSnapshot userHeroSnapshot = await UserDAO.getUserHeroRef(userId, herosId).GetSnapshotAsync();
        UserHero userHero = userHeroSnapshot.ConvertTo<UserHero>();
        userHero.id = herosId;
        return userHero;
    }

}
