using System.Threading.Tasks;
using Firebase.Firestore;

public static class HeroDAO
{
    public static DocumentReference getHeroRef(string heroId)
    {
        return DB._db.Document("Hero/" + heroId);
    }

    public async static Task<Hero> getHero(string heroId)
    {
        DocumentSnapshot heroSnapshot = await HeroDAO.getHeroRef(heroId).GetSnapshotAsync();
        return HeroDAO.getHero(heroSnapshot);
    }

    public static Hero getHero(DocumentSnapshot heroSnapshot)
    {
        Hero hero = heroSnapshot.ConvertTo<Hero>();
        hero.id = heroSnapshot.Id;
        return hero;
    }
}