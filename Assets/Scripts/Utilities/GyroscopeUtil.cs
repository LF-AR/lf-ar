﻿using System.Collections;
using UnityEngine;

public class GyroscopeUtil : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        DisableGyroscope();
        gameObject.SetActive(false);
    }

    public void ResetGyroscope()
    {
        Debug.Log("DEBUG: ResetGyroscope()");

        DisableGyroscope();

        // REMARKS: re-enable gyroscope after some time
        // Invoke("EnableGyroscope", 2f);
        Debug.Log("DEBUG: Invoking EnableGyroscope in 2 seconds");
        StartCoroutine(EnableGyroscope(2));
        Debug.Log("DEBUG: Invoked EnableGyroscope in 2 seconds");
    }

    public void DisableGyroscope()
    {
        Debug.Log("DEBUG: DisableGyroscope()");

        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = false;

            Debug.Log("DEBUG: Input.gyro.enabled: " + Input.gyro.enabled);
        }
    }

    IEnumerator EnableGyroscope(float time)
    {
        yield return new WaitForSeconds(time);

        Debug.Log("DEBUG: EnableGyroscope()");

        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;

            Debug.Log("DEBUG: Input.gyro.enabled: " + Input.gyro.enabled);
        }
    }
}
