﻿using System.Collections.Generic;
using Firebase;
using Firebase.Extensions;
using Firebase.Auth;
using Firebase.Firestore;
using UnityEngine;
using System;
using System.Threading.Tasks;

public class FirebaseUtil : MonoBehaviour
{
    public static FirebaseUser firebaseUser;
    public static string token;

    private static FirebaseAuth auth;
    private static FirebaseFirestore db;

    private GameManager gameManager;

    private ListenerRegistration userListener;
    private ListenerRegistration heroListener;
    private ListenerRegistration magicAttackListener;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        gameManager = FindObjectOfType<GameManager>();

        InitializeFirebase();
    }

    private void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

    // Handle initialization of the necessary firebase modules:
    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");

        auth = FirebaseAuth.DefaultInstance;
        db = FirebaseFirestore.DefaultInstance;

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }


    // Auth

    // Track state changes of the auth object.
    private void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != firebaseUser)
        {
            bool signedIn = firebaseUser != auth.CurrentUser && auth.CurrentUser != null;

            if (!signedIn && firebaseUser != null)
            {
                Debug.Log("Signed out");

                StopListeners();

                gameManager.OnSignOut();
            }

            firebaseUser = auth.CurrentUser;

            if (signedIn)
            {
                Debug.Log("Signed in as " + firebaseUser.UserId);

                GetToken();

                AddListeners();

                // REMARKS:
                // set userStatus to "idle"
                // in case auto sign-in (user didnt log out last time)
                SetUserStatusAfterSignIn();
            }
        }
    }

    private async void GetToken()
    {
        try
        {
            token = await firebaseUser.TokenAsync(true);

            Debug.Log("user's token: " + token);
        }
        catch (Exception e)
        {
            Debug.Log("Firebase get token Failed :(");
            Debug.Log(e.ToString());

            // TODO: handle error
        }
    }

    private void AddListeners()
    {
        AddUserListener();
        AddHeroListener();
        AddMagicAttackListener();
    }

    private void StopListeners()
    {
        userListener.Stop();
        heroListener.Stop();
        magicAttackListener.Stop();
    }

    private void AddUserListener()
    {
        userListener = UserDAO.getUserRef(firebaseUser.UserId).Listen(async snapshot =>
        {
            Debug.Log("Callback received query snapshot from userListener");
            Debug.Log("snapshot.Id: " + snapshot.Id);

            User user = await UserDAO.getUser(snapshot);
            
            if (ValidUser(user))
            {
                Debug.Log("Updating GameManager with user(status): " + user.id + "(" + user.status + ")");
                GameManager.User = user;
            }
            else
            {
                Debug.Log("Empty user :(");
            }
        });

        Debug.Log("added userListener, user.UserId = " + firebaseUser.UserId);
    }

    private void AddHeroListener()
    {
        heroListener = db.Collection("Hero").Listen(snapshot =>
        {
            Debug.Log("Callback received query snapshot from heroListener");

            List<Hero> heroes = new List<Hero>();

            foreach (DocumentSnapshot documentSnapshot in snapshot.Documents)
            {
                Hero hero = HeroDAO.getHero(documentSnapshot);
                heroes.Add(hero);
            }

            Debug.Log(heroes);

            GameManager.Heroes = heroes;
        });

        Debug.Log("added heroListener");
    }

    private void AddMagicAttackListener()
    {
        magicAttackListener = db.Collection("Magic_Attack").Listen(snapshot =>
        {
            Debug.Log("Callback received query snapshot from magicAttackListener");

            List<MagicAttack> magicAttacks = new List<MagicAttack>();

            foreach (DocumentSnapshot documentSnapshot in snapshot.Documents)
            {
                magicAttacks.Add(MagicAttackDAO.getMagicAttack(documentSnapshot));
            }

            Debug.Log(magicAttacks);

            GameManager.MagicAttacks = magicAttacks;
        });

        Debug.Log("added magicAttackListener");
    }

    public static async Task<bool> SignIn(string email, string password)
    {
        try
        {
            firebaseUser = await auth.SignInWithEmailAndPasswordAsync(email, password);

            return true;
        }
        catch (FirebaseException e)
        {
            Debug.Log("Firebase SignIn Failed :(");
            Debug.Log(e.ToString());

            // TODO: handle error
            return false;
        }
        catch (Exception e)
        {
            Debug.Log("Firebase SignIn Failed :(");
            Debug.Log(e.ToString());

            // TODO: handle error
            return false;
        }
    }

    public static async Task<bool> SignUp(string email, string password)
    {
        try
        {
            firebaseUser = await auth.CreateUserWithEmailAndPasswordAsync(email, password);

            return true;
        }
        catch (FirebaseException e)
        {
            Debug.Log("Firebase SignUp Failed :(");
            Debug.Log(e.ToString());

            // TODO: handle error
            return false;
        }
        catch (Exception e)
        {
            Debug.Log("Firebase SignUp Failed :(");
            Debug.Log(e.ToString());

            // TODO: handle error
            return false;
        }
    }

    public static void SignOut()
    {
        auth.SignOut();
    }

    public static bool ValidUser(User user)
    {
        return (user != null && !string.IsNullOrEmpty(user.id) && !string.IsNullOrEmpty(user.name) && !string.IsNullOrEmpty(user.status));
    }

    // End of Auth


    // FireStore

    public static void UpdateFirestore(string collectionName, string documentName, Dictionary<string, object> dataToBeUpdated)
    {
        DocumentReference docRef = db.Collection(collectionName).Document(documentName);

        UpdateFirestore(docRef, dataToBeUpdated);
    }

    public static void UpdateFirestore(DocumentReference docRef, Dictionary<string, object> dataToBeUpdated)
    {
        docRef.UpdateAsync(dataToBeUpdated).ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("Unable to update Firestore T.T");

                // TODO: handle error
            }
            else
            {
                Debug.Log("Updated Firestore: docRef: " + docRef);
                foreach (KeyValuePair<string, object> kvp in dataToBeUpdated)
                {
                    Debug.LogFormat("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                }
            }
        });
        // You can also update a single field with: cityRef.UpdateAsync("Capital", false);
    }

    public static void SetUserStatus(UserStatus userStatus)
    {
        Dictionary<string, object> dataToBeUpdated = new Dictionary<string, object>
        {
            { "status", EnumUtil.ToString(userStatus) }
        };

        FirebaseUtil.UpdateFirestore(UserDAO.getUserRef(firebaseUser.UserId), dataToBeUpdated);
    }

    private async void SetUserStatusAfterSignIn()
    {
        User user = await UserDAO.getUser(firebaseUser.UserId);
        
        // REMARKS:
        // only set for signIn (not for signUp)
        if (ValidUser(user) && user.status != EnumUtil.ToString(UserStatus.Init))
        {
            Debug.Log("Setting user's status from (" + user.status + ") to (" + UserStatus.Idle + ") after sign in");

            SetUserStatus(UserStatus.Idle);
        }
    }

    // End of FireStore
}
