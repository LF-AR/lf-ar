﻿using System.Collections;
using UnityEngine;

public class LocationUtil : MonoBehaviour
{
    public float initialCompassHeading;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        StartCoroutine(InitializeLocation());
    }

    public IEnumerator InitializeLocation()
    {
        // Wait until the editor and unity remote are connected before starting a location service
#if UNITY_EDITOR
        yield return new WaitForSeconds(10);
#endif

        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("location disabled by user");
            yield break;
        }

        // enable compass
        Input.compass.enabled = true;
        // start the location service
        Debug.Log("start location service");
        Input.location.Start();

#if UNITY_EDITOR
        yield return new WaitForSeconds(5);
#endif

        // Wait until service initializes
        int maxSecondsToWaitForLocation = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxSecondsToWaitForLocation > 0)
        {
            yield return new WaitForSeconds(1);
            maxSecondsToWaitForLocation--;
        }

        // Service didn't initialize in 20 seconds
        if (maxSecondsToWaitForLocation < 1)
        {
            Debug.Log("location service timeout");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("unable to determine device location");
            yield break;
        }

        Debug.Log("location service loaded");
        initialCompassHeading = Input.compass.magneticHeading;

        yield break;
    }
}
