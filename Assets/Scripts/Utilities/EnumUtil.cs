﻿using System;
using UnityEngine;

public enum GameStage
{
    Start,
    SignIn,
    Lobby,
    Profile,
    Leaderboard,
    RoomCreate,
    Room,
    Battle,
    Result
}

public enum RoomCustomProperty 
{
    HostNickName,
    GameMode,
    BattleMode,
    TimeLimit
}

public enum PlayerCustomProperty 
{
    SelectCharacterState,
    BattleState,
    FighterColor,
    FirebaseUserId,
    FirebaseHeroId,
}

public enum BattleMode
{
    Free,
    Team
}

public enum GameMode
{
    Shared,
    Detached
}

public enum TimeLimit
{
    ThirtySeconds,
    SixtySeconds,
    Infinite
}

public enum UserStatus
{
    Init,
    Idle,
    InGame,
    BuyHero,
    LevelUp,
    EndGame
}

public class EnumUtil
{
    // Room Custom Property
    const string roomCustomPropertyHostNickName = "hostNickName";
    const string roomCustomPropertyGameMode = "gameMode";
    const string roomCustomPropertyBattleMode = "battleMode";
    const string roomCustomPropertyTimeLimit = "timeLimit";
    // Player Custom Property
    const string playerCustomPropertySelectCharacterState = "SelectCharacterState";
    const string playerCustomPropertyBattleState = "BattleState";
    const string playerCustomPropertyFighterColor = "FighterColor";
    const string playerCustomPropertyFirebaseUserId = "FirebaseUserId";
    const string playerCustomPropertyFirebaseHeroId = "FirebaseHeroId";
    // Game Mode
    const string gameModeDetached = "Detached mode";
    const string gameModeShared = "Shared mode";
    // Battle Mode
    const string battleModeFree = "Free Battle";
    const string battleModeTeam = "Team Battle";
    // Time Limit
    const string timeLimitThirtySeconds = "30s";
    const string timeLimitSixtySeconds = "60s";
    const string timeLimitInfinite = "Infinite";
    // User Status
    const string userStatusInit = "init";
    const string userStatusIdle = "idle";
    const string userStatusInGame = "in-game";
    const string userStatusBuyHero = "buy-hero";
	const string userStatusLevelUp = "level-up";
	const string userStatusEndGame = "end-game";
    // Endpoint
    const string endpointAddMoney = "addMoney";
    const string endpointBuyHero = "buyHero";
    const string endpointEndGame = "endGame";
    const string endpointLevelUp = "levelUp";
    const string endpointPing = "ping";
    const string endpointPingPost = "pingPost";

    
    public static string ToString(object input)
    {
        try
        {
            
            if (input is RoomCustomProperty) 
            {
                return RoomCustomPropertyToString((RoomCustomProperty)input);
            }
            else if (input is PlayerCustomProperty)
            {
                return PlayerCustomPropertyToString((PlayerCustomProperty)input);
            }
            else if (input is GameMode)
            {
                return GameModeToString((GameMode)input);
            }
            else if (input is BattleMode)
            {
                return BattleModeToString((BattleMode)input);
            }
            else if (input is TimeLimit)
            {
                return TimeLimitToString((TimeLimit)input);
            }
            else if (input is UserStatus)
            {
                return UserStatusToString((UserStatus)input);
            }
            else if (input is Endpoint)
            {
                return EndpointToString((Endpoint)input);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        catch (NotImplementedException e)
        {
            Debug.Log(e.ToString());
            return "";
        }
    }

    static string RoomCustomPropertyToString(RoomCustomProperty input)
    {
        switch (input)
        {
            case RoomCustomProperty.HostNickName:
                return roomCustomPropertyHostNickName;
            case RoomCustomProperty.GameMode:
                return roomCustomPropertyGameMode;
            case RoomCustomProperty.BattleMode:
                return roomCustomPropertyBattleMode;
            case RoomCustomProperty.TimeLimit:
                return roomCustomPropertyTimeLimit;
            default:
                throw new NotImplementedException();
        }
    }

    static string PlayerCustomPropertyToString(PlayerCustomProperty input)
    {
        switch (input)
        {
            case PlayerCustomProperty.SelectCharacterState:
                return playerCustomPropertySelectCharacterState;
            case PlayerCustomProperty.BattleState:
                return playerCustomPropertyBattleState;
            case PlayerCustomProperty.FighterColor:
                return playerCustomPropertyFighterColor;
            case PlayerCustomProperty.FirebaseUserId:
                return playerCustomPropertyFirebaseUserId;
            case PlayerCustomProperty.FirebaseHeroId:
                return playerCustomPropertyFirebaseHeroId;
            default:
                throw new NotImplementedException();
        }
    }

    static string GameModeToString(GameMode input)
    {
        switch (input)
        {
            case GameMode.Detached:
                return gameModeDetached;
            case GameMode.Shared:
                return gameModeShared;
            default:
                throw new NotImplementedException();
        }
    }

    static string BattleModeToString(BattleMode input)
    {
        switch (input)
        {
            case BattleMode.Free:
                return battleModeFree;
            case BattleMode.Team:
                return battleModeTeam;
            default:
                throw new NotImplementedException();
        }
    }

    static string TimeLimitToString(TimeLimit input)
    {
        switch (input)
        {
            case TimeLimit.ThirtySeconds:
                return timeLimitThirtySeconds;
            case TimeLimit.SixtySeconds:
                return timeLimitSixtySeconds;
            case TimeLimit.Infinite:
                return timeLimitInfinite;
            default:
                throw new NotImplementedException();
        }
    }

    static string UserStatusToString(UserStatus input)
    {
        switch (input)
        {
            case UserStatus.Init:
                return userStatusInit;
            case UserStatus.Idle:
                return userStatusIdle;
            case UserStatus.InGame:
                return userStatusInGame;
            case UserStatus.BuyHero:
                return userStatusBuyHero;
            case UserStatus.LevelUp:
                return userStatusLevelUp;
            case UserStatus.EndGame:
                return userStatusEndGame;
            default:
                throw new NotImplementedException();
        }
    }

    static string EndpointToString(Endpoint input)
    {
        switch (input)
        {
            case Endpoint.AddMoney:
                return endpointAddMoney;
            case Endpoint.BuyHero:
                return endpointBuyHero;
            case Endpoint.EndGame:
                return endpointEndGame;
            case Endpoint.LevelUp:
                return endpointLevelUp;
            case Endpoint.Ping:
                return endpointPing;
            case Endpoint.PingPost:
                return endpointPingPost;
            default:
                throw new NotImplementedException();
        }
    }

    public static object StringToEnum(string str)
    {
        object output;
        try
        {
            output = SearchForEnum(str);
            return EnumStringValidation(str, output) ? output : throw new NotImplementedException();
        }
        catch (NotImplementedException e)
        {
            Debug.Log(e.ToString());
            return "";
        }
    }

    static object SearchForEnum(string str)
    {
        switch (str)
        {
            // Room Custom Property
            case roomCustomPropertyHostNickName:
                return RoomCustomProperty.HostNickName;
            case roomCustomPropertyGameMode:
                return RoomCustomProperty.GameMode;
            case roomCustomPropertyBattleMode:
                return RoomCustomProperty.BattleMode;
            case roomCustomPropertyTimeLimit:
                return RoomCustomProperty.TimeLimit;
            // Player Custom Property
            case playerCustomPropertySelectCharacterState:
                return PlayerCustomProperty.SelectCharacterState;
            case playerCustomPropertyBattleState:
                return PlayerCustomProperty.BattleState;
            case playerCustomPropertyFighterColor:
                return PlayerCustomProperty.FighterColor;
            case playerCustomPropertyFirebaseUserId:
                return PlayerCustomProperty.FirebaseUserId;
            case playerCustomPropertyFirebaseHeroId:
                return PlayerCustomProperty.FirebaseHeroId;
            // Game Mode
            case gameModeDetached:
                return GameMode.Detached;
            case gameModeShared:
                return GameMode.Shared;
            // Battle Mode
            case battleModeFree:
                return BattleMode.Free;
            case battleModeTeam:
                return BattleMode.Team;
            // Time Limit
            case timeLimitThirtySeconds:
                return TimeLimit.ThirtySeconds;
            case timeLimitSixtySeconds:
                return TimeLimit.SixtySeconds;
            case timeLimitInfinite:
                return TimeLimit.Infinite;
            // User Status
            case userStatusInit:
                return UserStatus.Init;
            case userStatusIdle:
                return UserStatus.Idle;
            case userStatusInGame:
                return UserStatus.InGame;
            case userStatusBuyHero:
                return UserStatus.BuyHero;
            case userStatusLevelUp:
                return UserStatus.LevelUp;
            case userStatusEndGame:
                return UserStatus.EndGame;
            // End point
            case endpointAddMoney:
                return Endpoint.AddMoney;
            case endpointBuyHero:
                return endpointBuyHero;
            case endpointEndGame:
                return Endpoint.EndGame;
            case endpointLevelUp:
                return Endpoint.LevelUp;
            case endpointPing:
                return Endpoint.Ping;
            case endpointPingPost:
                return Endpoint.PingPost;
            default:
                throw new NotImplementedException();

        }
    }

    static bool EnumStringValidation(string str, object output)
    {
        return ToString(output) == str;
    }
}
