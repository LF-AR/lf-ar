﻿using System;
using System.Collections.Generic;
using Proyecto26;
using UnityEngine;
using static Photon.Pun.UtilityScripts.PunTeams;

public enum Endpoint
{
    Ping,
    PingPost,
    BuyHero,
    EndGame,
    LevelUp,
    AddMoney
}

public enum EndGameResult
{
    Win,
    Lose
}

public static class RestAPIUtil
{
    private static Dictionary<string, string> GetHeaders()
    {
        Dictionary<string, string> headers = new Dictionary<string, string>
            {
                {"Authorization", FirebaseUtil.token},
                {"user_id", FirebaseUtil.firebaseUser.UserId}
            };

        return headers;
    }


    // Get
    
    public static RSG.IPromise<ResponseHelper> Get(Endpoint endpoint, string endPointParams = "")
    {
        return Get<ResponseHelper>(endpoint, endPointParams);
    }
    
    public static RSG.IPromise<T> Get<T>(Endpoint endpoint, string endPointParams = "")
    {
        Debug.Log("RestAPIUtil is \"GET\"ting to \"" + GameManager.API + EnumUtil.ToString(endpoint) + endPointParams + "\"");
        Debug.Log("user_id: " + FirebaseUtil.firebaseUser.UserId);
        Debug.Log("Authorization: " + FirebaseUtil.token);

        RequestHelper requestHelper = new RequestHelper
        {
            Uri = GameManager.API + EnumUtil.ToString(endpoint) + endPointParams,
            Headers = GetHeaders()
        };

        if (typeof(T) == typeof(ResponseHelper)) 
        {
            return (RSG.IPromise<T>) RestClient.Get(requestHelper);
        }
        else
        {
            return RestClient.Get<T>(requestHelper);
        }
    }

    // Get (end)


    // Post

    public static RSG.IPromise<T> Post<T>(Endpoint endpoint, object endPointParams)
    {
        Debug.Log("RestAPIUtil is \"POST\"ing to \"" + GameManager.API + EnumUtil.ToString(endpoint) + "\"");
        Debug.Log("user_id: " + FirebaseUtil.firebaseUser.UserId);
        Debug.Log("Authorization: " + FirebaseUtil.token);
        Debug.Log("endPointParams: " + endPointParams);
        
        return RestClient.Post<T>(new RequestHelper
        {
            Uri = GameManager.API + EnumUtil.ToString(endpoint) + "/",
            // Timeout = 120,
            Headers = GetHeaders(),
            Body = endPointParams,
            // Retries = 3, //Number of retries
            // RetryCallback = (err, retries) => {Debug.Log("RESTUtil POST error:");Debug.Log(err);}, //See the error before retrying the request
            EnableDebug = true, //See logs of the requests for debug mode
        });
    }

    // Post (end)
}


// EndGameRequest

[Serializable()]
public class EndGameRequest
{
    public BattleMode battle_mode;
    public EndGameRequestPlayer[] players;
}

[Serializable()]
public class EndGameRequestPlayer 
{
    public string user_id;
    public Team team;
    public float health_point;
    public float magic_point;
    public string hero_id;
}

// EndGameRequest (end)


// EndGameResponse

[Serializable()]
public class EndGameResponse
{
    public bool success;
    public float winnerIncreaseScoreAmount;
}

// EndGameResponse (end)


// BuyHeroRequest

[Serializable()]
public class BuyHeroRequest
{
    public string hero_id;

    public BuyHeroRequest(string hero_id)
    {
        this.hero_id = hero_id;
    }
}

// BuyHeroRequest (end)


// BuyHeroResponse

[Serializable()]
public class BuyHeroResponse
{
    public bool success;
}

// BuyHeroResponse (end)


// LevelUpRequest

[Serializable()]
public class LevelUpRequest
{
    public string heros_id;

    public LevelUpRequest(string heros_id)
    {
        this.heros_id = heros_id;
    }
}

// LevelUpRequest (end)


// LevelUpResponse

[Serializable()]
public class LevelUpResponse
{
    public bool success;
}

// LevelUpResponse (end)


// PingPostResponse
[Serializable()]
public class PingPostResponse
{
    public bool success;
    public User user;
}
// PingPostRespinse (end)
