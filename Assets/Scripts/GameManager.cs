using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static readonly string GameVersion = "v1";
    public static readonly int MaxPlayerInRoom = 4;
    public static readonly string API = "https://us-central1-lf-ar-c0dfb.cloudfunctions.net/api/";
    // public static readonly string API = "http://localhost:5001/lf-ar-c0dfb/us-central1/api/";

    [Header("LF-AR Game Manager")]

    public NetworkConnectionManager networkConnectionManager;
    public AudioManager audioManager;
    public GameObject aRFoundationObject;
    public GyroscopeUtil gyroscopeUtil;

    public GameStage GameStage
    {
        get { return _gameStage; }
        set { _gameStage = value; OnGameStageChanged(); }
    }
    private GameStage _gameStage;

    public static BattleMode BattleMode;
    public static GameMode GameMode;

    public static User PrevUser;
    public static User User
    {
        get { return _User; }
        set 
        { 
            PrevUser = _User; 
            _User = value; 
        }
    }
    private static User _User;

    public static List<Hero> Heroes;
    public static List<MagicAttack> MagicAttacks;

    public static FighterColor FighterColor; // REMARKS: may switch to CustomProperties

    public Vector3 playingFieldCenter;
    public Quaternion playingFieldRotation;

    private bool gameStarted;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(aRFoundationObject);

        GameStage = GameStage.Start;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Update()
    {
        switch (GameStage)
        {
            case GameStage.SignIn:
                {
                    if (StartGameReady())
                    {
                        StartGame();
                    }
                    
                    if (gameStarted && networkConnectionManager.GetConnectionState() == ConnectionState.ConnectedMaster)
                    {
                        NextScene();
                    }

                    break;
                }
        }
    }

    private void OnGameStageChanged()
    {
        switch (GameStage)
        {
            case GameStage.Start:
                {
                    audioManager.Play("BGM1");

                    break;
                }
            case GameStage.SignIn:
                {
                    break;
                }
            case GameStage.Lobby:
                {
                    break;
                }
            case GameStage.Profile:
                {
                    break;
                }
            case GameStage.Leaderboard:
                {
                    break;
                }
            case GameStage.RoomCreate:
                {
                    break;
                }
            case GameStage.Room:
                {
                    break;
                }
            case GameStage.Battle:
                {
                    audioManager.FadeOut("BGM1", 2f);
                    audioManager.Play("BGM2");

                    break;
                }
            case GameStage.Result:
                {
                    audioManager.FadeOut("BGM2", 2f);
                    audioManager.Play("BGM1");

                    break;
                }
        }
    }

    private bool StartGameReady()
    {
        return (!gameStarted && FirebaseUtil.ValidUser(User));
    }

    private void StartGame()
    {
        gameStarted = true;
        
        networkConnectionManager.ConnectToMaster(User.name);

        // debug: ping
        // Debug.Log("PINGing to backend");
        // RestAPIUtil.Get(Endpoint.Ping).Then(response => {
        //     Debug.Log("PING done!");
        //     Debug.Log(response.Text);
        // });
        // Debug.Log("PINGing (POST) to backend");
        // RestAPIUtil.Post<PingPostResponse>(Endpoint.PingPost, null).Then(response => {
        //     Debug.Log("PING (POST) done!");
        //     Debug.Log("response.success: " + response.success);
        //     Debug.Log("response.user: ");
        //     Debug.Log(response.user);
        // });
    }

    public void OnSignOut()
    {
        PhotonNetwork.Disconnect();

        User = null;
        gameStarted = false;

        LoadScene(GameStage.SignIn);
    }


    // Scene Management 
    public void NextScene()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);

            GameStage++;
        }
    }

    public void PreviousScene()
    {
        int previousSceneIndex = SceneManager.GetActiveScene().buildIndex - 1;

        if (previousSceneIndex >= 0)
        {
            SceneManager.LoadScene(previousSceneIndex);

            GameStage--;
        }
    }

    public void LoadScene(GameStage gameStage)
    {
        SceneManager.LoadScene(Enum.GetName(GameStage.GetType(), gameStage));

        GameStage = gameStage;
    }

    // Scene Management (end)
}
