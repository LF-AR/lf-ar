﻿using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using System.Collections.Generic;
using Firebase.Firestore;
using Proyecto26;

public class FirebaseLogin : MonoBehaviour
{
    public InputField emailInput;
    public InputField passwordInput;
    public InputField displayNameInput;
    public Text notificationText;

    private FirebaseAuth auth;
    private FirebaseUser user;
    private ListenerRegistration signUpListener;
    private string notification;
    private string token;

    void Start()
    {
        InitializeFirebase();
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

    public void SignUp()
    {
        string email = emailInput.text;
        string password = passwordInput.text;
        string displayName = displayNameInput.text;

        if (SignUpValidate(email, password, displayName)) 
        {
            auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.Log("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }

                if (task.IsFaulted)
                {
                    Debug.Log("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    notification = "Email has been used!";
                    return;
                }

                // Firebase user has been created.
                FirebaseUser newUser = task.Result;
                notification = "Successfully sign up!";
                Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);

                // add listener to update NickName
                AddSignUpListener();
            });
        } 
        else
        {
            notificationText.text = notification;
        }
    }

    public void SignIn()
    {
        string email = emailInput.text;
        string password = passwordInput.text;

        if (SignInValidate(email, password))
        {
            auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.Log("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.Log("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    notification = "Incorrect email or password";
                    return;
                }

                FirebaseUser newUser = task.Result;
                notification = "Successfully sign in!";

                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);

                SetUserStatus(UserStatus.Idle);
            });
        }
        else
        {
            notificationText.text = notification;
        }
    }

    public void SignOut()
    {
        auth.SignOut();

        notificationText.text = "Successfully sign out!";
    }

    public void UpdateDisplayName()
    {
        Dictionary<string, object> dataToBeUpdated = new Dictionary<string, object>
        {
                { "name", displayNameInput.text }
        };

        FirebaseUtil.UpdateFirestore(UserDAO.getUserRef(auth.CurrentUser.UserId), dataToBeUpdated);
    }

    private void GetToken() 
    {
        auth.CurrentUser.TokenAsync(true).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("TokenAsync was canceled.");
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError("TokenAsync encountered an error: " + task.Exception);
                return;
            }

            token = task.Result;

            Debug.Log("token: " + token);
        });
    }

    public void Ping()
    {
        /*
        RestClient.Get(GameManager.API + "/ping?id=" + user.UserId).Then(response => {
            Debug.Log(response.Text);
        });
        */

        // Send token to your backend via HTTPS
        RestAPIUtil.Get(Endpoint.Ping).Then(response => {
            Debug.Log(response.Text);
        });
    }

    // Handle initialization of the necessary firebase modules:
    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");

        auth = FirebaseAuth.DefaultInstance;
        
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    // Track state changes of the auth object.
    private void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            
            user = auth.CurrentUser;
            
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);

                GetToken();
            }
        }
    }
    
    private bool SignInValidate(string email, string password)
    {
        bool validated = true;
        
        if(email == "")
        {
            notification = "Email cannot be empty";
            validated = false;
        }
        else if(password == "")
        {
            notification = "Password cannot be empty";
            validated = false;
        }
        else if(!email.Contains("@") || !email.Contains("."))
        {
            notification = "Invalid email";
            validated = false;
        } 
        else if(password.Length < 6 )
        {
            notification = "Password must be at least 6 characters";
            validated = false;
        }

        return validated;
    }

    private bool SignUpValidate(string email, string password, string displayName)
    {
        bool validated = SignInValidate(email, password);
        
        if(displayName == "")
        {
            notification = "Display name cannot be empty";
            validated = false;
        }

        return validated;
    }

    private void SetUserStatus(UserStatus userStatus)
    {
        Dictionary<string, object> dataToBeUpdated = new Dictionary<string, object>
        {
                { "status", EnumUtil.ToString(userStatus) }
        };

        FirebaseUtil.UpdateFirestore(UserDAO.getUserRef(auth.CurrentUser.UserId), dataToBeUpdated);
    }

    private void AddSignUpListener()
    {
        DocumentReference docRef = UserDAO.getUserRef(auth.CurrentUser.UserId);

        signUpListener = docRef.Listen(snapshot => {
            Debug.Log("Callback received query snapshot from signUpListener");
            Debug.Log("snapshot: " + snapshot);

            if(auth.CurrentUser != null)
            {    
                Debug.Log("snapshot.Id: " + snapshot.Id);

                User user = snapshot.ConvertTo<User>();

                // TODO: 
                // wait Lawgei to update UserDAO
                // i.e. add `status` field
                // if (user.status == EnumUtil.ToString(UserStatus.Init))
                {
                    Debug.Log("found the init user!");

                    UpdateDisplayName();

                    // no need listen after updated nickname
                    signUpListener.Stop();
                    Debug.Log("stop signUpListener");
                }
            }
        });

        Debug.Log("added signUpListenerDocument, auth.CurrentUser.UserId = " + auth.CurrentUser.UserId);
    }
}
