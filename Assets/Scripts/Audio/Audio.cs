﻿using UnityEngine;

[System.Serializable]
public class Audio 
{
    [HideInInspector]
    public AudioSource source;
    public AudioClip clip;
    public string name;
    [Range(0f,10f)]
    public float volume;
    public bool loop;
}
