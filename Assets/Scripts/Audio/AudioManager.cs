﻿using UnityEngine;
using System;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public Audio[] audios;

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        foreach (Audio a in audios)
        {
            a.source = gameObject.AddComponent<AudioSource>();
            a.source.clip = a.clip;
            a.source.volume = a.volume;
            a.source.loop = a.loop;
            a.source.playOnAwake = false;
        }
    }

    public void Play(string name)
    {
        Audio a = Array.Find(audios, audio => audio.name == name);

        if (a == null)
        {
            Debug.LogWarning("Sound" + name + "not found");
            return;
        }
        else
        {
            a.source.Play();

        }
    }

    public void Stop(string name)
    {
        Audio a = Array.Find(audios, audio => audio.name == name);

        if (a == null)
        {
            Debug.LogWarning("Sound" + name + "not found");
            return;
        }
        else
        {
            a.source.Stop();
        }

    }


    public void FadeOut(string name, float FadeTime = 0.5f)
    {

        Audio a = Array.Find(audios, audio => audio.name == name);

        StartCoroutine(FadeOutIEnumerator(a, FadeTime));
    }

    static IEnumerator FadeOutIEnumerator(Audio a, float FadeTime)
    {
        float startVolume = a.source.volume;

        while (a.source.volume > 0)
        {
            a.source.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        a.source.Stop();

        a.source.volume = startVolume;
    }
}
