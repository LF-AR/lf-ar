﻿using UnityEngine;

public enum AttackTriggerType
{
    Fighter,
    MagicAttackProjectile,
    MagicAttackAOE,
}

public class AttackTrigger : MonoBehaviour
{
    private Fighter fighter;
    private MagicAttackProjectileInfo magicAttackProjectileInfo;
    private MagicAttackAOEInfo magicAttackAOEInfo;
    private AttackInfo attackInfo;

    public static void AttackTriggered(Collider other, AttackInfo attackInfo)
    {
        Debug.Log("AttackTrigger: AttackTriggered()");
        // the other is defending
        if (other.isTrigger == true && other.CompareTag("Defending"))
        {
            other.SendMessageUpwards("OnDefense");
        }

        // hitting the fighter
        if (other.isTrigger == false && other.CompareTag("Fighter"))
        {
            Debug.Log("AttackTrigger: calling onHit() to other: " + other.gameObject);

            other.SendMessageUpwards("OnHit", attackInfo);
        }
    }

    void Awake()
    {
        // REMARKS:
        // use Awake to get variable initialized before anything
        // or else will get null for InitAttackInfo
        // REAMARKS: attached to 
        // fighter, magicAttackProjectile, magicAttackAOE
        Transform parent = transform.parent;
        fighter = parent.GetComponent<Fighter>();
        magicAttackProjectileInfo = parent.GetComponent<MagicAttackProjectileInfo>();
        magicAttackAOEInfo = parent.GetComponent<MagicAttackAOEInfo>();

        Debug.Log("AttackTrigger: fighter (awake): " + fighter);
        Debug.Log("AttackTrigger: magicAttackProjectileInfo (awake): " + magicAttackProjectileInfo);
        Debug.Log("AttackTrigger: magicAttackAOEInfo (awake): " + magicAttackAOEInfo);

        // REMARKS: fighter will wait until ARFighter.playerID get RPC call to set
        if (magicAttackProjectileInfo != null)
        {
            InitAttackInfo(AttackTriggerType.MagicAttackProjectile);
        }
        else if (magicAttackAOEInfo != null)
        {
            InitAttackInfo(AttackTriggerType.MagicAttackAOE);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("AttackTrigger: OnTriggerEnter() to other: " + other.gameObject);

        if (attackInfo != null)
        {
            AttackTriggered(other, attackInfo);
        }
    }

    public void InitAttackInfo(AttackTriggerType attackTriggerType)
    {
        switch(attackTriggerType)
        {
            case AttackTriggerType.Fighter:
                {
                    Debug.Log("AttackTrigger: fighter: " + fighter);

                    attackInfo = AttackInfo.GetAttackInfo(fighter);
                    Debug.Log("AttackTrigger: attackInfo (createdBy Fighter): " + fighter.playerID);

                    break;
                }
            case AttackTriggerType.MagicAttackProjectile:
                {
                    Debug.Log("AttackTrigger: magicAttackProjectileInfo: " + magicAttackProjectileInfo);

                    attackInfo = AttackInfo.GetAttackInfo(magicAttackProjectileInfo);
                    Debug.Log("AttackTrigger: attackInfo (createdBy magicAttackProjectileInfo): " + magicAttackProjectileInfo.sourceID);

                    break;
                }
            case AttackTriggerType.MagicAttackAOE:
                {
                    Debug.Log("AttackTrigger: magicAttackAOEInfo: " + magicAttackAOEInfo);

                    attackInfo = AttackInfo.GetAttackInfo(magicAttackAOEInfo);
                    Debug.Log("AttackTrigger: attackInfo (createdBy magicAttackAOEInfo): " + magicAttackAOEInfo.sourceID);

                    break;
                }
        }
    }
}
