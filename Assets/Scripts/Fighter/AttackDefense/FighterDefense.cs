﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class FighterDefense : MonoBehaviourPunCallbacks
{
    public Button defenseBtn;
    public Collider defenseTrigger;

    private Animator animator;
    private bool defenseEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        defenseTrigger.enabled = false;

        AddButtonListener();
    }

    protected virtual void AddButtonListener()
    {
        defenseBtn.onClick.AddListener(DefenseBtnClicked);
    }

    protected void DefenseBtnClicked()
    {
        if (defenseEnabled)
        {
            defenseEnabled = false;

            // start animation
            animator.ResetTrigger("defending");
            animator.SetTrigger("defending");
        }
    }

    void ResetDefenseTrigger()
    {
        if (defenseTrigger.enabled)
        {
            defenseTrigger.enabled = false;
        }
    }

    // animation event
    // when the defense part of defense animation started
    // using "atk02"
    void DefenseTriggerStart()
    {
        if (!defenseTrigger.enabled)
        {
            defenseTrigger.enabled = true;
        }
    }

    // animation event
    // when the defense part of defense animation finished
    // using "atk02"
    void DefenseTriggerEnd()
    {
        ResetDefenseTrigger();
    }

    public void ResetFighter()
    {
        defenseEnabled = true;
        ResetDefenseTrigger();
    }
}