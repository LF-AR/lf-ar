﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class FighterAttack : MonoBehaviourPunCallbacks
{
    public Button attackBtn;
    public Collider attackTrigger;

    private Animator animator;
    private bool attackEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        attackTrigger.enabled = false;

        AddButtonListener();
    }

    protected virtual void AddButtonListener()
    {
        attackBtn.onClick.AddListener(AttackBtnClicked);
    }

    protected void AttackBtnClicked()
    {
        if (attackEnabled)
        {
            attackEnabled = false;

            // start animation
            animator.ResetTrigger("attacking");
            animator.SetTrigger("attacking");
        }
    }

    void ResetAttackTrigger()
    {
        if (attackTrigger.enabled)
        {
            attackTrigger.enabled = false;
        }
    }

    // animation event
    // when the attack part of attack animation started
    // using "atk01"
    void AttackTriggerStart()
    {
        if (!attackTrigger.enabled)
        {
            attackTrigger.enabled = true;
        }
    }

    // animation event
    // when the attack part of attack animation finished
    // using "atk01"
    void AttackTriggerEnd()
    {
        ResetAttackTrigger();
    }

    public void ResetFighter()
    {
        attackEnabled = true;
        ResetAttackTrigger();
    }
}
