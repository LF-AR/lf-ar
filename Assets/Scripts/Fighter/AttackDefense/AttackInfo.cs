﻿using UnityEngine;

public enum AttackEffect
{
    Hurt,
    Stun,
    Heal,
    Defend
};

public class AttackInfo
{
    public readonly float damage;
    public readonly AttackEffect attackEffect;
    public readonly float attackEffectDuration;
    public readonly string sourceID;

    public static AttackInfo GetAttackInfo(Fighter fighter)
    {
        Debug.Log("GetAttackInfo: fighter.sourceID: " + fighter.playerID);
        Debug.Log("GetAttackInfo: fighter.attackEffect: " + AttackEffect.Hurt);
        Debug.Log("GetAttackInfo: fighter.attackEffectDuration: " + 0);
        Debug.Log("GetAttackInfo: fighter.damage: " + fighter.damage);

        return new AttackInfo(fighter.damage, fighter.playerID, AttackEffect.Hurt, 0);
    }

    public static AttackInfo GetAttackInfo(MagicAttackProjectileInfo magicAttackProjectileInfo)
    {
        Debug.Log("GetAttackInfo: magicAttackProjectileInfo.sourceID: " + magicAttackProjectileInfo.sourceID);
        Debug.Log("GetAttackInfo: magicAttackProjectileInfo.attackEffect: " + magicAttackProjectileInfo.attackEffect);
        Debug.Log("GetAttackInfo: magicAttackProjectileInfo.attackEffectDuration: " + magicAttackProjectileInfo.attackEffectDuration);
        Debug.Log("GetAttackInfo: magicAttackProjectileInfo.damage: " + magicAttackProjectileInfo.damage);

        float levelAdjustmentDamage = SceneManagerBattle.GetLevelAdjustment(magicAttackProjectileInfo.sourceID);
        levelAdjustmentDamage *= ARFighter.LevelFactorDamage;
        Debug.Log("GetAttackInfo: levelAdjustmentDamage: " + levelAdjustmentDamage);

        float increasedDamage = magicAttackProjectileInfo.damage + levelAdjustmentDamage;
        Debug.Log("GetAttackInfo: increasedDamage: " + increasedDamage);

        return new AttackInfo(increasedDamage, magicAttackProjectileInfo.sourceID, magicAttackProjectileInfo.attackEffect, magicAttackProjectileInfo.attackEffectDuration);
    }

    public static AttackInfo GetAttackInfo(MagicAttackAOEInfo magicAttackAOEInfo)
    {
        Debug.Log("GetAttackInfo: magicAttackAOEInfo.sourceID: " + magicAttackAOEInfo.sourceID);
        Debug.Log("GetAttackInfo: magicAttackAOEInfo.attackEffect: " + magicAttackAOEInfo.attackEffect);
        Debug.Log("GetAttackInfo: magicAttackAOEInfo.attackEffectDuration: " + magicAttackAOEInfo.attackEffectDuration);
        Debug.Log("GetAttackInfo: magicAttackAOEInfo.damage: " + magicAttackAOEInfo.damage);

        float levelAdjustmentDamage = SceneManagerBattle.GetLevelAdjustment(magicAttackAOEInfo.sourceID);
        levelAdjustmentDamage *= ARFighter.LevelFactorDamage;
        Debug.Log("GetAttackInfo: levelAdjustmentDamage: " + levelAdjustmentDamage);

        float increasedDamage = magicAttackAOEInfo.damage + levelAdjustmentDamage;
        Debug.Log("GetAttackInfo: increasedDamage: " + increasedDamage);

        return new AttackInfo(increasedDamage, magicAttackAOEInfo.sourceID, magicAttackAOEInfo.attackEffect, magicAttackAOEInfo.attackEffectDuration);
    }

    public AttackInfo(float damage, string sourceID, AttackEffect attackEffect, float attackEffectDuration)
    {
        this.damage = damage;
        this.sourceID = sourceID;
        this.attackEffect = attackEffect;
        this.attackEffectDuration = attackEffectDuration;
    }
}
