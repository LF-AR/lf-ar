﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum MoveAnimation
{
    Idle = 0,
    Walk = 1,
    Run = 2
}

public class ARFighter : Fighter, IPunObservable
{
    [Header("Level Factor")]
    public static readonly float LevelFactorHealthPoint = 1.0f;
    public static readonly float LevelFactorMagicPoint = 1.0f;
    public static readonly float LevelFactorDamage = 0.5f;
    public static readonly float LevelFactorMagicPointRecoverSpeed = 0.03f;
    public static readonly float LevelFactorMoveSpeed = 0.01f;

    [Header("AR")]
    public Transform m_HitTransform;
    public float maxRayDistance = 30.0f;
    public LayerMask collisionLayer = 1 << 10;  //ARKitPlane layer
    public Camera mainCamera;

    private GameManager gameManager;
    private Vector3 remotePosition;

    [HideInInspector]
    public struct InputStr
    {
        public float healthPoint;
        public float magicPoint;
        public Vector3 position;
        public Quaternion rotation;
    }
    public InputStr inputStr;

    public TextMeshProUGUI nickNameField;
    public Image teamColor;


    //testing
    private bool remotePositionInitialized = false;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        gameManager = FindObjectOfType<GameManager>();
        mainCamera = FindObjectOfType<Camera>();

        nickNameField.text = photonView.Owner.NickName;

        if (photonView.IsMine)
        {
            ARUpdatePosition();

            joystick = FindObjectOfType<Joystick>();
        }
        else
        {
            joystick = null;

            foreach (FighterMotion fighterMotion in GetComponentsInChildren<FighterMotion>())
            {
                fighterMotion.gameObject.SetActive(false);
            }
        }

        if (GameManager.BattleMode == BattleMode.Team)
        {
            switch (photonView.Owner.GetTeam())
            {
                case PunTeams.Team.blue:
                    teamColor.color = Color.blue;
                    break;
                case PunTeams.Team.red:
                    teamColor.color = Color.red;
                    break;
                default:
                    teamColor.gameObject.SetActive(false);
                    break;
            }
        }
        else
        {
            teamColor.gameObject.SetActive(false);
        }
    }

    protected override void FixedUpdate()
    {
        inputStr.healthPoint = healthPoint;
        inputStr.magicPoint = magicPoint;

        if (moveEnabled && joystick != null)
        {
            float cameraRotationY = -(Mathf.PI / 180) * mainCamera.transform.eulerAngles.y;

            float horizontal = joystick.Horizontal * Mathf.Cos(cameraRotationY) - joystick.Vertical * Mathf.Sin(cameraRotationY);
            float vertical = joystick.Horizontal * Mathf.Sin(cameraRotationY) + joystick.Vertical * Mathf.Cos(cameraRotationY);

            Move(horizontal, vertical);
        }
        else
        {
            ARMove();
        }
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();

        //If Player go out of bounds, player lose immediately
        if (transform.position.y < -100 && photonView.IsMine && healthPoint > 0)
        {
            photonView.RPC("OnFallToDeath", RpcTarget.All);

            return;
        }
    }

    protected override void Move(float moveHorizontal, float moveVertical)
    {
        base.Move(moveHorizontal, moveVertical);

        ARUpdatePosition();
    }


    // Initialization

    [PunRPC]
    void SetID(string playerID, PhotonMessageInfo info)
    {
        base.playerID = playerID;

        InitWithFirebaseUser();
    }

    public static void SpawnARFighter(ref ARFighter player, ARFighter prefab, Pose hitPose, string playerID)
    {
        hitPose.position.y += 0.2f;
        player = PhotonNetwork.Instantiate(prefab.gameObject.name, hitPose.position, hitPose.rotation).GetComponent<ARFighter>();
        player.GetComponentInParent<PhotonView>().RPC("SetID", RpcTarget.All, playerID);
    }

    public static void SpawnARFighterEditor(ref ARFighter player, ARFighter prefab, string playerID)
    {
        Vector3 position = Vector3.zero;
        Quaternion rotation = Quaternion.identity;
        player = PhotonNetwork.Instantiate(prefab.gameObject.name, position, rotation).GetComponent<ARFighter>();
        player.GetComponentInParent<PhotonView>().RPC("SetID", RpcTarget.All, playerID);
    }

    void InitWithFirebaseUser()
    {
        PlayerUserHeroBundle playerUserHeroBundle = SceneManagerBattle.GetPlayerUserHeroBundle(playerID);
        User currentUser = playerUserHeroBundle.user;
        Hero currentHero = playerUserHeroBundle.hero;

        float levelAdjustment = SceneManagerBattle.GetLevelAdjustment(currentUser, currentHero);

        initialHealthPoint = currentHero.health_point + (levelAdjustment * LevelFactorHealthPoint);
        initialMagicPoint = currentHero.magic_point + (levelAdjustment * LevelFactorMagicPoint);
        magicPointRecoverSpeed = currentHero.magic_point_recover_speed + (levelAdjustment * LevelFactorMagicPointRecoverSpeed);
        moveSpeed = currentHero.move_speed + (levelAdjustment * LevelFactorMoveSpeed);
        damage = currentHero.attack_damage + (levelAdjustment * LevelFactorDamage);

        Debug.Log("ARFighter: " + this);
        Debug.Log("ARFighter: levelAdjustment: " + levelAdjustment);
        Debug.Log("ARFighter: initialHealthPoint: " + currentHero.health_point + " + " + levelAdjustment + " * " + LevelFactorHealthPoint + " = " + initialHealthPoint);
        Debug.Log("ARFighter: initialMagicPoint: " + currentHero.magic_point + " + " + levelAdjustment + " * " + LevelFactorMagicPoint + " = " + initialMagicPoint);
        Debug.Log("ARFighter: magicPointRecoverSpeed: " + currentHero.magic_point_recover_speed + " + " + levelAdjustment + " * " + LevelFactorMagicPointRecoverSpeed + " = " + magicPointRecoverSpeed);
        Debug.Log("ARFighter: moveSpeed: " + currentHero.move_speed + " + " + levelAdjustment + " * " + LevelFactorMoveSpeed + " = " + moveSpeed);
        Debug.Log("ARFighter: damage: " + currentHero.attack_damage + " + " + levelAdjustment + " * " + LevelFactorDamage + " = " + damage);

        healthPoint = initialHealthPoint;
        magicPoint = initialMagicPoint;

        Debug.Log("ARFighter: healthPoint: " + healthPoint);
        Debug.Log("ARFighter: magicPoint: " + magicPoint);

        GetComponentInChildren<AttackTrigger>().InitAttackInfo(AttackTriggerType.Fighter);
    }

    // Initialization (end)


    // Movement

    void ARMove()
    {
        if (photonView.IsMine) return;

        // find out moved distance
        Vector3 lagDistance = remotePosition - transform.position;

        Vector3 lookAtPosition = new Vector3(remotePosition.x, transform.position.y, remotePosition.z);

        if (transform.position.y > -100)
        {
            lagDistance.y = 0;
        }
        else
        {
            float restorePositionY = transform.position.y;
            if (GameManager.GameMode == GameMode.Shared)
            {
                restorePositionY = remotePosition.y;
            }
            else if (GameManager.GameMode == GameMode.Detached)
            {
                restorePositionY = gameManager.playingFieldCenter.y + 0.5f;
            }
            // character fall off at non-owner devices, pull back to height.
            Teleport(new Vector3(transform.position.x, restorePositionY, transform.position.z));
        }

        // too far to compensate => move to position 
        if (lagDistance.magnitude >= 5f)
        {
            // logtext.text += "Teleport " + lagDistance.magnitude + "\n"; ;
            Teleport(remotePosition);
            lagDistance = Vector3.zero;
        }
        else if (lagDistance.magnitude >= 0.7f) // run animation
        {
            // transform.position = Vector3.MoveTowards(transform.position, remotePosition, inputStr.speed * 1.1f);
            transform.position = Vector3.MoveTowards(transform.position, lookAtPosition, runSpeed * moveSpeed * Time.deltaTime * 1.1f);
            animator.SetFloat("move", 1f);
            transform.LookAt(lookAtPosition);
            // logtext.text += name + " Running " + lagDistance.magnitude + "\n";
        }
        else if (lagDistance.magnitude >= 0.1f) // walk animation
        {
            // transform.position = Vector3.MoveTowards(transform.position, remotePosition, inputStr.speed * 1.1f);
            transform.position = Vector3.MoveTowards(transform.position, lookAtPosition, walkSpeed * moveSpeed * Time.deltaTime * 1.1f);
            animator.SetFloat("move", 0.2f);
            transform.LookAt(lookAtPosition);
            // logtext.text += name + " Walking " + lagDistance.magnitude + "\n";
        }
        else // idle
        {
            animator.SetFloat("move", 0f);
            transform.rotation = inputStr.rotation;
        }
    }

    void Teleport(Vector3 teleportPosition)
    {
        transform.position = teleportPosition;
        transform.rotation = inputStr.rotation;
    }

    protected override void BounceBack()
    {
        base.BounceBack();

        ARUpdatePosition();
    }

    void ARUpdatePosition()
    {
        // update character position on server
        if (GameManager.GameMode == GameMode.Shared)
        {
            inputStr.rotation = transform.rotation;
            inputStr.position = transform.position;
        }
        else if (GameManager.GameMode == GameMode.Detached)
        {
            // inputStr.rotation.SetFromToRotation(transform.position, gameManager.playingFieldCenter);
            inputStr.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y - gameManager.playingFieldRotation.eulerAngles.y, 0);

            inputStr.position = new Vector3(transform.position.x - gameManager.playingFieldCenter.x, transform.position.y, transform.position.z - gameManager.playingFieldCenter.z);
        }
    }

    [PunRPC]
    void OnFallToDeath(PhotonMessageInfo info)
    {
        healthPoint = 0;
    }

    // Movement (end)


    // OnHit

    public override void OnHit(AttackInfo attackInfo)
    {
        if (photonView.IsMine)
        {
            Debug.Log("AR OnHit: attackInfo.damage: " + attackInfo.damage);
            
            photonView.RPC("OnHitRpc", RpcTarget.All, attackInfo.damage, attackInfo.sourceID, attackInfo.attackEffect, attackInfo.attackEffectDuration);
        }
    }

    [PunRPC]
    void OnHitRpc(float damageTaken, string sourceID, AttackEffect attackEffect, float attackEffectDuration, PhotonMessageInfo info)
    {
        // if (gameManager.battleMode == BattleMode.Team)
        // {
        //     // TODO: add sourceID for normal attack & magicAttackProjectile (now only AOE have sourceID)
        //     bool sameTeam = photonView.Owner.GetTeam() == PhotonView.Find(sourceID).Owner.GetTeam();

        //     // REMARKS: same ID means cannot be attack
        //     base.OnHit(new AttackInfo(damageTaken, attackEffect, attackEffectDuration, sameTeam ? ID : ID + 1));
        // }
        // else if (gameManager.battleMode == BattleMode.Free)
        // {
            Debug.Log("OnHitRpc: damageTaken: " + damageTaken);

        base.OnHit(new AttackInfo(damageTaken, sourceID, attackEffect, attackEffectDuration));
        // }
    }

    // OnHit (end)


    // PUN Synchronization

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(inputStr.position);
            stream.SendNext(inputStr.rotation);

            stream.SendNext(inputStr.healthPoint);
            stream.SendNext(inputStr.magicPoint);
        }
        else
        {
            if (GameManager.GameMode == GameMode.Shared)
            {
                remotePosition = (Vector3)stream.ReceiveNext();
                inputStr.rotation = (Quaternion)stream.ReceiveNext();
            }
            else if (GameManager.GameMode == GameMode.Detached)
            {
                Vector3 receivedPosition = (Vector3)stream.ReceiveNext();
                remotePosition = new Vector3(receivedPosition.x + gameManager.playingFieldCenter.x, transform.position.y, receivedPosition.z + gameManager.playingFieldCenter.z);

                Quaternion receivedRotation = (Quaternion)stream.ReceiveNext();
                // Quaternion resetRotation = new Quaternion();
                // resetRotation.SetFromToRotation(transform.position, gameManager.playingFieldCenter);
                inputStr.rotation = Quaternion.Euler(0, gameManager.playingFieldRotation.eulerAngles.y + receivedRotation.eulerAngles.y, 0);

                // string text = "\n==================\n";
                // text += "ReceivedRotation: " + receivedRotation.eulerAngles + "\n";
                // text += "PlayingField Rotation" + gameManager.playingFieldRotation + "\n";
                // logtext.text = text;
                // Debug.Log(text);
            }

            inputStr.healthPoint = (float)stream.ReceiveNext();
            inputStr.magicPoint = (float)stream.ReceiveNext();

            if (!remotePositionInitialized)
            {
                Debug.Log("remotePositionInitialized: " + remotePositionInitialized);
                Debug.Log(photonView.ViewID + " telepoting to remote position: " + remotePosition);
                Teleport(remotePosition);

                remotePositionInitialized = true;
            }
        }
    }

    // PUN Synchronization (end)
}
