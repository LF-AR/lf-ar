﻿using Photon.Pun;

public class ARFighterMotion4 : FighterMotion4
{
    protected override void MotionTriggered()
    {
        ARFighterMotion.MotionTriggered(GetComponentInParent<PhotonView>(), fighterMagicAttack.GetFighterMagicAttackType());
    }
}
