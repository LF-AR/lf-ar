﻿using Photon.Pun;

public class ARFighterMotion1 : FighterMotion1
{
    protected override void MotionTriggered()
    {
        ARFighterMotion.MotionTriggered(GetComponentInParent<PhotonView>(), fighterMagicAttack.GetFighterMagicAttackType());
    }
}
