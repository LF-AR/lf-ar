﻿using UnityEngine;
using Photon.Pun;

public class ARFighterMotion : MonoBehaviour
{
    public static void MotionTriggered(PhotonView photonView, FighterMagicAttackType fighterMagicAttackType)
    {
        switch (fighterMagicAttackType)
        {
            case FighterMagicAttackType.Projectile:
                {
                    photonView.RPC("MagicAttackProjectileStartRPC", RpcTarget.All);

                    break;
                }
            case FighterMagicAttackType.AOE:
                {
                    photonView.RPC("MagicAttackAOEStartRPC", RpcTarget.All);

                    break;
                }
        }
    }
}
