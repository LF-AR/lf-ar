﻿using Photon.Pun;

public class ARFighterMotion2 : FighterMotion2
{
    protected override void MotionTriggered()
    {
        ARFighterMotion.MotionTriggered(GetComponentInParent<PhotonView>(), fighterMagicAttack.GetFighterMagicAttackType());
    }
}
