﻿using Photon.Pun;

public class ARFighterMotion3 : FighterMotion3
{
    protected override void MotionTriggered()
    {
        ARFighterMotion.MotionTriggered(GetComponentInParent<PhotonView>(), fighterMagicAttack.GetFighterMagicAttackType());
    }
}
