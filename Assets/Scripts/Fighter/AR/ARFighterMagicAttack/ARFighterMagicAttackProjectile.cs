﻿using Photon.Pun;

public class ARFighterMagicAttackProjectile : FighterMagicAttackProjectile
{
    protected override void AssignFighterMotion()
    {
        if (photonView.IsMine)
        {
            base.AssignFighterMotion();
        }
	}

    [PunRPC]
    void MagicAttackProjectileStartRPC(PhotonMessageInfo info)
    {
        MagicAttackStart(magicAttackIndex);
    }
}
