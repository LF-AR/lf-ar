﻿using Photon.Pun;

public class ARFighterMagicAttackAOE : FighterMagicAttackAOE
{
    protected override void AssignFighterMotion()
    {
        if (photonView.IsMine)
        {
            base.AssignFighterMotion();
        }
    }

    [PunRPC]
    void MagicAttackAOEStartRPC(PhotonMessageInfo info)
    {
        MagicAttackStart(magicAttackIndex);
    }
}
