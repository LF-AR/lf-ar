﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class ARFighterDefense : FighterDefense
{
    protected override void AddButtonListener()
    {
        if (photonView.IsMine)
        {
            defenseBtn = GameObject.Find("Defense Button").GetComponent<Button>();
            defenseBtn.onClick.AddListener(ARDefenseBtnClicked);
        }
    }

    void ARDefenseBtnClicked()
    {
        photonView.RPC("DefenseBtnClickedRpc", RpcTarget.All);
    }

    [PunRPC]
    void DefenseBtnClickedRpc(PhotonMessageInfo info)
    {
        DefenseBtnClicked();
    }
}