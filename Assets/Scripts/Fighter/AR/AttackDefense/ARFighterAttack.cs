﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class ARFighterAttack : FighterAttack
{
    protected override void AddButtonListener()
    {
        if (photonView.IsMine)
        {
            attackBtn = GameObject.Find("Attack Button").GetComponent<Button>();
            attackBtn.onClick.AddListener(ARAttackBtnClicked);
        }
    }

    void ARAttackBtnClicked()
    {
        photonView.RPC("AttackBtnClickedRpc", RpcTarget.All);
    }

    [PunRPC]
    void AttackBtnClickedRpc(PhotonMessageInfo info)
    {
        AttackBtnClicked();
    }
}
