﻿using UnityEngine;

public class FighterIdleBehaviour : StateMachineBehaviour
{
    bool init = true;
    SceneManagerBattle sceneManagerBattle;
    Fighter fighter;
    FighterAttack fighterAttack;
    FighterDefense fighterDefense;
    FighterMagicAttackProjectile fighterMagicAttackProjectile;
    FighterMagicAttackAOE fighterMagicAttackAOE;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (init)
        {
            sceneManagerBattle = FindObjectOfType<SceneManagerBattleShared>();
            if (sceneManagerBattle == null)
            {
                sceneManagerBattle = FindObjectOfType<SceneManagerBattleDetached>();
            }

            fighter = animator.GetComponent<Fighter>();
            fighterAttack = animator.GetComponent<FighterAttack>();
            fighterDefense = animator.GetComponent<FighterDefense>();
            fighterMagicAttackProjectile = animator.GetComponent<FighterMagicAttackProjectile>();
            fighterMagicAttackAOE = animator.GetComponent<FighterMagicAttackAOE>();

            init = false;
        }

        if (sceneManagerBattle?.BattleState == BattleState.Playing)
        {
            fighter.ResetFighter();
            fighterAttack.ResetFighter();
            fighterDefense.ResetFighter();
            fighterMagicAttackProjectile.ResetFighter();
            fighterMagicAttackAOE.ResetFighter();
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
