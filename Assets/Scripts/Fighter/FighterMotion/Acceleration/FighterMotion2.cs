﻿using UnityEngine;

// REMARKS:
// A motion pushing outward and then inward
public class FighterMotion2 : FighterMotionAcceleration
{
    protected override float MotionDuration { get { return 1; } } // 1s
    private readonly float[] motionMagnitudes = { 0.4f, -0.5f, 0.4f };

    public void CheckMotionPhase0(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        // accelerating outward
        if (Increasing(currentAcceleration.z, lastAcceleration.z))
        {
            if (!motionPhaseCompleted && currentAcceleration.z >= motionMagnitudes[motionPhase])
            {
                StartMotionPhases();

                CompleteMotionPhase();
            }
        }
        else // accelerating inward
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase1(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        // accelerating inward
        if (Decreasing(currentAcceleration.z, lastAcceleration.z))
        {
            if (!motionPhaseCompleted && currentAcceleration.z <= motionMagnitudes[motionPhase])
            {
                CompleteMotionPhase();
            }
        }
        else // accelerating outward
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase2(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        // accelerating outward
        if (Increasing(currentAcceleration.z, lastAcceleration.z))
        {
            if (currentAcceleration.z >= motionMagnitudes[motionPhase])
            {
                ResetMotionPhase();

                MotionTriggered();
            }
        }
    }
}
