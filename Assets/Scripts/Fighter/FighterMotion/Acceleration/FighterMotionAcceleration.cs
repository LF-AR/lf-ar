﻿using System.Reflection;
using UnityEngine;

public abstract class FighterMotionAcceleration : FighterMotion
{
    protected readonly float violationMagnitudeThreshold = 0.1f;

    private Camera mainCamera;
    private Vector3 lastAcceleration = Vector3.zero;
    private float compassHeadingOffset;

    protected override void Start()
    {
        base.Start();

        mainCamera = GameObject.Find("AR Camera").GetComponent<Camera>();

        compassHeadingOffset = -(sceneManagerBattle.initialCompassHeading - locationUtil.initialCompassHeading);
    }

    protected bool Increasing(float current, float last)
    {
        return (current - last) >= -violationMagnitudeThreshold;
    }

    protected bool Decreasing(float current, float last)
    {
        return (current - last) <= violationMagnitudeThreshold;
    }

    private Vector3 NormalizeAcceleration()
    {
        Vector3 normalizedAcceleration = Input.acceleration;

        Quaternion[] rotations = new Quaternion[4];
        // REMARKS: normalized the rotation of the device
        rotations[0] = Input.gyro.attitude;
        // REMARKS: rotate to match the x,y,z coordinate of the game (facing user)
        rotations[1] = Quaternion.Euler(90, 0, 90);
        // REMARKS: offset the initial compass heading difference
        rotations[2] = Quaternion.Euler(0, compassHeadingOffset, 0);
        // REMARKS: rotate the y-axis to match the user real world orientation
        rotations[3] = Quaternion.Euler(0, -mainCamera.transform.eulerAngles.y, 0);

        // Debug.Log("DEBUG: mainCamera.transform.eulerAngles.y: " + mainCamera.transform.eulerAngles.y);

        for (int i = 0; i < rotations.Length; i++)
        {
            normalizedAcceleration = rotations[i] * normalizedAcceleration;
        }

        // REMARKS: flip the values to match the raw accelerometer
        normalizedAcceleration.y *= -1;
        normalizedAcceleration.z *= -1;

        // REMARKS: remove gravity to y-axis
        normalizedAcceleration.y += 1;

        // REMARKS:
        // (0, 0, 0) is the normalized acceleration
        // no matter how the device is rotated
        // i.e.
        // x points right
        // y points up
        // z points outward
        return normalizedAcceleration;
    }

    protected override void CheckMotionPhases()
    {
        Vector3 currentAcceleration = NormalizeAcceleration();

        MethodInfo method = motionType.GetMethod("CheckMotionPhase" + motionPhase);
        method.Invoke(this, new object[] { currentAcceleration, lastAcceleration });

        lastAcceleration = currentAcceleration;

        // DEBUG
        // Debug.Log("DEBUG: Input.gyro.attitude.eulerAngles: " + Input.gyro.attitude.eulerAngles);
        // Debug.Log("Input.compass.magneticHeading: " + Input.compass.magneticHeading);
        // Debug.Log("Input.compass.rawVector: " + Input.compass.rawVector);
        // Debug.Log("Normalized Acceleration: " + currentAcceleration);
        // GraphDbg.Log("AccelerationGraphX", currentAcceleration.x, 2000, true);
        // GraphDbg.Log("AccelerationGraphY", currentAcceleration.y, 2000, true);
        // GraphDbg.Log("AccelerationGraphZ", currentAcceleration.z, 2000, true);

        // REMARKS:
        // use Input.acceleration for accelerometer value
        // use gyro.attitude.eulerAngles to check actual orientation value
    }
}
