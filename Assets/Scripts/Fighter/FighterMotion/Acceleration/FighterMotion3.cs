﻿using UnityEngine;

// REMARKS:
// A motion drawing a circle along x-axis & y-axis
public class FighterMotion3 : FighterMotionAcceleration
{
    protected override float MotionDuration { get { return 3; } } // 3s
    private readonly float[] motionMagnitudesX = { -0.05f, 0.1f, -0.05f };
    private readonly float[] motionMagnitudesY = { -0.05f, 0.1f, -0.1f, 0.05f };

    public void CheckMotionPhase0(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Decreasing(currentAcceleration.x, lastAcceleration.x) && Decreasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.y <= motionMagnitudesY[0])
            {
                StartMotionPhases();

                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase1(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Decreasing(currentAcceleration.x, lastAcceleration.x) && Increasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.x <= motionMagnitudesX[0])
            {
                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase2(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Increasing(currentAcceleration.x, lastAcceleration.x) && Increasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.y >= motionMagnitudesY[1])
            {
                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase3(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Increasing(currentAcceleration.x, lastAcceleration.x) && Decreasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.x >= motionMagnitudesX[1])
            {
                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase4(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Decreasing(currentAcceleration.x, lastAcceleration.x) && Decreasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.y <= motionMagnitudesY[2])
            {
                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase5(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Decreasing(currentAcceleration.x, lastAcceleration.x) && Increasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (!motionPhaseCompleted && currentAcceleration.x <= motionMagnitudesX[2])
            {
                CompleteMotionPhase();
            }
        }
        else
        {
            StopMotionPhase();
        }
    }

    public void CheckMotionPhase6(Vector3 currentAcceleration, Vector3 lastAcceleration)
    {
        if (Increasing(currentAcceleration.y, lastAcceleration.y))
        {
            if (currentAcceleration.y >= motionMagnitudesY[3])
            {
                ResetMotionPhase();

                MotionTriggered();
            }
        }
    }
}
