﻿using UnityEngine;

// REMARKS:
// A motion rotating downward & outward along y-axis
public class FighterMotion1 : FighterMotionRotation
{
    protected override float MotionDuration { get { return 1; } } // 1s
    private readonly float[] motionMagnitudes = { 60 }; // angle of 60 degree

    private float startRotationAngleY;

    public void CheckMotionPhase0(Vector3 currentRotationRate, Vector3 currentRotationAngle)
    {
        // rotating down
        if (currentRotationRate.x <= 0)
        {
            if (currentRotationRate.x <= -rotationRateThreshold)
            {
                startRotationAngleY = currentRotationAngle.y;

                StartMotionPhases();

                CompleteMotionPhase();

                StopMotionPhase();
            }
        }
    }

    public void CheckMotionPhase1(Vector3 currentRotationRate, Vector3 currentRotationAngle)
    {
        // rotating down
        if (currentRotationRate.x <= 0)
        {
            if (currentRotationRate.x <= -rotationRateThreshold)
            {
                float angleMagnitudeY = NormalizedRotationMagnitude1(startRotationAngleY, currentRotationAngle.y);

                if (angleMagnitudeY >= motionMagnitudes[0])
                {
                    ResetMotionPhase();

                    MotionTriggered();
                }
            }
        }
        else
        {
            StopMotionPhase();
        }
    }
}
