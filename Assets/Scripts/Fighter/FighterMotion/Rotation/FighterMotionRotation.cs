﻿using System.Reflection;
using UnityEngine;

public abstract class FighterMotionRotation : FighterMotion
{
    protected readonly float rotationRateThreshold = 2f;

    protected float NormalizedRotationMagnitude1(float startRotationAngle, float endRotationAngle)
    {
        // magnitude adjustment since may rotate over 360
        float normalizedRotationMagnitude = endRotationAngle - startRotationAngle;
        if (startRotationAngle > endRotationAngle)
        {
            normalizedRotationMagnitude += 360;
        }

        return normalizedRotationMagnitude;
    }

    protected float NormalizedRotationMagnitude2(float startRotationAngle, float endRotationAngle)
    {
        // magnitude adjustment since may rotate over 360
        float normalizedRotationMagnitude = startRotationAngle - endRotationAngle;
        if (endRotationAngle > startRotationAngle)
        {
            normalizedRotationMagnitude += 360;
        }

        return normalizedRotationMagnitude;
    }

    protected override void CheckMotionPhases()
    {
        Vector3 currentRotationRate = Input.gyro.rotationRateUnbiased;
        Vector3 currentRotationAngle = Input.gyro.attitude.eulerAngles;

        MethodInfo method = motionType.GetMethod("CheckMotionPhase" + motionPhase);
        method.Invoke(this, new object[] { currentRotationRate, currentRotationAngle });

        // DEBUG
        //GraphDbg.Log("RotationRateGraphX", currentRotationRate.x, 2000, true);
        //GraphDbg.Log("RotationAngleGraphY", currentRotationAngle.y, 2000, true);

        // REMARKS:
        // use gyro.rotationRateUnbiased to check rotation direction
        // gyro.rotationRateUnbiased.x for vertical rotation (along the horizontal x-axis)
        // >0 = rotate towards user
        // <0 = rotate away from user
        // gyro.rotationRateUnbiased.z for horizontal rotation (along the horizontal z-axis)
        // >0 = rotate left
        // <0 = rotate right

        // REMARKS:
        // use gyro.attitude.eulerAngles to check actual orientation value
        // gyro.attitude.eulerAngles.y for vertical orientation (the vertical y-axis)
        // 0 = lie flatly
        // 90 = vertical (facing outside)
        // 180 = upside down
        // 270 = vertical (facing user)
        // gyro.attitude.eulerAngles.z for horizontal orientation (the z-axis)
        // 0 = home btn on top
        // 90 = home btn on left
        // 180 = home btn on bottom
        // 270 = home btn on right
    }
}