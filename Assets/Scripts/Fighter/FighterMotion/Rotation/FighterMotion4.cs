﻿using UnityEngine;

// REMARKS:
// A motion rotating left then right along z-axis
// TODO: fix gimbal lock
public class FighterMotion4 : FighterMotionRotation
{
    protected override float MotionDuration { get { return 2; } } // 1s
    private readonly float[] motionMagnitudes = { 90, 215 }; // angle of 60 degree

    private float startRotationAngleZ;

    public void CheckMotionPhase0(Vector3 currentRotationRate, Vector3 currentRotationAngle)
    {
        // rotating left
        if (currentRotationRate.z >= 0)
        {
            if (currentRotationRate.z >= rotationRateThreshold)
            {
                startRotationAngleZ = currentRotationAngle.z;

                StartMotionPhases();

                CompleteMotionPhase();

                StopMotionPhase();
            }
        }
    }

    public void CheckMotionPhase1(Vector3 currentRotationRate, Vector3 currentRotationAngle)
    {
        // rotating left
        if (currentRotationRate.z >= 0)
        {
            if (currentRotationRate.z >= rotationRateThreshold)
            {
                float angleMagnitudeZ = NormalizedRotationMagnitude1(startRotationAngleZ, currentRotationAngle.z);

                if (angleMagnitudeZ >= motionMagnitudes[0])
                {
                    CompleteMotionPhase();
                }
            }
        }
        else
        {
            startRotationAngleZ = currentRotationAngle.z;

            StopMotionPhase();
        }
    }

    public void CheckMotionPhase2(Vector3 currentRotationRate, Vector3 currentRotationAngle)
    {
        // rotating right
        if (currentRotationRate.z <= 0)
        {
            if (currentRotationRate.z <= -rotationRateThreshold)
            {
                float angleMagnitudeZ = NormalizedRotationMagnitude2(startRotationAngleZ, currentRotationAngle.z);

                if (angleMagnitudeZ >= motionMagnitudes[1])
                {
                    ResetMotionPhase();

                    MotionTriggered();
                }
            }
        }
        else
        {
            StopMotionPhase();
        }
    }
}
