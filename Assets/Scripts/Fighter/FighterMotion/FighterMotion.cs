﻿using System;
using UnityEngine;

public abstract class FighterMotion : MonoBehaviour
{
    protected Type motionType;
    protected FighterMagicAttack fighterMagicAttack;
    protected int magicAttackIndex;
    protected abstract float MotionDuration { get; }
    protected int motionPhase;
    protected bool motionPhaseCompleted;


    protected LocationUtil locationUtil;
    protected SceneManagerBattle sceneManagerBattle;

    private float motionStartTime;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        locationUtil = FindObjectOfType<LocationUtil>();
        sceneManagerBattle = FindObjectOfType<SceneManagerBattle>();

        motionType = GetType();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.gyro.enabled)
        {
            CheckMotionTime();

            CheckMotionPhases();
        }
    }

    public void SetMagicAttack(FighterMagicAttack fighterMagicAttack)
    {
        this.fighterMagicAttack = fighterMagicAttack;
    }

    public void SetMagicAttackIndex(int magicAttackIndex)
    {
        this.magicAttackIndex = magicAttackIndex;
    }

    protected void StartMotionPhases()
    {
        motionStartTime = Time.time;
    }

    protected void CompleteMotionPhase()
    {
        motionPhaseCompleted = true;
    }

    protected void StopMotionPhase()
    {
        if (motionPhaseCompleted)
        {
            // proceed to next phase
            motionPhaseCompleted = false;
            motionPhase++;
        }
        else
        {
            ResetMotionPhase();
        }
    }

    protected void ResetMotionPhase()
    {
        motionPhase = 0;
        motionPhaseCompleted = false;
    }

    protected void CheckMotionTime()
    {
        if ((Time.time - motionStartTime) > MotionDuration && motionPhase != 0)
        {
            ResetMotionPhase();
        }
    }

    protected abstract void CheckMotionPhases();

    protected virtual void MotionTriggered()
    {
        fighterMagicAttack?.MagicAttackStart(magicAttackIndex);
    }
}
