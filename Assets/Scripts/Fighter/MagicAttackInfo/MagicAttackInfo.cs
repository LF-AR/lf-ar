﻿using UnityEngine;

public abstract class MagicAttackInfo : MonoBehaviour
{
    public static readonly string SOURCE_NULL = "";

    public float magicPoint;
    public float damage;
    public float duration;
    public AttackEffect attackEffect;
    public float attackEffectDuration;
    public string MagicSpawnSound;
    public string sourceID;

    //public string MagicBassSound
}
