﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public enum FighterColor
{
    Red,
    Blue,
    Green,
    Yellow,
    Purple,
}

public class Fighter : MonoBehaviourPunCallbacks
{
    public string playerID;
    public float initialHealthPoint;
    public float initialMagicPoint;
    public float magicPointRecoverSpeed;
    public float moveSpeed;
    public float damage;
    public Joystick joystick;
    public Image healthBar;
    public Image magicBar;
    public Canvas status;
    public bool magicAttackEnabled;

    protected Animator animator;
    protected AudioManager audioManager;
    protected readonly float joyStickThresholdWalk = 0.1f;
    protected readonly float joyStickThresholdRun = 0.7f;
    protected readonly float walkSpeed = 0.5f;
    protected readonly float runSpeed = 1.0f;
    protected float healthPoint;
    protected float magicPoint;
    protected bool moveEnabled;

    private AttackEffect attackEffect;
    private float attackEffectTimer;
    private bool attackEffectEnabled;
    private bool victory;
    private bool defending;



    // Start is called before the first frame update
    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();

        magicAttackEnabled = false;
        moveEnabled = false;

        healthPoint = initialHealthPoint;
        magicPoint = initialMagicPoint;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (healthPoint <= 0)
        {
            // REMARKS: reset for checking winning player/team
            if (healthPoint < 0)
            {
                healthPoint = 0;
            }

            if (!animator.GetBool("dead"))
            {
                animator.SetBool("dead", true);
            }

            if (moveEnabled)
            {
                moveEnabled = false;
            }

            if (magicAttackEnabled)
            {
                magicAttackEnabled = false;
            }
        }
        else if (!victory)
        {
            IncreaseMagicPoint();

            UpdateAttackEffectTimer();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (moveEnabled)
        {
            Move(joystick.Horizontal, joystick.Vertical);
        }
    }

    protected virtual void LateUpdate()
    {
        status.transform.LookAt(Camera.main.transform);
        status.transform.Rotate(0, 180, 0);
    }

    protected virtual void Move(float moveHorizontal, float moveVertical)
    {
        float joystickHandleDistance = Mathf.Sqrt(Mathf.Pow(moveHorizontal, 2f) + Mathf.Pow(moveVertical, 2f));

        // rotate to joystick direction
        if (joystickHandleDistance > 0)
        {
            float direction = Mathf.Atan2(moveHorizontal, moveVertical);
            transform.rotation = Quaternion.Euler(0f, direction * Mathf.Rad2Deg, 0f);
        }

        // move
        if (joystickHandleDistance > joyStickThresholdRun)
        {
            transform.position += transform.forward * (runSpeed * moveSpeed * Time.deltaTime);
        }
        else if (joystickHandleDistance > joyStickThresholdWalk)
        {
            transform.position += transform.forward * (walkSpeed * moveSpeed * Time.deltaTime);
        }

        // set animation
        animator.SetFloat("move", joystickHandleDistance);
    }

    protected virtual void BounceBack()
    {
        transform.position -= transform.forward * (runSpeed * moveSpeed * Time.deltaTime);
    }

    void IncreaseMagicPoint()
    {
        magicPoint += magicPointRecoverSpeed * Time.deltaTime;

        if (magicPoint > initialMagicPoint)
        {
            magicPoint = initialMagicPoint;
        }

        magicBar.fillAmount = magicPoint / initialMagicPoint;
    }

    void SetAttackEffect(AttackInfo attackInfo)
    {
        attackEffectTimer = attackInfo.attackEffectDuration;
        attackEffectEnabled = true;
    }

    void UpdateAttackEffectTimer()
    {
        if (attackEffectEnabled)
        {
            if (attackEffectTimer > 0)
            {
                attackEffectTimer -= Time.deltaTime;

                switch (attackEffect)
                {
                    case AttackEffect.Defend:
                        {
                            BounceBack();

                            break;
                        }
                }
            }
            else
            {
                ResetFighter();
            }
        }
    }

    public void SetVictory()
    {
        ResetFighter();

        DisableMove();
        DisableMagicAttack();

        victory = true;

        animator.SetBool("victory", true);
    }

    public void DisableMove()
    {
        moveEnabled = false;
    }

    public void DisableMagicAttack()
    {
        magicAttackEnabled = false;
    }

    public void ResetFighter()
    {
        if (attackEffectEnabled)
        {
            attackEffectEnabled = false;

            animator.SetBool("stun", false);
        }

        moveEnabled = true;
        magicAttackEnabled = true;
    }

    public float GetMagicPoint()
    {
        return magicPoint;
    }

    public void ReduceMagicPoint(float magicPoint)
    {
        this.magicPoint -= magicPoint;
    }

    // AttackTrigger called when hitting
    public virtual void OnHit(AttackInfo attackInfo)
    {
        bool attackable = (attackInfo.sourceID != playerID) || (attackInfo.attackEffect == AttackEffect.Heal);

        if (attackable && !defending && healthPoint > 0 && !victory)
        {
            Debug.Log("OnHit (damage): " + attackInfo.damage);

            if (attackInfo.attackEffect == AttackEffect.Heal)
            {
                healthPoint += attackInfo.damage;
                healthPoint = (healthPoint > initialHealthPoint) ? initialHealthPoint : healthPoint;
            }
            else
            {
                healthPoint -= attackInfo.damage;
            }
            healthBar.fillAmount = healthPoint / initialHealthPoint;

            attackEffect = attackInfo.attackEffect;
            switch (attackInfo.attackEffect)
            {
                case AttackEffect.Hurt:
                    {
                        animator.Play("hurt");

                        break;
                    }
                case AttackEffect.Stun:
                    {
                        SetAttackEffect(attackInfo);

                        animator.SetBool("stun", true);
                        animator.Play("stun");

                        break;
                    }
                case AttackEffect.Heal:
                    {
                        // @Nelson: TODO: play heal music

                        break;
                    }
                case AttackEffect.Defend:
                    {
                        SetAttackEffect(attackInfo);

                        DisableMove();
                        animator.Play("hurt");

                        break;
                    }
            }
        }
    }

    // AttackTrigger called when defending
    public void OnDefense()
    {
        defending = true;
    }

    // animation event
    // using "atk02"
    public void DefenseTriggerEnd()
    {
        defending = false;
    }

    // animation event
    // using "walk"
    private void StepSound1()
    {
        audioManager.Play("Step1");
    }

    // animation event
    // using "walk"
    private void StepSound2()
    {
        audioManager.Play("Step2");
    }
}
