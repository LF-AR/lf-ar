﻿public class FighterMagicAttackProjectile : FighterMagicAttack
{
    private MagicAttackProjectileInfo magicAttackProjectileInfo;
    private FighterMagicAttackProjectileMover fighterMagicAttackProjectileMover;
    private bool magicSpawned;

    protected override void Start()
    {
        fighterMagicAttackType = FighterMagicAttackType.Projectile;

        base.Start();
    }

    protected override void MagicAttackAnimationStart()
    {
        magicAttackProjectileInfo = (MagicAttackProjectileInfo)magicAttackInfo;

        string animationTrigger = magicAttackAnimations[magicAttackIndex];
        animator.ResetTrigger(animationTrigger);
        animator.SetTrigger(animationTrigger);
    }

    protected override void MagicAttackInstantiationStart()
    {
        magicSpawned = false;
    }

    // animation event
    // using "atk04", "atk06", "atk08"
    void MagicAttackSpawn()
    {
        magicAttack = Instantiate(magicAttacks[magicAttackIndex], FirePoint.transform.position, FirePoint.transform.rotation);

        // stop initial movement
        fighterMagicAttackProjectileMover = magicAttack.GetComponent<FighterMagicAttackProjectileMover>();
        fighterMagicAttackProjectileMover.SetSpeed(0);
    }

    // animation event
    // using "atk04", "atk06", "atk08"
    void MagicAttackSpawnAudio()
    {
        MagicAttackAudioStart();
    }

    // animation event
    // let magicAttack move after spawned
    // using "atk04", "atk06", "atk08"
    void MagicAttackMove()
    {
        fighterMagicAttackProjectileMover.SetSpeed(magicAttackProjectileInfo.speed);

        magicSpawned = true;
    }

    // animation event
    // using "atk04", "atk06", "atk08"
    void MagicAttackMoveAudio()
    {
        audioManager.Play(magicAttackProjectileInfo.MagicMoveSound);
    }

    public override void ResetFighter()
    {
        base.ResetFighter();

        if (!magicSpawned)
        {
            DestroyMagicAttack();
        }

        fighter.magicAttackEnabled = true;
    }
}
