﻿using UnityEngine;

public class FighterMagicAttackProjectileMover : MonoBehaviour
{
    public float hitOffset;
    public bool UseFirePointRotation;
    public GameObject hit;
    public GameObject flash;

    private AudioManager audioManager;
    private float speed;
    private float duration;
    private float durationTimer;


    // REMARKS: put these in awake so these are called before setters
    private void Awake()
    {
        MagicAttackProjectileInfo magicAttackProjectileInfo = GetComponent<MagicAttackProjectileInfo>();
        audioManager = FindObjectOfType<AudioManager>();
        speed = magicAttackProjectileInfo.speed;
        duration = magicAttackProjectileInfo.duration;
    }

    void Start()
    {
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            flashInstance.transform.forward = gameObject.transform.forward;

            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs == null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }
    }

    private void Update()
    {
        if (durationTimer >= duration)
        {
            DestroyAudio();
            Destroy(gameObject);
        }
        else
        {
            durationTimer += Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        if (!speed.Equals(0))
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
    }

    //https ://docs.unity3d.com/ScriptReference/Rigidbody.OnCollisionEnter.html
    void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];

        Destroy(contact.point, contact.normal);
    }

    public void Destroy(Vector3 contactPoint, Vector3 contactNormal)
    {
        speed = 0;

        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contactNormal);
        Vector3 pos = contactPoint + contactNormal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);

            if (UseFirePointRotation)
            {
                hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0);
            }
            else
            {
                hitInstance.transform.LookAt(contactPoint + contactNormal);
            }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs == null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }

        DestroyAudio(true);

        Destroy(gameObject);
    }

    void DestroyAudio(bool destroy = false)
    {
        MagicAttackProjectileInfo magicAttackProjectileInfo = GetComponent<MagicAttackProjectileInfo>();

        if (destroy)
        {
            audioManager.Play("Destroy");
        }

        // change to fade out
        audioManager.FadeOut(magicAttackProjectileInfo.MagicMoveSound);
        audioManager.FadeOut("MagicAtkBassSound");
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
}
