﻿using Photon.Pun;
using UnityEngine;

public enum FighterMagicAttackType
{
    Projectile,
    AOE
};

public abstract class FighterMagicAttack : MonoBehaviourPunCallbacks
{
    public GameObject FirePoint;
    public FighterMotion[] fighterMotions;
    public GameObject[] magicAttacks;
    public string[] magicAttackAnimations;

    protected Fighter fighter;
    protected Animator animator;
    protected GameObject magicAttack;
    protected MagicAttackInfo magicAttackInfo;
    protected int magicAttackIndex;
    protected FighterMagicAttackType fighterMagicAttackType;
    protected AudioManager audioManager;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        fighter = GetComponent<Fighter>();
        animator = GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();

        AssignFighterMotion();
    }

    public void MagicAttackStart(int index)
    {
        magicAttackInfo = magicAttacks[index].GetComponent<MagicAttackInfo>();
        magicAttackInfo.sourceID = fighter.playerID;

        bool enoughMagicPoint = fighter.GetMagicPoint() >= magicAttackInfo.magicPoint;

        if (fighter.magicAttackEnabled && enoughMagicPoint)
        {
            fighter.magicAttackEnabled = false;
            magicAttackIndex = index;

            // reduce magicPoint
            fighter.ReduceMagicPoint(magicAttackInfo.magicPoint);

            // start animation
            MagicAttackAnimationStart();

            // instantiate magic attack
            MagicAttackInstantiationStart();
        }
    }

    public FighterMagicAttackType GetFighterMagicAttackType()
    {
        return fighterMagicAttackType;
    }

    protected virtual void AssignFighterMotion()
    {
        for (int i = 0; i < fighterMotions.Length; i++)
        {
            int index = i;  // Closure problem of C#
            fighterMotions[index].SetMagicAttack(this);
            fighterMotions[index].SetMagicAttackIndex(index);
        }
    }

    protected void MagicAttackAudioStart()
    {
        audioManager.Play(magicAttackInfo.MagicSpawnSound);
        audioManager.Play("MagicAtkBassSound");

    }

    protected void DestroyMagicAttack()
    {
        if (magicAttackInfo is MagicAttackProjectileInfo magicAttackProjectileInfo)
        {
            audioManager.FadeOut(magicAttackProjectileInfo.MagicMoveSound);
        }

        audioManager.FadeOut("MagicAtkBassSound");

        Destroy(magicAttack);
    }

    protected abstract void MagicAttackAnimationStart();

    protected abstract void MagicAttackInstantiationStart();

    public virtual void ResetFighter()
    {
        // reset magicAttackInfo's source
        // will be null if hasn't cast magicAttack yet
        if (magicAttackInfo != null)
        {
            magicAttackInfo.sourceID = MagicAttackInfo.SOURCE_NULL;
        }
    }
}
