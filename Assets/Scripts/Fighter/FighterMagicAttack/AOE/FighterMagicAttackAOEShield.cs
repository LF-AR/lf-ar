﻿using System.Collections.Generic;
using UnityEngine;

public class FighterMagicAttackAOEShield : MonoBehaviour
{
    public ParticleSystem pSystem;
    public List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
        pSystem = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("MagicAttackProjectile"))
        {
            int numCollisionEvents = pSystem.GetCollisionEvents(other, collisionEvents);

            for (int i = 0; i < numCollisionEvents; i++)
            {
                FighterMagicAttackProjectileMover fighterMagicAttackProjectileMover = other.GetComponent<FighterMagicAttackProjectileMover>();
                fighterMagicAttackProjectileMover.Destroy(collisionEvents[i].intersection, collisionEvents[i].normal);
            }
        }
        else if (other.CompareTag("Fighter"))
        {
            Collider fighterCollider = other.GetComponent<Collider>();
            MagicAttackAOEInfo magicAttackAOEInfo = GetComponentInParent<MagicAttackAOEInfo>();
            AttackInfo attackInfo = AttackInfo.GetAttackInfo(magicAttackAOEInfo);

            Debug.Log("AttackTriggered! (OnParticleCollision) attackInfo: ");
            Debug.Log(attackInfo);

            AttackTrigger.AttackTriggered(fighterCollider, attackInfo);
        }
    }
}
