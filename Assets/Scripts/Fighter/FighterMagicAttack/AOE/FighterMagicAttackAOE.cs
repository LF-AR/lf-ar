﻿using UnityEngine;

public class FighterMagicAttackAOE : FighterMagicAttack
{
    private MagicAttackAOEInfo magicAttackAOEInfo;
    private float magicAttackTimer;
    private bool magicAttackTiming;

    protected override void Start()
    {
        fighterMagicAttackType = FighterMagicAttackType.AOE;

        base.Start();
    }

    void Update()
    {
        // during magicAttack animation
        if (magicAttackTiming)
        {
            if (magicAttackTimer > 0)
            {
                magicAttackTimer -= Time.deltaTime;
            }
            else
            {
                ResetFighter();
            }
        }
    }

    protected override void MagicAttackAnimationStart()
    {
        string animationBool = magicAttackAnimations[magicAttackIndex];
        animator.SetBool(animationBool, true);
    }

    protected override void MagicAttackInstantiationStart()
    {
        magicAttackAOEInfo = (MagicAttackAOEInfo)magicAttackInfo;
        float size = magicAttackAOEInfo.size;

        // set for duration
        magicAttackTimer = magicAttackAOEInfo.duration;
        magicAttackTiming = true;

        magicAttack = Instantiate(magicAttacks[magicAttackIndex], FirePoint.transform.position, FirePoint.transform.rotation);
        magicAttack.transform.localScale = new Vector3(size, size, size);

        MagicAttackAudioStart();
    }

    public override void ResetFighter()
    {
        base.ResetFighter();

        // during magicAttack animation
        if (magicAttackTiming)
        {
            DestroyMagicAttack();

            string animationBool = magicAttackAnimations[magicAttackIndex];
            animator.SetBool(animationBool, false);

            magicAttackTiming = false;
        }

        fighter.magicAttackEnabled = true;
    }
}
