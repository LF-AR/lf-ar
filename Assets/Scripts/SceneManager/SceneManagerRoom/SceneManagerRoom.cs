using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using TMPro;

public enum SelectCharacterState
{
    Selecting,
    Selected,
    WaitingForStart,
    Start,
}

public abstract class SceneManagerRoom : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI displayRoomName_Text;
    public TextMeshProUGUI hostNameText;
    public TextMeshProUGUI gameModeText;
    public TextMeshProUGUI battleModeText;
    public TextMeshProUGUI notification_text;
    public LFARButton backButton;
    public LFARButton PlayerReadyButton;
    public LFARButton StartGameButton;

    public float animationSpeed;

    public FighterColor[] fighterColors;
    private LFARButton[] leftButtons;
    private LFARButton[] rightButtons;
    private LFARButton[] teamColorButtons;

    public GameObject[] fighterDisplayBoxs;
    public ModelDisplay[] fighterDisplays;
    private Camera[] displayCameras;
    private GameObject[] displaySpotlights;
    private TextMeshProUGUI[] playerNames;

    protected GameManager gameManager;
    private NetworkConnectionManager networkConnectionManager;
    private PunTeams punTeams;
    private Hashtable customProperties;
    private int maxPlayers;
    private int playerIndex;
    private bool roomLeaving;
    protected bool initial = true;

    public SelectCharacterState SelectCharacterState
    {
        get { return _selectCharacterState; }
        set { _selectCharacterState = value; OnSelectCharacterStateUpdated(); }
    }
    private SelectCharacterState _selectCharacterState;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        InitRoomStatus();

        InitUI();

        SetFirebaseUserIdToPlayer();

        AddButtonListeners();

        UpdateSelectCharacterState(SelectCharacterState.Selecting);
    }

    // Update is called once per frame
    void Update()
    {
        // leave room call back
        if (roomLeaving && networkConnectionManager.GetConnectionState() == ConnectionState.ConnectedMaster)
        {
            gameManager.LoadScene(GameStage.Lobby);
        }
    }


    // Initiation

    // REMARKS:
    // Called in both Host & Client
    protected void InitVariables()
    {
        gameManager = FindObjectOfType<GameManager>();
        networkConnectionManager = FindObjectOfType<NetworkConnectionManager>();
        punTeams = FindObjectOfType<PunTeams>();

        maxPlayers = GameManager.MaxPlayerInRoom;

        fighterColors = new FighterColor[maxPlayers];
        leftButtons = new LFARButton[maxPlayers];
        teamColorButtons = new LFARButton[maxPlayers];
        rightButtons = new LFARButton[maxPlayers];
        playerNames = new TextMeshProUGUI[maxPlayers];

        GameManager.GameMode = (GameMode)PhotonNetwork.CurrentRoom.CustomProperties[EnumUtil.ToString(RoomCustomProperty.GameMode)];
        GameManager.BattleMode = (BattleMode)PhotonNetwork.CurrentRoom.CustomProperties[EnumUtil.ToString(RoomCustomProperty.BattleMode)];

        if (GameManager.BattleMode == BattleMode.Free)
        {
            punTeams.enabled = false;
        }
        else if (GameManager.BattleMode == BattleMode.Team)
        {
            punTeams.enabled = true;

            PhotonNetwork.LocalPlayer.SetTeam(PunTeams.Team.none);
        }

        for (int i = 0; i < maxPlayers; i++)
        {
            int index = i;

            playerNames[index] = fighterDisplayBoxs[index].GetComponentsInChildren<TextMeshProUGUI>()[0];
            leftButtons[index] = fighterDisplayBoxs[index].GetComponentsInChildren<LFARButton>()[0];
            rightButtons[index] = fighterDisplayBoxs[index].GetComponentsInChildren<LFARButton>()[1];
            teamColorButtons[index] = fighterDisplayBoxs[index].GetComponentsInChildren<LFARButton>()[2];
        }

        OnVariableInited();
    }

    // REMARKS:
    // both used to check whether both host & client init their variables
    protected abstract void OnVariableInited();
    protected abstract void OnAllVariableInited();

    void InitRoomStatus()
    {
        // REMARKS: restore all the players status (which is already in the room)
        int index = 0;
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            bool updateFighterColor = true;

            playerNames[index].text = player.NickName;

            if (player.Equals(PhotonNetwork.LocalPlayer))
            {
                if (initial)
                {
                    SetLocalFighterColor(FighterColor.Red);

                    updateFighterColor = false;
                }

                playerIndex = index;
            }

            if (updateFighterColor && player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FighterColor)] != null)
            {
                UpdateFightercolor(index, (FighterColor)player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FighterColor)]);
            }

            if (GameManager.BattleMode == BattleMode.Team)
            {
                UpdateTeamColor(index, player.GetTeam());
            }

            index++;
        }
    }

    void InitUI()
    {
        if (initial)
        {
            hostNameText.text = PhotonNetwork.MasterClient.NickName + "'s Room";
            gameModeText.text = EnumUtil.ToString(GameManager.GameMode);
            battleModeText.text = EnumUtil.ToString(GameManager.BattleMode);
            displayRoomName_Text.text = PhotonNetwork.CurrentRoom.Name;

            initial = false;
        }

        for (int i = 0; i < maxPlayers; i++)
        {
            int index = i;

            teamColorButtons[index].gameObject.SetActive(GameManager.BattleMode == BattleMode.Team);

            if (index == playerIndex)
            {
                leftButtons[index].gameObject.SetActive(true);
                rightButtons[index].gameObject.SetActive(true);

                leftButtons[index].onClick.AddListener(() => DisplayMoveLeft(index));
                rightButtons[index].onClick.AddListener(() => DisplayMoveRight(index));

                if (GameManager.BattleMode == BattleMode.Team)
                {
                    teamColorButtons[index].enabled = true;
                    teamColorButtons[index].onClick.AddListener(ChangeTeam);
                }
            }
            else
            {
                leftButtons[index].gameObject.SetActive(false);
                rightButtons[index].gameObject.SetActive(false);
                teamColorButtons[index].enabled = false;
            }
        }

        for (int i = 0; i < maxPlayers; i++)
        {
            int index = i;

            fighterDisplayBoxs[index].gameObject.SetActive(index < PhotonNetwork.CurrentRoom.PlayerCount);
        }
    }

    protected virtual void AddButtonListeners()
    {
        backButton.onClick.AddListener(BackToLobby);
        PlayerReadyButton.onClick.AddListener(PlayerReady);
    }

    // Initiation (end)


    // misc

    void SetLocalFighterColor(FighterColor fighterColor)
    {
        customProperties = new Hashtable
            {
                { "FighterColor", fighterColor }
            };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
    }

    void UpdateFightercolor(int index, FighterColor fighterColor)
    {
        fighterColors[index] = fighterColor;

        fighterDisplays[index].DisplayMoveTo(fighterColor, index);
    }

    void UpdateTeamColor(int index, PunTeams.Team team)
    {
        switch (team)
        {
            case PunTeams.Team.red:
                teamColorButtons[index].image.color = Color.red;
                break;
            case PunTeams.Team.blue:
                teamColorButtons[index].image.color = Color.blue;
                break;
            default:
                teamColorButtons[index].image.color = Color.white;
                break;
        }
    }

    void ToggleRoomVisbility(bool toggle)
    {
        Room room = PhotonNetwork.CurrentRoom;

        room.IsOpen = toggle;
        room.IsVisible = toggle;
    }

    public abstract void SwitchedFromClientToHost();

    // misc (end)


    // Character Display

    void DisplayMoveLeft(int index)
    {
        FighterColor moveToColor;
        if (fighterColors[index] == fighterDisplays[index].userFighterColors[0])
        {
            moveToColor = fighterDisplays[index].userFighterColors[fighterDisplays[index].userFighterColors.Count - 1];
        }
        else
        {
            int currentHeroIndex = 0;

            for (int i = 0; i < fighterDisplays[index].userFighterColors.Count; i++)
            {
                if (fighterDisplays[index].userFighterColors[i] == fighterColors[index])
                {
                    currentHeroIndex = i;
                }
            }
            moveToColor = fighterDisplays[index].userFighterColors[currentHeroIndex - 1];
        }

        SetLocalFighterColor(moveToColor);
    }

    void DisplayMoveRight(int index)
    {
        FighterColor moveToColor;

        if (fighterColors[index] == fighterDisplays[index].userFighterColors[fighterDisplays[index].userFighterColors.Count - 1])
        {
            moveToColor = fighterDisplays[index].userFighterColors[0];
        }
        else
        {
            int currentHeroIndex = 0;

            for (int i = 0; i < fighterDisplays[index].userFighterColors.Count; i++)
            {
                if (fighterDisplays[index].userFighterColors[i] == fighterColors[index])
                {
                    currentHeroIndex = i;
                }
            }
            moveToColor = fighterDisplays[index].userFighterColors[currentHeroIndex + 1];
        }

        SetLocalFighterColor(moveToColor);
    }

    // Character Display (end)


    // PUN Callbacks

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        InitRoomStatus();

        InitUI();

        UpdateSelectCharacterState(SelectCharacterState.Selecting);

        Room room = PhotonNetwork.CurrentRoom;
        if (room.PlayerCount >= room.MaxPlayers)
        {
            ToggleRoomVisbility(false);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        InitRoomStatus();

        InitUI();

        UpdateSelectCharacterState(SelectCharacterState.Selecting);

        Room room = PhotonNetwork.CurrentRoom;
        if (room.PlayerCount < room.MaxPlayers)
        {
            ToggleRoomVisbility(true);
        }
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);

        // REMARKS: update fighterColor & team
        int index = 0;
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.Equals(target))
            {
                if (changedProps["FighterColor"] != null)
                {
                    UpdateFightercolor(index, (FighterColor)changedProps["FighterColor"]);
                }

                if (GameManager.BattleMode == BattleMode.Team && changedProps["Team"] != null)
                {
                    UpdateTeamColor(index, (PunTeams.Team)changedProps["Team"]);
                }

                break;
            }

            index++;
        }
    }

    // PUN Callbacks (end)


    // Button Listener

    void BackToLobby()
    {
        backButton.Disable();

        // REMARKS: reset pun team
        if (GameManager.BattleMode == BattleMode.Team)
        {
            PhotonNetwork.LocalPlayer.SetTeam(PunTeams.Team.none);
        }

        roomLeaving = true;

        networkConnectionManager.LeaveRoom();
    }

    void ChangeTeam()
    {
        if (GameManager.BattleMode != BattleMode.Team)
        {
            return;
        }

        PunTeams.Team team = PunTeams.Team.none;

        // REMARKS: select different team initially
        if (PunTeams.PlayersPerTeam[PunTeams.Team.red].Count == 0)
        {
            team = PunTeams.Team.red;
        }
        else if (PunTeams.PlayersPerTeam[PunTeams.Team.blue].Count == 0)
        {
            team = PunTeams.Team.blue;
        }
        else
        {
            // REMARKS: switch team
            switch (PhotonNetwork.LocalPlayer.GetTeam())
            {
                case PunTeams.Team.red:
                    {
                        team = PunTeams.Team.blue;
                        break;
                    }
                case PunTeams.Team.blue:
                    {
                        team = PunTeams.Team.red;
                        break;
                    }
            }
        }

        PhotonNetwork.LocalPlayer.SetTeam(team);

        customProperties = new Hashtable
        {
            { "Team", team }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
    }

    void PlayerReady()
    {
        if (GameManager.BattleMode == BattleMode.Team)
        {
            // make sure player selecte a team
            if (PhotonNetwork.LocalPlayer.GetTeam() == PunTeams.Team.none)
            {
                return;
            }
        }

        UpdateSelectCharacterState(SelectCharacterState.Selected);
    }

    // Button Listener (end)


    // SelectCharacterState

    protected void UpdateSelectCharacterState(SelectCharacterState newState)
    {
        if (newState - SelectCharacterState == 1)
        {
            SelectCharacterState = newState;
        }
        else if (newState == SelectCharacterState.Selecting)
        {
            SelectCharacterState = newState;
        }

        customProperties = new Hashtable
        {
            { "SelectCharacterState", SelectCharacterState }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
    }

    protected virtual void OnSelectCharacterStateUpdated()
    {
        PlayerReadyButton.gameObject.SetActive(SelectCharacterState == SelectCharacterState.Selecting);

        leftButtons[playerIndex].gameObject.SetActive(SelectCharacterState == SelectCharacterState.Selecting);
        rightButtons[playerIndex].gameObject.SetActive(SelectCharacterState == SelectCharacterState.Selecting);
        teamColorButtons[playerIndex].gameObject.GetComponent<LFARButton>().interactable = (SelectCharacterState == SelectCharacterState.Selecting && GameManager.BattleMode == BattleMode.Team);

        switch (SelectCharacterState)
        {
            case SelectCharacterState.Selecting:
                {
                    notification_text.text = "Select your character and press ready";

                    break;
                }
            case SelectCharacterState.Selected:
                {
                    notification_text.text = "Waiting for other players";

                    break;
                }
            case SelectCharacterState.WaitingForStart:
                {
                    notification_text.text = "All players ready";

                    break;
                }
            case SelectCharacterState.Start:
                {
                    FinishCharacterSelection();

                    break;
                }
        }
    }

    protected virtual void FinishCharacterSelection()
    {
        ToggleRoomVisbility(false);

        GameManager.FighterColor = fighterColors[playerIndex];

        SetFirebaseHeroIdToPlayer();

        if (GameManager.BattleMode == BattleMode.Team)
        {
            punTeams.enabled = false;
        }

        FirebaseUtil.SetUserStatus(UserStatus.InGame);

        gameManager.NextScene();
    }

    // SelectCharacterState (end)


    // Firebase

    void SetFirebaseUserIdToPlayer()
    {
        customProperties = new Hashtable
            {
                { "FirebaseUserId", GameManager.User.id }
            };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
    }

    void SetFirebaseHeroIdToPlayer()
    {
        Hero selectedHero = GameManager.Heroes.Find(hero => hero.fighter_color == fighterColors[playerIndex]);

        customProperties = new Hashtable
            {
                { "FirebaseHeroId", selectedHero.id }
            };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
    }

    // Firebase (end)
}
