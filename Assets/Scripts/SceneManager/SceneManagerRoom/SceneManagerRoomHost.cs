﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using static Photon.Pun.UtilityScripts.PunTeams;

public class SceneManagerRoomHost : SceneManagerRoom
{
    public SceneManagerRoomClient sceneManagerRoomClient;

    private bool variableInited;
    private bool clientVariableInited;

    // Start is called before the first frame update
    protected override void Start()
    {
        InitVariables();
    }

    protected override void OnVariableInited()
    {
        variableInited = true;

        sceneManagerRoomClient.OnHostVariableInited();

        if (clientVariableInited)
        {
            OnAllVariableInited();
        }
    }

    public void OnClientVariableInited()
    {
        clientVariableInited = true;

        if (variableInited)
        {
            OnAllVariableInited();
        }
    }

    protected override void OnAllVariableInited()
    {
        enabled = PhotonNetwork.IsMasterClient;

        if (enabled)
        {
            base.Start();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    protected override void AddButtonListeners()
    {
        base.AddButtonListeners();

        StartGameButton.onClick.AddListener(StartGame);
    }

    protected override void OnSelectCharacterStateUpdated()
    {
        base.OnSelectCharacterStateUpdated();

        StartGameButton.gameObject.SetActive(SelectCharacterState == SelectCharacterState.WaitingForStart);
    }

    protected override void FinishCharacterSelection()
    {
        StartGameButton.Disable();

        base.FinishCharacterSelection();
    }


    // PUN Callbacks (end)

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);

        switch (SelectCharacterState)
        {
            case SelectCharacterState.Selected:
                {
                    if (CheckAllPlayerSelected())
                    {
                        UpdateSelectCharacterState(SelectCharacterState.WaitingForStart);
                    }

                    break;
                }
        }
    }

    // PUN Callbacks (end)


    bool CheckAllPlayerSelected()
    {
        // REMARKS: Debug only
        // This will cause incorrect state if more than 1 player in the room
        // if (Debug.isDebugBuild)
        // {
        //     if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        //     {
        //         return true;
        //     }
        // }
        // Debug (ended)

        // REMARKS: not enough player
        if (PhotonNetwork.CurrentRoom.PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            return false;
        }

        foreach (Player player in PhotonNetwork.PlayerListOthers)
        {
            if ((SelectCharacterState)player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.SelectCharacterState)] == SelectCharacterState.Selecting)
            {
                return false;
            }
        }

        return true;
    }

    void StartGame()
    {
        // REMARKS: double checking each team has member
        if (GameManager.BattleMode == BattleMode.Team)
        {
            if (PlayersPerTeam[Team.blue].Count == 0
                || PlayersPerTeam[Team.red].Count == 0)
            {
                return;
            }
        }

        UpdateSelectCharacterState(SelectCharacterState.Start);
    }

    public override void SwitchedFromClientToHost()
    {
        initial = false;

        base.Start();
    }
}
