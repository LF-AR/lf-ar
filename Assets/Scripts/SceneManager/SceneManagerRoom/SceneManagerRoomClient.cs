﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

public class SceneManagerRoomClient : SceneManagerRoom
{
    public SceneManagerRoomHost sceneManagerRoomHost;

    private bool variableInited;
    private bool hostVariableInited;

    // Start is called before the first frame update
    protected override void Start()
    {
        InitVariables();
    }

    protected override void OnVariableInited()
    {
        variableInited = true;

        sceneManagerRoomHost.OnClientVariableInited();

        if (hostVariableInited)
        {
            OnAllVariableInited();
        }
    }

    public void OnHostVariableInited()
    {
        hostVariableInited = true;

        if (variableInited)
        {
            OnAllVariableInited();
        }
    }

    protected override void OnAllVariableInited()
    {
        enabled = !PhotonNetwork.IsMasterClient;

        if (enabled)
        {
            base.Start();

            StartGameButton.gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public override void SwitchedFromClientToHost()
    {
        sceneManagerRoomHost.gameObject.SetActive(true);
        sceneManagerRoomHost.enabled = true;

        gameObject.SetActive(false);
        enabled = false;

        sceneManagerRoomHost.SwitchedFromClientToHost();
    }

    // PUN Callbacks

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        // REMARKS: changed to host when all other player left
        if (PhotonNetwork.IsMasterClient)
        {
            SwitchedFromClientToHost();
        }
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);

        // REMARKS: update SelectCharacterState from host
        if (target.IsMasterClient && changedProps["SelectCharacterState"] != null)
        {
            SelectCharacterState changedState = (SelectCharacterState)changedProps["SelectCharacterState"];

            switch (changedState)
            {
                case SelectCharacterState.WaitingForStart:
                case SelectCharacterState.Start:
                    {
                        if (changedState != SelectCharacterState)
                        {
                            UpdateSelectCharacterState(changedState);
                        }

                        break;
                    }
            }
        }
    }

    // PUN Callbacks (end)
}
