﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using Firebase.Firestore;

public class SceneManagerSignIn : MonoBehaviour
{
    public LFARButton signInBtn;
    public LFARButton signUpBtn;
    public LFARButton switchToSignUpBtn;
    public LFARButton switchToSignInBtn;
    public TMP_InputField emailInput;
    public TMP_InputField passwordInput;
    public TMP_InputField displayNameInput;
    public TMP_Text notificationText;
    public GameObject signInForm;
    public GameObject signUpForm;

    private ListenerRegistration signUpListener;


    private void Start()
    {
        InitUI();
    }

    private void InitUI() {
        signInBtn.Disable();
        signUpBtn.Disable();
        
        signInBtn.onClick.AddListener(SignIn);
        signUpBtn.onClick.AddListener(SignUp);
        switchToSignUpBtn.onClick.AddListener(() => {ToggleForm(false);});
        switchToSignInBtn.onClick.AddListener(() => {ToggleForm(true);});

        emailInput.onValueChanged.AddListener(ToggleButton);
        passwordInput.onValueChanged.AddListener(ToggleButton);
        displayNameInput.onValueChanged.AddListener(ToggleButton);
        
        // Default signInForm
        signUpForm.SetActive(false);
    }

    public async void SignUp()
    {
        signUpBtn.Disable();

        string email = emailInput.text;
        string password = passwordInput.text;
        string displayName = displayNameInput.text;

        if (SignUpValidate(email, password, displayName))
        {
            if(await FirebaseUtil.SignUp(email, password))
            {
                // add listener to update NickName
                AddSignUpListener();
            }
            else
            {
                notificationText.text = "SignUp Failed :(";

                signUpBtn.Enable();
            }
        }
    }

    public async void SignIn()
    {
        signInBtn.Disable();

        string email = emailInput.text;
        string password = passwordInput.text;

        if (SignInValidate(email, password))
        {
            bool signInSuccess = await FirebaseUtil.SignIn(email, password);
            
            if (!signInSuccess)
            {
                notificationText.text = "SignIn Failed :(";

                signInBtn.Enable();
            }
        }
    }

    public void UpdateDisplayName()
    {
        Dictionary<string, object> dataToBeUpdated = new Dictionary<string, object>
        {
            { "name", displayNameInput.text }
        };

        FirebaseUtil.UpdateFirestore(UserDAO.getUserRef(FirebaseUtil.firebaseUser.UserId), dataToBeUpdated);
    }

    private bool SignInValidate(string email, string password)
    {
        bool validated = true;
        notificationText.text = "";

        if (email == "")
        {
            notificationText.text = "Email cannot be empty";
            validated = false;
        }
        else if (!email.Contains("@") || !email.Contains("."))
        {
            notificationText.text = "Invalid email";
            validated = false;
        }
        else if (password == "")
        {
            notificationText.text = "Password cannot be empty";
            validated = false;
        }
        else if (password.Length < 6)
        {
            notificationText.text = "Password must be at least 6 characters";
            validated = false;
        }

        return validated;
    }

    private bool SignUpValidate(string email, string password, string displayName)
    {
        bool validated = SignInValidate(email, password);

        if (validated && displayName == "")
        {
            notificationText.text = "Display name cannot be empty";
            validated = false;
        }

        return validated;
    }

    private void AddSignUpListener()
    {
        signUpListener = UserDAO.getUserRef(FirebaseUtil.firebaseUser.UserId).Listen(snapshot => {
            Debug.Log("Callback received query snapshot from signUpListener");
            Debug.Log("snapshot.Id: " + snapshot.Id);

            User user = snapshot.ConvertTo<User>();

            Debug.Log("user.status: " + user.status);

            if (user.status == EnumUtil.ToString(UserStatus.Init))
            {
                Debug.Log("Found the init user!");

                UpdateDisplayName();

                FirebaseUtil.SetUserStatus(UserStatus.Idle);

                // no need listen after updated nickname
                signUpListener.Stop();
                Debug.Log("Stop signUpListener");
            }
        });

        Debug.Log("added signUpListener, user.UserId = " + FirebaseUtil.firebaseUser.UserId);
    }

    private void ToggleForm(bool signIn)
    {
        signInForm.SetActive(signIn);
        signUpForm.SetActive(!signIn);
        
        ToggleButton();

        notificationText.text = "";
    }

    private void ToggleButton(string input = "")
    {
        string email = emailInput.text;
        string password = passwordInput.text;
        string displayName = displayNameInput.text;

        if (signInForm.activeSelf)
        {
            if (SignInValidate(email, password))
            {
                signInBtn.Enable();
            }
            else
            {
                signInBtn.Disable();
            }
        }
        else if (signUpForm.activeSelf)
        {
            if (SignUpValidate(email, password, displayName))
            {
                signUpBtn.Enable();
            }
            else
            {
                signUpBtn.Disable();
            }
        }
    }
}
