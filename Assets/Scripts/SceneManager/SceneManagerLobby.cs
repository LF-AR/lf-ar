using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SceneManagerLobby : MonoBehaviourPunCallbacks
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject roomListItemPrefab;
    public TextMeshProUGUI introText;
    public LFARButton createRoomBtn;
    public LFARButton profileBtn;
    public LFARButton leaderboardBtn;

    private NetworkConnectionManager networkConnectionManager;
    private GameManager gameManager;
    private bool roomJoining;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        networkConnectionManager = FindObjectOfType<NetworkConnectionManager>();

        PhotonNetwork.JoinLobby(TypedLobby.Default);

        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (roomJoining && networkConnectionManager.GetConnectionState() == ConnectionState.Joined)
        {
            gameManager.LoadScene(GameStage.Room);
        }
    }

    private void InitUI() {
        introText.text = "Hi " + GameManager.User.name + "! Select a room to join, or create your own :)";

        // REMARKS: scroll to the top of the scrollView
        scrollView.verticalNormalizedPosition = 1;

        createRoomBtn.onClick.AddListener(CreateRoom);
        profileBtn.onClick.AddListener(LoadSceneProfile);
        leaderboardBtn.onClick.AddListener(LoadSceneLeaderboard);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        ClearLobby();

        foreach (RoomInfo room in roomList)
        {
            if (room.IsOpen && room.IsVisible && !room.RemovedFromList)
            {
                GameObject roomListItem = Instantiate(roomListItemPrefab, transform);
                roomListItem.transform.SetParent(scrollContent.transform, false);

                Button roomListItemBtn = roomListItem.GetComponent<Button>();

                Dictionary<string, string> roomListItemBtnProperties = new Dictionary<string, string>();
                roomListItemBtnProperties["RoomNameGroup/RoomName"] = room.Name;
                roomListItemBtnProperties["MaxNoOfPlayerGroup/MaxNoOfPlayer"] = room.MaxPlayers.ToString();
                foreach (RoomCustomProperty roomCustomProperty in (RoomCustomProperty[]) Enum.GetValues(typeof(RoomCustomProperty)))
                {
                    switch (roomCustomProperty)
                    {
                        case RoomCustomProperty.HostNickName:
                            {
                                roomListItemBtnProperties["NickNameGroup/NickName"] = (string)room.CustomProperties[EnumUtil.ToString(RoomCustomProperty.HostNickName)];
                                break;
                            }
                        case RoomCustomProperty.GameMode:
                            {
                                roomListItemBtnProperties["GameModeGroup/GameMode"] = EnumUtil.ToString((GameMode)room.CustomProperties[EnumUtil.ToString(RoomCustomProperty.GameMode)]);
                                break;
                            }
                        case RoomCustomProperty.BattleMode:
                            {
                                roomListItemBtnProperties["BattleModeGroup/BattleMode"] = EnumUtil.ToString((BattleMode)room.CustomProperties[EnumUtil.ToString(RoomCustomProperty.BattleMode)]);
                                break;
                            }
                        case RoomCustomProperty.TimeLimit:
                            {
                                roomListItemBtnProperties["TimeLimitGroup/TimeLimit"] = EnumUtil.ToString((TimeLimit)room.CustomProperties[EnumUtil.ToString(RoomCustomProperty.TimeLimit)]);
                                break;
                            }
                        default:
                            throw new NotImplementedException();
                    }
                }

                foreach(KeyValuePair<string, string> roomListItemBtnProperty in roomListItemBtnProperties)
                {
                    roomListItemBtn.transform.Find(roomListItemBtnProperty.Key).gameObject.GetComponent<TextMeshProUGUI>().text = roomListItemBtnProperty.Value;
                }

                roomListItemBtn.onClick.AddListener(() => JoinRoom(room.Name));
            }
        }
    }

    void ClearLobby()
    {
        Button[] items = scrollContent.GetComponentsInChildren<Button>();
        foreach (Button item in items)
        {
            Destroy(item.gameObject);
        }
    }

    public void JoinRoom(string roomName)
    {
        roomJoining = true;

        networkConnectionManager.JoinRoom(roomName);
    }

    private void DisableButtons()
    {
        createRoomBtn.Disable();
        profileBtn.Disable();
        leaderboardBtn.Disable();
    }

    private void CreateRoom()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.RoomCreate);
    }

    private void LoadSceneProfile()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Profile);
    }

    private void LoadSceneLeaderboard()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Leaderboard);
    }
}
