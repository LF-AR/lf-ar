﻿using UnityEngine;

public abstract class SceneManagerBattleShared : SceneManagerBattle
{
    public GameObject ARPlaneBoundaryPrefab;

    private GameObject ARPlaneBoundaryHolder;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        InitButtons();

        ARPlaneBoundaryHolder = new GameObject();
    }

    protected abstract void InitButtons();

    protected override void OnBattleStateUpdated()
    {
        base.OnBattleStateUpdated();

        switch (BattleState)
        {
            case BattleState.Spawned:
                {
#if !UNITY_EDITOR
                    GeneratePlaneBoundary();
#endif
                    break;
                }
        }
    }

    void GeneratePlaneBoundary()
    {
        ARPlaneBoundaryHolder.transform.position = selectedPlane.transform.position;

        Vector2[] boundary = new Vector2[selectedPlane.boundary.Length];
        selectedPlane.boundary.CopyTo(boundary);

        if (boundary != null && boundary.Length > 1)
        {
            Vector2 prevPoint = boundary[0];

            for (int i = 1; i < boundary.Length; i++)
            {
                int index = i;

                Vector2 currentPoint = boundary[index];

                InstantiateBoundary(prevPoint, currentPoint);

                prevPoint = currentPoint;
            }

            // instantiate the last wall
            InstantiateBoundary(boundary[boundary.Length - 1], boundary[0]);

            // rotate the whole boundary
            ARPlaneBoundaryHolder.transform.rotation = selectedPlane.transform.rotation;
        }
    }

    void InstantiateBoundary(Vector2 start, Vector2 end)
    {
        float midX = (end.x + start.x) / 2;
        float midY = (end.y + start.y) / 2;
        float differenceX = end.x - start.x;
        float differenceY = end.y - start.y;
        float length = Mathf.Sqrt(Mathf.Pow(differenceX, 2f) + Mathf.Pow(differenceY, 2f));

        Vector3 selectedPlanePosition = selectedPlane.transform.position;
        Vector3 center = new Vector3(selectedPlanePosition.x + midX, selectedPlanePosition.y + 0.25f, selectedPlanePosition.z + midY);
        Quaternion rotation = Quaternion.Euler(0, 90 + Mathf.Atan2(differenceX, differenceY) * Mathf.Rad2Deg, 0);

        GameObject ARPlaneBoundary = Instantiate(ARPlaneBoundaryPrefab, center, rotation);
        ARPlaneBoundary.transform.localScale = new Vector3(length, ARPlaneBoundary.transform.localScale.y, ARPlaneBoundary.transform.localScale.z);
        ARPlaneBoundary.transform.SetParent(ARPlaneBoundaryHolder.transform);
    }
}
