using Photon.Pun;

public class SceneManagerBattleSharedHost : SceneManagerBattleShared, ISceneManagerBattleHost
{
    // Start is called before the first frame update
    protected override void Start()
    {
        enabled = PhotonNetwork.IsMasterClient && GameManager.GameMode == GameMode.Shared;

        if (enabled)
        {
            base.Start();

            planeDetectionController.TogglePlaneDetection(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    protected override void InitButtons()
    {
        finishScanButton.onClick.AddListener(FinishScan);
        startGameButton.onClick.AddListener(StartCountDown);

        finishChooseButton.gameObject.SetActive(false);
    }

    protected override void ToggleButtons()
    {
        finishScanButton.gameObject.SetActive(BattleState == BattleState.HostScanningMap);
        startGameButton.gameObject.SetActive(BattleState == BattleState.AllPlayersReady);
    }


    // Button Listener

    void FinishScan()
    {
        planeDetectionController.TogglePlaneDetection(false);

        UpdateBattleState(BattleState.SharingWorldMap);
    }

    // Button Listener (end)
}
