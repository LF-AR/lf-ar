using Photon.Pun;

public class SceneManagerBattleSharedClient : SceneManagerBattleShared, ISceneManagerBattleClient
{
    // Start is called before the first frame update
    protected override void Start()
    {
        enabled = !PhotonNetwork.IsMasterClient && GameManager.GameMode == GameMode.Shared;

        if (enabled)
        {
            base.Start();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    protected override void InitButtons()
    {
        finishScanButton.onClick.AddListener(FinishScan);

        finishChooseButton.gameObject.SetActive(false);
        startGameButton.gameObject.SetActive(false);
    }

    protected override void ToggleButtons()
    {
        finishScanButton.gameObject.SetActive(BattleState == BattleState.ClientScanningMap);
    }


    // Button Listener

    void FinishScan()
    {
        planeDetectionController.TogglePlaneDetection(false);

        UpdateBattleState(BattleState.Ready);
    }

    // Button Listener (end)


    // REMARKS: called by multipeer
    public void MapReceived()
    {
        UpdateBattleState(BattleState.ClientScanningMap);
    }
}
