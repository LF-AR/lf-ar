using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;


public class SceneManagerBattleDetachedClient : SceneManagerBattleDetached, ISceneManagerBattleClient
{
    public Vector3 targetPlayingFieldSize;
    bool containValidPlane;
    bool validPlane;

    // Start is called before the first frame update
    protected override void Start()
    {
        enabled = !PhotonNetwork.IsMasterClient && GameManager.GameMode == GameMode.Detached;

        if (enabled)
        {
            base.Start();

            AttachButtons();

            startGameButton.gameObject.SetActive(false);

            validPlane = false;
            containValidPlane = false;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    protected override void Update()
    {

        base.Update();

        switch (BattleState)
        {
            case BattleState.ClientScanningMap:
                if (ChoosePlaneState == ChoosePlaneState.Scanning)
                {
                    planeDetectionController.UpdatePlaneValidity(targetPlayingFieldSize, out containValidPlane);
                }
                break;
        }
    }

    protected override void UpdateBattleState(BattleState newBattleState)
    {
        if (BattleState == BattleState.ClientScanningMap && ChoosePlaneState != ChoosePlaneState.Chosen)
        {
            Debug.Log("Attempt to update battleState when choosing plane");
            return;
        }

        base.UpdateBattleState(newBattleState);
    }

    protected override void UpdateChoosePlaneState(ChoosePlaneState newChoosePlaneState)
    {
        base.UpdateChoosePlaneState(newChoosePlaneState);
    }

    protected override void OnUpdateChoosePlaneState()
    {
        base.OnUpdateChoosePlaneState();

        ToggleButtons();

        switch (ChoosePlaneState)
        {
            case ChoosePlaneState.Choosing:
                {
                    notification.text = "Please choose the plane you want to play on";

                    break;
                }
            case ChoosePlaneState.Chosen:
                {
                    planeDetectionController.TogglePlaneDetection(false);

                    UpdateBattleState(BattleState.Ready);

                    break;
                }
        }
    }

    protected override void OnBattleStateUpdated()
    {
        base.OnBattleStateUpdated();

        switch (BattleState)
        {
            case BattleState.ClientScanningMap:
                {
                    UpdateChoosePlaneState(ChoosePlaneState.Scanning);

                    break;
                }
        }
    }

    protected override void ToggleButtons()
    {
        finishScanButton.gameObject.SetActive(BattleState == BattleState.ClientScanningMap && ChoosePlaneState == ChoosePlaneState.Scanning);
        finishChooseButton.gameObject.SetActive(BattleState == BattleState.ClientScanningMap && ChoosePlaneState == ChoosePlaneState.Choosing);
    }


    protected override void PlacePlayingField()
    {
        base.PlacePlayingField();

        MeshRenderer mesh = selectedPlane.gameObject.GetComponent<MeshRenderer>();

        if (targetPlayingFieldSize.x <= mesh.bounds.size.x && targetPlayingFieldSize.z <= mesh.bounds.size.z)
        {
            playingFieldSize = targetPlayingFieldSize;
            playingField.transform.localScale = playingFieldSize;
            playingField.gameObject.GetComponent<Renderer>().material = selectedPlaneMaterial;

            validPlane = true;
        }
        else
        {
            playingFieldSize = mesh.bounds.size;
            playingField.transform.localScale = playingFieldSize;
            playingField.gameObject.GetComponent<Renderer>().material = disabledPlaneMaterial;

            validPlane = false;
        }
    }

    protected override void FinishChoosing()
    {
        if (validPlane)
        {
            playingField.gameObject.GetComponent<Renderer>().material = planeMaterial;
            selectedPlane.gameObject.GetComponent<Renderer>().material = planeMaterial;

            base.FinishChoosing();
        }
#if UNITY_EDITOR
        base.FinishChoosing();
#endif 
    }

    protected override void FinishScan()
    {
        if (containValidPlane)
        {
            base.FinishScan();
        }
#if UNITY_EDITOR
        base.FinishScan();
#endif 
    }

    public void MapReceived()
    {
        UpdateBattleState(BattleState.ClientScanningMap);
    }

    // PUN Callbacks

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);

        if (propertiesThatChanged["playingFieldSizeX"] != null && propertiesThatChanged["playingFieldSizeY"] != null && propertiesThatChanged["playingFieldSizeZ"] != null)
        {
            float x = (float)propertiesThatChanged["playingFieldSizeX"];
            float y = (float)propertiesThatChanged["playingFieldSizeY"];
            float z = (float)propertiesThatChanged["playingFieldSizeZ"];
            targetPlayingFieldSize = new Vector3(x, y, z);

            MapReceived();
        }
    }

    // PUN Callbacks (end)
}
