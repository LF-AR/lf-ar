﻿using UnityEngine;
using UnityEngine.XR.ARSubsystems;

public enum ChoosePlaneState
{
    Initial,
    Scanning,
    Choosing,
    Chosen,
}


public abstract class SceneManagerBattleDetached : SceneManagerBattle
{
    protected ChoosePlaneState ChoosePlaneState
    {
        get { return _choosePlaneState; }
        set { _choosePlaneState = value; OnUpdateChoosePlaneState(); }
    }
    private ChoosePlaneState _choosePlaneState;

    public GameObject playingField;
    protected Vector3 playingFieldSize;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        UpdateChoosePlaneState(ChoosePlaneState.Initial);
    }

    protected override void Update()
    {
        base.Update();

        switch (ChoosePlaneState)
        {
            case ChoosePlaneState.Choosing:
                {
                    CheckSelectPlane();

                    break;
                }
        }
    }

    protected void AttachButtons()
    {
        finishScanButton.onClick.AddListener(FinishScan);
        finishChooseButton.onClick.AddListener(FinishChoosing);
        startGameButton.onClick.AddListener(StartCountDown);
    }


    // UpdateChoosePlaneState

    protected virtual void UpdateChoosePlaneState(ChoosePlaneState newChoosePlaneState)
    {
        if (newChoosePlaneState - ChoosePlaneState == 1)
        {
            ChoosePlaneState = newChoosePlaneState;
        }
        else if (newChoosePlaneState == ChoosePlaneState.Initial)
        {
            ChoosePlaneState = newChoosePlaneState;
        }
        else
        {
            Debug.Log("UpdateChoosePlaneState Shielded" + ChoosePlaneState + " " + newChoosePlaneState);
        }
    }

    protected virtual void OnUpdateChoosePlaneState()
    {
        switch (ChoosePlaneState)
        {
            case ChoosePlaneState.Choosing:
                {
                    planeDetectionController.TogglePlaneDetection(false);

                    break;
                }
            case ChoosePlaneState.Chosen:
                {
                    Debug.Log("Plane Chosen Set all plane active to false");

                    break;
                }
        }
    }

    // UpdateChoosePlaneState (end)


    // Play Plane

    protected void CheckSelectPlane()
    {
        if (TryGetTouchPosition(out Vector2 touchPosition))
        {
            if (aRRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
            {
                selectedPlane = planeDetectionController.GetPlaneFromHit(hits[0]);

                PlacePlayingField();
            }
        }
    }

    protected virtual void PlacePlayingField()
    {
        MeshRenderer mesh = selectedPlane.gameObject.GetComponent<MeshRenderer>();

        if (playingField == null)
        {
            playingField = Instantiate(playingFieldPrefab, mesh.bounds.center, Quaternion.identity);
        }
        else
        {
            playingField.transform.position = mesh.bounds.center;
        }
    }

    // Play Plane (end)


    protected virtual void FinishScan()
    {
        UpdateChoosePlaneState(ChoosePlaneState.Choosing);
    }

    protected virtual void FinishChoosing()
    {
#if UNITY_EDITOR
        UpdateChoosePlaneState(ChoosePlaneState.Chosen);
#endif
        if (playingField == null)
        {
#if !UNITY_EDITOR
            Debug.Log("No Plane Chosen");
#endif            
        }
        else
        {
            planeDetectionController.ShowOnlyOnePlane(selectedPlane.trackableId);

#if UNITY_EDITOR
            gameManager.playingFieldCenter = new Vector3(0, 0, 0);
#else 
            gameManager.playingFieldCenter = playingField.gameObject.GetComponent<MeshRenderer>().bounds.center;
#endif
            gameManager.playingFieldRotation = playingField.gameObject.GetComponent<MeshRenderer>().transform.rotation;

            UpdateChoosePlaneState(ChoosePlaneState.Chosen);
        }
    }
}
