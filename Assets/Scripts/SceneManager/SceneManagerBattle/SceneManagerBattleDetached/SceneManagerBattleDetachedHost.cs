
using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class SceneManagerBattleDetachedHost : SceneManagerBattleDetached, ISceneManagerBattleHost
{
    // Start is called before the first frame update
    protected override void Start()
    {
        enabled = PhotonNetwork.IsMasterClient && GameManager.GameMode == GameMode.Detached;

        if (enabled)
        {
            base.Start();

            planeDetectionController.TogglePlaneDetection(true);

            AttachButtons();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    protected override void UpdateBattleState(BattleState newBattleState)
    {
        if (BattleState == BattleState.HostScanningMap && ChoosePlaneState != ChoosePlaneState.Chosen)
        {
            Debug.Log("Attempt to update battleState when choosing plane");
            return;
        }

        base.UpdateBattleState(newBattleState);
    }

    protected override void UpdateChoosePlaneState(ChoosePlaneState newChoosePlaneState)
    {
        base.UpdateChoosePlaneState(newChoosePlaneState);
    }

    protected override void OnBattleStateUpdated()
    {
        base.OnBattleStateUpdated();

        switch (BattleState)
        {
            case BattleState.HostScanningMap:
                {
                    UpdateChoosePlaneState(ChoosePlaneState.Scanning);

                    break;
                }
            case BattleState.SharingWorldMap:
                {
                    planeDetectionController.TogglePlaneDetection(false);

                    break;
                }
        }
    }

    protected override void OnUpdateChoosePlaneState()
    {
        base.OnUpdateChoosePlaneState();

        switch (ChoosePlaneState)
        {
            case ChoosePlaneState.Choosing:
                notification.text = "Please choose the plane you want to play on";
                break;
            case ChoosePlaneState.Chosen:
                UpdateBattleState(BattleState.SharingWorldMap);
                break;
        }

        ToggleButtons();
    }

    protected override void ToggleButtons()
    {
        finishScanButton.gameObject.SetActive(BattleState == BattleState.HostScanningMap && ChoosePlaneState == ChoosePlaneState.Scanning);
        finishChooseButton.gameObject.SetActive(BattleState == BattleState.HostScanningMap && ChoosePlaneState == ChoosePlaneState.Choosing);
        startGameButton.gameObject.SetActive(BattleState == BattleState.AllPlayersReady);
    }

    protected override void PlacePlayingField()
    {
        base.PlacePlayingField();

        MeshRenderer mesh = selectedPlane.gameObject.GetComponent<MeshRenderer>();

        playingFieldSize = new Vector3(mesh.bounds.size.x, playingField.transform.localScale.y, mesh.bounds.size.z);
        playingField.transform.localScale = playingFieldSize;
    }

    protected override void FinishChoosing()
    {
        base.FinishChoosing();

        Hashtable customProperties = new Hashtable();

#if UNITY_EDITOR
        customProperties.Add("playingFieldSizeX", 1f);
        customProperties.Add("playingFieldSizeY", 1f);
        customProperties.Add("playingFieldSizeZ", 1f);
#else        
        customProperties.Add("playingFieldSizeX", playingFieldSize.x);
        customProperties.Add("playingFieldSizeY", playingFieldSize.y);
        customProperties.Add("playingFieldSizeZ", playingFieldSize.z);
#endif

        PhotonNetwork.CurrentRoom.SetCustomProperties(customProperties);
    }
}
