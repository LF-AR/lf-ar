using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Firebase.Firestore;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using TMPro;

public enum BattleState
{
    Initial,
    HostScanningMap,
    SharingWorldMap,
    ClientScanningMap,
    Ready,
    WaitingForSpawn,
    Spawned,
    AllPlayersReady,
    CountingDown,
    Playing,
    Ended,
    ShowResult
}

public abstract class SceneManagerBattle : MonoBehaviourPunCallbacks
{
    public BattleState BattleState
    {
        get { return _battleState; }
        set { _battleState = value; OnBattleStateUpdated(); }
    }
    private BattleState _battleState;

    [Header("UI")]
    public CanvasGroup controllerPanel;
    public Button finishScanButton;
    public Button startGameButton;
    public Button finishChooseButton;
    public TextMeshProUGUI notification;
    public TextMeshProUGUI timer;

    [Header("AR")]
    private ARSession aRSession;
    protected ARSessionOrigin aRSessionOrigin;
    protected ARRaycastManager aRRaycastManager;
    protected PlaneDetectionController planeDetectionController;
    private ARWorldMapController aRWorldMapController;
    protected static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    protected ARPlane selectedPlane;
    public GameObject playingFieldPrefab;
    public Material planeMaterial;
    public Material selectedPlaneMaterial;
    public Material disabledPlaneMaterial;

    [Header("Network")]
    public NetworkConnectionManager networkConnectionManager;
    public Hashtable customProperties;

    [Header("Fighters")]
    public ARFighter PlayerPrefabRed;
    public ARFighter PlayerPrefabBlue;
    public ARFighter PlayerPrefabGreen;
    public ARFighter PlayerPrefabYellow;
    public ARFighter PlayerPrefabPurple;
    [HideInInspector]
    public ARFighter LocalPlayer;
    private ARFighter PlayerPrefab;

    [Header("Manager")]
    protected GameManager gameManager;
    protected BattleManager battleManager;

    [Header("Count Down")]
    private readonly int readyDuration = 5;
    private float TimeLeft;
    private bool controllerActive;

    [Header("Firebase")]
    public static List<User> Users;
    private ListenerRegistration usersListener;

    [HideInInspector]
    public float initialCompassHeading;

    protected virtual void Start()
    {
        InitVariable();

        InitWithContainer();

        GetFighterPrefab();

        ActivateBattleManager();

        InitAR();

        UpdateBattleState(BattleState.Initial);

        ToggleController(false);
    }

    protected virtual void Update()
    {
        switch (BattleState)
        {
            case BattleState.WaitingForSpawn:
                {
                    CheckTouchInput();

                    break;
                }
            case BattleState.CountingDown:
                {
                    CountDown();

                    break;
                }
            case BattleState.Playing:
                {
                    UpdateTimer();

                    if (battleManager.CheckEndGame())
                    {
                        UpdateBattleState(BattleState.Ended);
                    }

                    break;
                }
        }
    }


    // Init

    void InitVariable()
    {
        gameManager = FindObjectOfType<GameManager>();
        //controllerPanel = FindObjectOfType<CanvasGroup>();

        selectedPlane = new ARPlane();

        initialCompassHeading = Input.compass.magneticHeading;

        Users = null;
    }

    void InitWithContainer()
    {
        SceneManagerBattleContainer container = FindObjectOfType<SceneManagerBattleContainer>();

        if (container == null)
        {
            Debug.Log("Missing SceneManagerBattleContainer");
            return;
        }

        controllerPanel = container.controllerPanel;
        finishScanButton = container.finishScanButton;
        finishChooseButton = container.finishChooseButton;
        startGameButton = container.startGameButton;
        notification = container.notification;
        timer = container.timer;

        PlayerPrefabRed = container.PlayerPrefabRed;
        PlayerPrefabBlue = container.PlayerPrefabBlue;
        PlayerPrefabGreen = container.PlayerPrefabGreen;
        PlayerPrefabYellow = container.PlayerPrefabYellow;
        PlayerPrefabPurple = container.PlayerPrefabPurple;

        playingFieldPrefab = container.playingFieldPrefab;

        planeMaterial = container.planeMaterial;
        selectedPlaneMaterial = container.selectedPlaneMaterial;
        disabledPlaneMaterial = container.disabledPlaneMaterial;

        container.enabled = false;
    }

    // Init (end)


    // Battle

    void GetFighterPrefab()
    {
        switch (GameManager.FighterColor)
        {
            case FighterColor.Red:
                PlayerPrefab = PlayerPrefabRed;
                break;
            case FighterColor.Blue:
                PlayerPrefab = PlayerPrefabBlue;
                break;
            case FighterColor.Green:
                PlayerPrefab = PlayerPrefabGreen;
                break;
            case FighterColor.Yellow:
                PlayerPrefab = PlayerPrefabYellow;
                break;
            case FighterColor.Purple:
                PlayerPrefab = PlayerPrefabPurple;
                break;
            default:
                PlayerPrefab = PlayerPrefabRed;
                break;
        }
    }

    void ResetFighters()
    {
        foreach (Fighter fighter in FindObjectsOfType<Fighter>())
        {
            fighter.ResetFighter();
        }
    }

    void ActivateBattleManager()
    {
        switch (GameManager.BattleMode)
        {
            case BattleMode.Free:
                battleManager = FindObjectOfType<BattleManagerFree>();

                battleManager.enabled = true;
                FindObjectOfType<BattleManagerTeam>().enabled = false;

                break;
            case BattleMode.Team:
                battleManager = FindObjectOfType<BattleManagerTeam>();

                battleManager.enabled = true;
                FindObjectOfType<BattleManagerFree>().enabled = false;

                break;
        }

        switch ((TimeLimit)PhotonNetwork.CurrentRoom.CustomProperties[EnumUtil.ToString(RoomCustomProperty.TimeLimit)])
        {
            case TimeLimit.ThirtySeconds:
                {
                    battleManager.SetTimeLeft(30.0f);

                    break;
                }
            case TimeLimit.SixtySeconds:
                {
                    battleManager.SetTimeLeft(60.0f);

                    break;
                }
            case TimeLimit.Infinite:
                {
                    battleManager.SetTimeLeft(float.PositiveInfinity);

                    break;
                }
        }
    }

    protected virtual void UpdateBattleState(BattleState newBattleState)
    {
        Debug.Log("UpdateBattleState (try): " + newBattleState);
        
        if (newBattleState - BattleState == 1)
        {
            BattleState = newBattleState;

            Debug.Log("UpdateBattleState (success): " + BattleState);
        }
        else if (newBattleState == BattleState.Initial)
        {
            BattleState = newBattleState;

            Debug.Log("UpdateBattleState (success): " + BattleState);
        }

        customProperties = new Hashtable
        {
            { "BattleState", BattleState }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(customProperties);
        Debug.Log("Setting localPlayer CustomProperties (BattleState): " + BattleState);
    }

    protected virtual void OnBattleStateUpdated()
    {
        Debug.Log("OnBattleStateUpdated: " + BattleState);

        ToggleButtons();
        
        switch (BattleState)
        {
            case BattleState.Initial:
                {
                    notification.text = "Loading Scene";

                    AddUsersListener();

                    break;
                }
            case BattleState.Ready:
                {
                    notification.text = "Waiting for other players to be ready";

                    break;
                }
            case BattleState.WaitingForSpawn:
                {
                    notification.text = "Touch anywhere on the WorldMap to spawn your character";

                    break;
                }
            case BattleState.Spawned:
                {
                    notification.text = "Waiting for other players to spawn their characters";

                    break;
                }
            case BattleState.AllPlayersReady:
                {
                    notification.text = "All players are ready";

                    break;
                }
            case BattleState.CountingDown:
                {
                    TimeLeft = readyDuration;

                    UpdateTimer();

                    FadeInController();

                    gameManager.gyroscopeUtil.gameObject.SetActive(true);
                    gameManager.gyroscopeUtil.ResetGyroscope();

                    break;
                }
            case BattleState.Playing:
                {
                    notification.text = "";

                    ResetFighters();

                    ToggleController(true);

                    break;
                }
            case BattleState.Ended:
                {
                    notification.text = "Waiting for result";

                    gameManager.gyroscopeUtil.gameObject.SetActive(false);
                    gameManager.gyroscopeUtil.DisableGyroscope();

                    ToggleController(false);
                    FadeOutController();

                    break;
                }
        }

        // Host specified changes
        if (PhotonNetwork.IsMasterClient)
        {
            switch (BattleState)
            {
                case BattleState.HostScanningMap:
                    {
                        notification.text = "Please scan a plane until it is full mapped";

                        break;
                    }
                case BattleState.SharingWorldMap:
                    {
                        notification.text = "Sending WorldMap to other players";

                        break;
                    }
                case BattleState.ClientScanningMap:
                    {
                        notification.text = "Waiting for other players";

                        UpdateBattleState(BattleState.Ready); // host auto ready

                        break;
                    }
            }
        }
        else // Client specified changes
        {
            switch (BattleState)
            {
                case BattleState.Initial:
                    {
                        planeDetectionController.TogglePlaneDetection(false);

                        break;
                    }
                case BattleState.HostScanningMap:
                    {
                        notification.text = "Waiting for Host's WorldMap";

                        break;
                    }
                case BattleState.SharingWorldMap:
                    {
                        notification.text = "Map Scanned. Receiving WorldMap";

#if UNITY_EDITOR
                        UpdateBattleState(BattleState.ClientScanningMap);
#endif

                        break;
                    }
                case BattleState.ClientScanningMap:
                    {
                        notification.text = "Please scan for the plane the host scanned until it is fully mapped";

                        planeDetectionController.TogglePlaneDetection(true);

                        break;
                    }
            }
        }
    }

    void ShowResult() {
        Debug.Log("ShowResult()");

        foreach (ARFighter fighter in FindObjectsOfType<ARFighter>())
        {
            if (fighter.photonView.Owner == PhotonNetwork.LocalPlayer)
            {
                SceneManagerResult.remainingHP = fighter.inputStr.healthPoint;
                SceneManagerResult.remainingMP = fighter.inputStr.magicPoint;

                // TODO: debuging
                // break;
                Debug.Log("Mine:");
            }
            else
            {
                Debug.Log("Others:");
            }

            Debug.Log("HP: " + fighter.inputStr.healthPoint);
            Debug.Log("MP: " + fighter.inputStr.magicPoint);
        }

        gameManager.NextScene();
    }

    // Battle (end)


    // AR

    void InitAR()
    {
        aRSession = FindObjectOfType<ARSession>();
        aRSessionOrigin = FindObjectOfType<ARSessionOrigin>();
        aRWorldMapController = FindObjectOfType<ARWorldMapController>();
        aRRaycastManager = FindObjectOfType<ARRaycastManager>();
        planeDetectionController = FindObjectOfType<PlaneDetectionController>();
        aRWorldMapController.mappingStatusText = GameObject.Find("MappingStatusText").GetComponent<TextMeshProUGUI>();

        aRSession.Reset();
    }

    void CheckTouchInput()
    {
#if UNITY_EDITOR
        ARFighter.SpawnARFighterEditor(ref LocalPlayer, PlayerPrefab, PhotonNetwork.LocalPlayer.UserId);

        UpdateBattleState(BattleState.Spawned);
#endif
        if (TryGetTouchPosition(out Vector2 touchPosition))
        {
            if (aRRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
            {
                // Raycast hits are sorted by distance, so the first one
                // will be the closest hit.
                selectedPlane = planeDetectionController.GetPlaneFromHit(hits[0]);
                Pose hitPose = hits[0].pose;

                planeDetectionController.TogglePlaneDetection(false);
                planeDetectionController.ShowOnlyOnePlane(selectedPlane.trackableId);

                ARFighter.SpawnARFighter(ref LocalPlayer, PlayerPrefab, hitPose, PhotonNetwork.LocalPlayer.UserId);

                UpdateBattleState(BattleState.Spawned);
            }
        }
    }

    protected bool TryGetTouchPosition(out Vector2 touchPosition)
    {

#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            // REAMRKS: block raycast at UI button
            if (!EventSystem.current.currentSelectedGameObject)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }
        }
#endif

        touchPosition = default;
        return false;
    }

    // AR (end)


    // Count Down

    protected void StartCountDown()
    {
        UpdateBattleState(BattleState.CountingDown);
    }

    void CountDown()
    {
        TimeLeft -= Time.deltaTime;

        notification.text = "Get Ready \n " + Mathf.RoundToInt(TimeLeft);

        if (TimeLeft <= 0)
        {
            UpdateBattleState(BattleState.Playing);
        }
    }

    // Count Down (end)


    // UI

    protected abstract void ToggleButtons();

    void UpdateTimer()
    {
        float timeLeft = battleManager.GetTimeLeft();

        if (timeLeft.Equals(float.PositiveInfinity))
        {
            timer.text = EnumUtil.ToString(TimeLimit.Infinite);
        }
        else
        {
            timer.text = Mathf.RoundToInt(timeLeft).ToString();
        }
    }

    void ToggleController(bool toggle)
    {
        foreach (Button button in controllerPanel.GetComponentsInChildren<Button>())
        {
            button.enabled = toggle;
        }

        controllerPanel.GetComponentInChildren<Joystick>().enabled = toggle;
    }

    void FadeInController()
    {
        if (!controllerActive)
        {
            controllerActive = true;
            StartCoroutine(FadeCanvas(controllerPanel, 0f, 1f, readyDuration));
        }
    }

    void FadeOutController()
    {
        if (controllerActive)
        {
            controllerActive = false;
            StartCoroutine(FadeCanvas(controllerPanel, 1f, 0f, readyDuration));
        }
    }

    public static IEnumerator FadeCanvas(CanvasGroup canvas, float startAlpha, float endAlpha, float duration)
    {
        float startTime = Time.time;
        float endTime = Time.time + duration;
        float elapsedTime = 0f;
        float percentage;

        canvas.alpha = startAlpha;

        while (Time.time <= endTime)
        {
            elapsedTime = Time.time - startTime;
            percentage = 1 / (duration / elapsedTime);
            if (startAlpha > endAlpha)
            {
                canvas.alpha = startAlpha - percentage;
            }
            else
            {
                canvas.alpha = startAlpha + percentage;
            }

            yield return new WaitForEndOfFrame();
        }

        canvas.alpha = endAlpha;

    }

    // UI


    // PUN callbacks

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);

        // REMARKS: host get updates from client
        if (PhotonNetwork.IsMasterClient)
        // if (PhotonNetwork.IsMasterClient && !target.IsMasterClient)
        {
            Debug.Log("Master: received OnPlayerPropertiesUpdate() from: " + target.NickName);
            Debug.Log("Master: current BattleState: " + BattleState);

            switch (BattleState)
            {
                case BattleState.Initial:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.Initial);

                        if (CheckClientsBattleState(BattleState.Initial))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.Initial + ". Master moving onto BattleState: " + BattleState.HostScanningMap);

                            UpdateBattleState(BattleState.HostScanningMap);
                        }

                        break;
                    }
                case BattleState.SharingWorldMap:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.ClientScanningMap);

                        if (CheckClientsBattleState(BattleState.ClientScanningMap))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.ClientScanningMap + ". Master moving onto BattleState: " + BattleState.ClientScanningMap);

                            UpdateBattleState(BattleState.ClientScanningMap);
                        }

                        break;
                    }
                case BattleState.Ready:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.Ready);

                        if (CheckClientsBattleState(BattleState.Ready))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.Ready + ". Master moving onto BattleState: " + BattleState.WaitingForSpawn);

                            UpdateBattleState(BattleState.WaitingForSpawn);
                        }

                        break;
                    }
                case BattleState.Spawned:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.Spawned);

                        if (CheckClientsBattleState(BattleState.Spawned))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.Spawned + ". Master moving onto BattleState: " + BattleState.AllPlayersReady);

                            UpdateBattleState(BattleState.AllPlayersReady);
                        }

                        break;
                    }
                case BattleState.Ended:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.Ended);

                        if (CheckClientsBattleState(BattleState.Ended))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.Ended);

                            EndGameByHost();
                        }

                        break;
                    }
                case BattleState.ShowResult:
                    {
                        Debug.Log("Master: checking client's BattleState: " + BattleState.ShowResult);

                        if (CheckClientsBattleState(BattleState.ShowResult))
                        {
                            Debug.Log("Master: All client is on BattleState: " + BattleState.ShowResult);

                            ShowResult();
                        }

                        break;
                    }
            }
        }
        else // REMARKS: client
        {
            Debug.Log("Client: received OnPlayerPropertiesUpdate() from: " + target.NickName);
            Debug.Log("Client: received BattleState: " + changedProps["BattleState"]);
            Debug.Log("Client: current BattleState: " + BattleState);

            if (target.IsMasterClient) // REMARKS: client get updates from host
            {
                if (changedProps["BattleState"] != null)
                {
                    switch (BattleState)
                    {
                        case BattleState.Initial:
                        case BattleState.HostScanningMap:
                        case BattleState.Ready:
                        case BattleState.Spawned:
                        case BattleState.AllPlayersReady:
                        case BattleState.CountingDown:
                        case BattleState.Playing:
                        case BattleState.Ended:
                            {
                                Debug.Log("Client: moving onto BattleState: " + changedProps["BattleState"]);

                                BattleState newBattleState = (BattleState)changedProps["BattleState"];

                                if (BattleState != newBattleState)
                                {
                                    UpdateBattleState(newBattleState);
                                }

                                break;
                            }
                    }
                }
            }
            else // REMARKS: client get updates from client itself
            {
                if (changedProps["BattleState"] != null)
                {
                    switch (BattleState)
                    {
                        case BattleState.ShowResult:
                            {
                                // REMARKS:
                                // Ensure the master receive client's showResult BattleState
                                // before client move on to next scene
                                ShowResult();

                                break;
                            }
                    }
                }
            }
        }
    }

    // PUN callbacks (end)


    // Host only

    bool CheckClientsBattleState(BattleState targetBattleState)
    {
        // REMARKS: only Host can call this function
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }
        
        foreach (Player player in PhotonNetwork.PlayerListOthers)
        {
            if (!player.IsMasterClient)
            {
                if (player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.BattleState)] == null)
                {
                    return false;
                }

                if ((BattleState)player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.BattleState)] != targetBattleState)
                {
                    return false;
                }
            }
        }

        return true;
    }
    
    protected void EndGameByHost()
    {
        // REMARKS: only Host can call this function
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        EndGameRequest endGameRequestParams = new EndGameRequest();

        endGameRequestParams.battle_mode = GameManager.BattleMode;
        endGameRequestParams.players = new EndGameRequestPlayer[PhotonNetwork.PlayerList.Length];

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            Player player = PhotonNetwork.PlayerList[i];
            EndGameRequestPlayer endGameRequestPlayer = new EndGameRequestPlayer();

            endGameRequestPlayer.user_id = (string) player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseUserId)];
            endGameRequestPlayer.hero_id = (string) player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseHeroId)];
            
            // REMARKS: Team.none if battleMode = BattleMode.Free
            endGameRequestPlayer.team = player.GetTeam();

            foreach (ARFighter fighter in FindObjectsOfType<ARFighter>())
            {
                if (fighter.photonView.Owner.Equals(player))
                {
                    endGameRequestPlayer.health_point = fighter.inputStr.healthPoint;
                    endGameRequestPlayer.magic_point = fighter.inputStr.magicPoint;

                    break;
                }
            }

            endGameRequestParams.players[i] = endGameRequestPlayer;
        }

        Debug.Log("endGameRequestParams:");
        Debug.Log(endGameRequestParams.ToString());
        
        // TODO: BUG:
        // when build to phone, can't get response
        // now use listener to get response
        RestAPIUtil.Post<EndGameResponse>(Endpoint.EndGame, endGameRequestParams);
        
        UpdateBattleState(BattleState.ShowResult);
    }

    // Host only (end)


    // Firebase

    void AddUsersListener()
    {
        usersListener = DB._db.Collection("User").Listen(async (snapshot) =>
        {
            Debug.Log("SceneManagerBattle: Callback received query snapshot from userListener");

            List<User> users = new List<User>();

            foreach(DocumentSnapshot documentSnapshot in snapshot.Documents)
            {
                User user = await UserDAO.getUser(documentSnapshot);

                if (FirebaseUtil.ValidUser(user))
                {
                    Debug.Log("SceneManagerBattle: checking user (status): " + user.id + " (" + user.status + ")");
                    users.Add(user);
                }
                else
                {
                    Debug.Log("SceneManagerBattle: Empty user: " + documentSnapshot.Id);
                }
            }

            // check all players are "in-game"
            List<string> playersFirebaseUserIds = new List<string>();
            foreach(Player player in PhotonNetwork.PlayerList)
            {
                string playerFirebaseUserId = (string)player.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseUserId)];
                playersFirebaseUserIds.Add(playerFirebaseUserId);
            }

            bool allUserInGame = true;
            foreach(string playerFirebaseUserId in playersFirebaseUserIds)
            {
                User userOfPlayer = users.Find(user => user.id == playerFirebaseUserId);

                if (userOfPlayer.status != EnumUtil.ToString(UserStatus.InGame))
                {
                    allUserInGame = false;
                    break;
                }
            }

            if (allUserInGame)
            {
                Debug.Log("SceneManagerBattle: all User/Player in-game");

                // add to Users
                Users = users;

                // stop lisener
                usersListener.Stop();

                Debug.Log("SceneManagerBattle: usersListener stopped");
            }
        });

        Debug.Log("SceneManagerBattle: added usersListener");
    }

    public static PlayerUserHeroBundle GetPlayerUserHeroBundle(string currentPlayerID)
    {
        if (string.IsNullOrEmpty(currentPlayerID))
        {
            Debug.Log("Cannot find currentPlayerID");
            return null;
        }
        Debug.Log("SceneManagerBattle: currentPlayerID: " + currentPlayerID);
        
        Player currentPlayer = Array.Find(PhotonNetwork.PlayerList, (player) => {
            Debug.Log("SceneManagerBattle: player.UserId (searching): " + player.UserId);

            return (player.UserId == currentPlayerID);
        });
        Debug.Log("SceneManagerBattle: currentPlayer: " + currentPlayer.UserId);

        string currentFirebaseUserId = (string)currentPlayer.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseUserId)];
        Debug.Log("SceneManagerBattle: currentFirebaseUserId: " + currentFirebaseUserId);

        string currentFirebaseHeroId = (string)currentPlayer.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseHeroId)];
        Debug.Log("SceneManagerBattle: currentFirebaseHeroId: " + currentFirebaseHeroId);

        User currentUser = SceneManagerBattle.Users.Find(user => user.id == currentFirebaseUserId);
        Debug.Log("SceneManagerBattle: currentUser: " + currentUser.id);

        Hero currentHero = GameManager.Heroes.Find(hero => hero.id == currentFirebaseHeroId);

        PlayerUserHeroBundle playerUserHeroBundle = new PlayerUserHeroBundle(currentPlayer, currentUser, currentHero);

        return playerUserHeroBundle;
    }

    // REMARKS: get the levelAdjustment for a specific Player
    public static int GetLevelAdjustment(string currentPlayerID)
    {
        PlayerUserHeroBundle playerUserHeroBundle = GetPlayerUserHeroBundle(currentPlayerID);
        
        return GetLevelAdjustment(playerUserHeroBundle.user, playerUserHeroBundle.hero);
    }

    public static int GetLevelAdjustment(User currentUser, Hero currentHero)
    {
        UserHero currentUserHero = currentUser.heros.Find(userHero => userHero.heroRef.Id == currentHero.id);

        int levelAdjustment = (currentHero.level_factor * (currentUserHero.level - 1));

        Debug.Log("SceneManagerBattle: currentHero: " + currentHero.fighter_color);
        Debug.Log("SceneManagerBattle: currentHero (level factor): " + currentHero.level_factor);
        Debug.Log("SceneManagerBattle: currentUserHero: " + currentUserHero.id);
        Debug.Log("SceneManagerBattle: currentUserHero (level): " + currentUserHero.level);
        Debug.Log("SceneManagerBattle: levelAdjustment: " + levelAdjustment);
        
        return levelAdjustment;
    }

    // Firebase (end)
}

public class PlayerUserHeroBundle
{
    public readonly Player player;
    public readonly User user;
    public readonly Hero hero;

    public PlayerUserHeroBundle(Player player, User user, Hero hero)
    {
        this.player = player;
        this.user = user;
        this.hero = hero;
    }
}
