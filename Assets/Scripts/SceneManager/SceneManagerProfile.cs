﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SceneManagerProfile : MonoBehaviour
{
    public TextMeshProUGUI userName_Text;
    public TextMeshProUGUI money_Text;
    public TextMeshProUGUI score_Text;
    public TextMeshProUGUI heroName_Text;
    public TextMeshProUGUI heroType_Text;
    public TextMeshProUGUI heroExp_Text;
    public TextMeshProUGUI heroLevel_Text;
    public TextMeshProUGUI heroHP_Text;
    public TextMeshProUGUI heroMP_Text;
    public TextMeshProUGUI heroDamage_Text;
    public TextMeshProUGUI heroRecovery_Text;
    public TextMeshProUGUI heroSpeed_Text;
    public TextMeshProUGUI heroPrice_Text;
    public TextMeshProUGUI heroDescription_Text;
    public TextMeshProUGUI notification_Text;

    
    public ModelDisplay[] modelDisplays;
    public GameObject fighterDisplayBox;
    public GameObject magicAttackProjectileDisplayBox;
    public GameObject magicAttackAOEDisplayBox;
    private LFARButton displayControllerLeftBtn;
    private LFARButton displayControllerRightBtn;
    private Image fighterDisplayBoxLock;
    private Image magicAttackProjectileDisplayBoxLock;
    private Image magicAttackAOEDisplayBoxLock;
    private Animator[] animators;

    public LFARButton buyBtn;
    public LFARButton upgradeBtn;
    public LFARButton signOutBtn;
    public LFARButton lobbyBtn;
    public LFARButton leaderboardBtn;

    private GameManager gameManager;
    private FighterColor fighterColor;
    private Hero currentHero;
    private UserHero currentUserHero;
    private bool buyHero;
    private bool levelUp;


    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        InitUI();
    }

    private void Update()
    {
        if (buyHero && (GameManager.User.status == EnumUtil.ToString(UserStatus.BuyHero)))
        {
            buyHero = false;

            FinishAPICall(Endpoint.BuyHero);

            notification_Text.text = "Unlocked Hero!";
        }

        if (levelUp && (GameManager.User.status == EnumUtil.ToString(UserStatus.LevelUp)))
        {
            levelUp = false;

            FinishAPICall(Endpoint.LevelUp);

            notification_Text.text = "Level Up!";
        }
    }


    // UI

    private void InitUI()
    {
        displayControllerLeftBtn = fighterDisplayBox.GetComponentsInChildren<LFARButton>()[0];
        displayControllerRightBtn = fighterDisplayBox.GetComponentsInChildren<LFARButton>()[1];
        fighterDisplayBoxLock = fighterDisplayBox.GetComponentsInChildren<Image>()[2];
        magicAttackProjectileDisplayBoxLock = magicAttackProjectileDisplayBox.GetComponentInChildren<Image>();
        magicAttackAOEDisplayBoxLock = magicAttackAOEDisplayBox.GetComponentInChildren<Image>();

        animators = modelDisplays[0].gameObject.GetComponentsInChildren<Animator>();
        
        fighterColor = FighterColor.Red;
        
        UpdateUserInfo();

        UpdateHeroInfo();
        
        AddButtonListener();
    }

    private void AddButtonListener()
    {
        displayControllerLeftBtn.onClick.AddListener(DisplayMoveLeft);
        displayControllerRightBtn.onClick.AddListener(DisplayMoveRight);
        buyBtn.onClick.AddListener(BuyHero);
        upgradeBtn.onClick.AddListener(UpgradeHero);
        signOutBtn.onClick.AddListener(SignOut);
        lobbyBtn.onClick.AddListener(LoadSceneLobby);
        leaderboardBtn.onClick.AddListener(LoadSceneLeaderboard);
    }

    private void UpdateUserInfo()
    {
        userName_Text.text = GameManager.User.name;
        money_Text.text = GameManager.User.money.ToString();
        score_Text.text = GameManager.User.score.ToString();
    }

    private void UpdateHeroInfo(Endpoint? endpoint = null)
    {
        currentHero = GameManager.Heroes.Find(hero => hero.fighter_color == fighterColor);
        currentUserHero = GameManager.User.heros.Find(userHero => userHero.heroRef.Id == currentHero.id);
        
        heroName_Text.text = currentHero.name;
        heroType_Text.text = currentHero.type;
        heroHP_Text.text = currentHero.health_point.ToString();
        heroMP_Text.text = currentHero.magic_point.ToString();
        heroDamage_Text.text = currentHero.attack_damage.ToString();
        heroRecovery_Text.text = currentHero.magic_point_recover_speed.ToString();
        heroSpeed_Text.text = currentHero.move_speed.ToString();
        heroDescription_Text.text = currentHero.description;
        notification_Text.text = "";

        string ColorGrey = "#808080ff";
        
        if (currentUserHero == null) // locked
        {
            LockDisplayBox(true);

            buyBtn.gameObject.SetActive(true);
            upgradeBtn.gameObject.SetActive(false);

            heroExp_Text.text = "<color=" + ColorGrey + ">/</color>";
            heroLevel_Text.text = "<color=" + ColorGrey + ">/</color>";
            heroPrice_Text.text = "<color=yellow>" + currentHero.price.ToString() + "</color>";
        }
        else // unlocked
        {
            LockDisplayBox(false);

            buyBtn.gameObject.SetActive(false);;
            upgradeBtn.gameObject.SetActive(true);;

            int levelAdjustment = currentHero.level_factor * (currentUserHero.level - 1);
            
            string heroExpString = currentUserHero.exp.ToString();
            string heroLevelString = currentUserHero.level.ToString();

            if (endpoint == Endpoint.BuyHero || endpoint == Endpoint.LevelUp)
            {
                heroExpString = "<color=#00ffff>" + heroExpString + "</color>";
                heroLevelString = "<color=#00ffff>" + heroLevelString + "</color>";
            }

            heroExp_Text.text = heroExpString + " / " + currentHero.max_exp;
            heroLevel_Text.text = heroLevelString + " / " + currentHero.max_level;
            heroHP_Text.text += GetLevelAdjustmentString(levelAdjustment * ARFighter.LevelFactorHealthPoint);
            heroMP_Text.text += GetLevelAdjustmentString(levelAdjustment * ARFighter.LevelFactorMagicPoint);
            heroDamage_Text.text += GetLevelAdjustmentString(levelAdjustment * ARFighter.LevelFactorDamage);
            heroRecovery_Text.text += GetLevelAdjustmentString(levelAdjustment * ARFighter.LevelFactorMagicPointRecoverSpeed);
            heroSpeed_Text.text += GetLevelAdjustmentString(levelAdjustment * ARFighter.LevelFactorMoveSpeed);
            heroPrice_Text.text = "<color=" + ColorGrey + ">/</color>";
        }

        foreach (Animator animator in animators)
        {
            animator.SetBool("victory", false);
        }
    }

    private string GetLevelAdjustmentString(float levelAdjustment)
    {
        return ("<color=green> + " + String.Format("{0:0.##}", levelAdjustment) + "</color>");
    }

    private void DisplayMoveLeft()
    {
        if (fighterColor == FighterColor.Red)
        {
            fighterColor = FighterColor.Purple;
        }
        else
        {
            fighterColor--;
        }

        DisplayMoveTo(fighterColor);
    }

    private void DisplayMoveRight()
    {
        if (fighterColor == FighterColor.Purple)
        {
            fighterColor = FighterColor.Red;
        }
        else
        {
            fighterColor++;
        }

        DisplayMoveTo(fighterColor);
    }

    private void DisplayMoveTo(FighterColor fighterColor)
    {
        for (int i = 0; i < modelDisplays.Length; i++)
        {
            modelDisplays[i].DisplayMoveTo(fighterColor, i);
        }

        UpdateHeroInfo();
    }

    private void LockDisplayBox(bool lockDisplayBox)
    {
        fighterDisplayBoxLock.enabled = lockDisplayBox;
        magicAttackProjectileDisplayBoxLock.enabled = lockDisplayBox;
        magicAttackAOEDisplayBoxLock.enabled = lockDisplayBox;
    }

    private void DisableButtons()
    {
        buyBtn.Disable();
        upgradeBtn.Disable();
        signOutBtn.Disable();
        lobbyBtn.Disable();
        leaderboardBtn.Disable();
    }

    private void EnableButtons()
    {
        buyBtn.Enable();
        upgradeBtn.Enable();
        signOutBtn.Enable();
        lobbyBtn.Enable();
        leaderboardBtn.Enable();
    }

    private void FinishAPICall(Endpoint endpoint)
    {
        UpdateUserInfo();
        UpdateHeroInfo(endpoint);

        EnableButtons();

        animators[(int)fighterColor].SetBool("victory", true);

        FirebaseUtil.SetUserStatus(UserStatus.Idle);
    }

    // UI (end)


    // Button Listener

    private void BuyHero()
    {
        if (GameManager.User.money < currentHero.price)
        {
            notification_Text.text = "Not enough money :(";
            return;
        }

        DisableButtons();
        
        buyHero = true;
        
        BuyHeroRequest buyHeroRequest = new BuyHeroRequest(currentHero.id);

        RestAPIUtil.Post<BuyHeroResponse>(Endpoint.BuyHero, buyHeroRequest);
    }

    private void UpgradeHero()
    {
        if (currentUserHero.exp < currentHero.max_exp)
        {
            notification_Text.text = "Not enough exp :(";
            return;
        }
        else if (currentUserHero.level >= currentHero.max_level)
        {
            notification_Text.text = "Already max level :)";
            return;
        }

        DisableButtons();
        
        levelUp = true;

        LevelUpRequest levelUpRequest = new LevelUpRequest(currentUserHero.id);

        RestAPIUtil.Post<LevelUpResponse>(Endpoint.LevelUp, levelUpRequest);
    }

    private void SignOut()
    {
        DisableButtons();
        
        FirebaseUtil.SignOut();
    }

    // Button Listener (end)
    

    // Scene Management

    private void LoadSceneLobby()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Lobby);
    }

    private void LoadSceneLeaderboard()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Leaderboard);
    }

    // Scene Management (end)
}
