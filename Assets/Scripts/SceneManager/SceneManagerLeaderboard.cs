using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Firebase.Firestore;
using Firebase.Extensions;

public class SceneManagerLeaderboard : MonoBehaviour
{
    public ScrollRect scrollView;
    public GameObject scrollContent;
    public GameObject playerScoreListItemPrefab;
    public LFARButton profileBtn;
    public LFARButton lobbyBtn;

    private GameManager gameManager;
    private Dictionary<string, string> playerScoreDict = new Dictionary<string, string>();

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        InitUI();
        AddLeaderboardListener();
    }

    private void InitUI()
    { 
        // REMARKS: scroll to the top of the scrollView
        scrollView.verticalNormalizedPosition = 1;

        profileBtn.onClick.AddListener(LoadSceneProfile);
        lobbyBtn.onClick.AddListener(LoadSceneLobby);
    }


    private void AddLeaderboardListener()
    {
        Query query = DB._db.Collection("User").OrderByDescending("score");
        ListenerRegistration listener = query.Listen(snapshot => {
            // When score of any user change, update the leaderboard
            UpdateList();
        });
    }
    
    private void UpdateList()
    {
        // Clear the dictionary that used to store the player,score pair
        playerScoreDict.Clear();

        // Fetch score of all users from Firestore
        Query query = DB._db.Collection("User").OrderByDescending("score");
        query.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            QuerySnapshot userQuerySnapshot = task.Result;
            foreach (DocumentSnapshot documentSnapshot in userQuerySnapshot.Documents)
            {
                string name = "";
                string score = "";
                // Debug.Log(String.Format("Document data for {0} document:", documentSnapshot.Id));
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user)
                {
                    //Debug.Log(String.Format("{0}: {1}", pair.Key, pair.Value));
                    if (pair.Key == "name")
                    {
                        name = pair.Value.ToString();
                    }
                    if (pair.Key == "score")
                    {
                        score = pair.Value.ToString();
                    }
                }
                playerScoreDict[name] = score;
            }
            // Dynamically add prefab for each play score,pair
            RenderList();
        });

    }

    private void RenderList()
    {
        // Clear the leaderboard before update
        ClearLeaderboard();
        int i = 1;
        foreach (KeyValuePair<string, string> playerScore in playerScoreDict)
        {
            GenerateItem(playerScore.Key, playerScore.Value, i.ToString());
            i++;
        }
    }


    private void GenerateItem(string playerName, string score, string rank)
    {

        GameObject scrollItemObj = Instantiate(playerScoreListItemPrefab, transform);
        scrollItemObj.transform.SetParent(scrollContent.transform, false);
        scrollItemObj.transform.Find("Rank").gameObject.GetComponent<TextMeshProUGUI>().text = rank;
        scrollItemObj.transform.Find("PlayerName").gameObject.GetComponent<TextMeshProUGUI>().text = playerName;
        scrollItemObj.transform.Find("Score").gameObject.GetComponent<TextMeshProUGUI>().text = score;
        if(playerName == GameManager.User.name)
        {
            scrollItemObj.GetComponent<Image>().color = new Color32(0, 255, 255, 255);
        }

    }

    void ClearLeaderboard()
    {
        Button[] items = scrollContent.GetComponentsInChildren<Button>();
        foreach (Button item in items)
        {
            Destroy(item.gameObject);
        }
    }

    private void DisableButtons()
    {
        profileBtn.Disable();
        lobbyBtn.Disable();
    }


    private void LoadSceneProfile()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Profile);
    }

    private void LoadSceneLobby()
    {
        DisableButtons();

        gameManager.LoadScene(GameStage.Lobby);
    }

}
