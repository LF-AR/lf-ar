using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine;
using TMPro;

public class SceneManagerRoomCreate : MonoBehaviour
{
    public TMP_InputField roomNameInput;
    public TMP_Dropdown gameModeDropdown;
    public TMP_Dropdown battleModeDropdown;
    public TMP_Dropdown timeLimitDropdown;
    public TMP_Dropdown maxNoOfPlayersDropDown;
    public LFARButton backBtn;
    public LFARButton createBtn;

    private GameManager gameManager;
    private NetworkConnectionManager networkConnectionManager;
    private string roomName;
    private bool roomCreating;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        networkConnectionManager = FindObjectOfType<NetworkConnectionManager>();

        InitUI();
    }

    private void Update()
    {
        if (roomCreating && networkConnectionManager.GetConnectionState() == ConnectionState.Joined)
        {
            gameManager.NextScene();
        }
    }

    private void InitUI() 
    {
        AddDropDownOptions();

        createBtn.Disable();

        roomNameInput.onValueChanged.AddListener(OnRoomNameChanged);
        createBtn.onClick.AddListener(CreateRoom);
        backBtn.onClick.AddListener(Back);
    }

    void AddDropDownOptions()
    {
        List<string> gameModeDropdownOptions = new List<string>();
        List<string> battleModeDropdownOptions = new List<string>();
        List<string> maxNoOfPlayersDropDownOptions = new List<string>();
        List<string> timeLimitDropDownOptions = new List<string>();

        foreach (GameMode gameMode in Enum.GetValues(typeof(GameMode)))
        {
            string gameModeDropdownOption = "";
            gameModeDropdownOption = EnumUtil.ToString(gameMode);

            gameModeDropdownOptions.Add(gameModeDropdownOption);
        }

        foreach (BattleMode battleMode in Enum.GetValues(typeof(BattleMode)))
        {
            string battleModeDropdownOption = "";
            battleModeDropdownOption = EnumUtil.ToString(battleMode);

            battleModeDropdownOptions.Add(battleModeDropdownOption);
        }
        for (int i = 2; i <= GameManager.MaxPlayerInRoom; i++)
        {
            maxNoOfPlayersDropDownOptions.Add(i.ToString());
        }

        foreach (TimeLimit timeLimit in Enum.GetValues(typeof(TimeLimit)))
        {
            timeLimitDropDownOptions.Add(EnumUtil.ToString(timeLimit));
        }

        gameModeDropdown.AddOptions(gameModeDropdownOptions);
        battleModeDropdown.AddOptions(battleModeDropdownOptions);
        maxNoOfPlayersDropDown.AddOptions(maxNoOfPlayersDropDownOptions);
        timeLimitDropdown.AddOptions(timeLimitDropDownOptions);
    }

    void OnRoomNameChanged(string input)
    {
        roomName = input;

        if (roomName == "")
        {
            createBtn.Disable();
        }
        else
        {
            createBtn.Enable();
        }

    }

    void CreateRoom()
    {
        createBtn.Disable();

        roomCreating = true;

        string gameMode = gameModeDropdown.options[gameModeDropdown.value].text;
        string battleMode = battleModeDropdown.options[battleModeDropdown.value].text;
        string timeLimit = timeLimitDropdown.options[timeLimitDropdown.value].text;

        string roomCustomPropertyHostNickName = EnumUtil.ToString(RoomCustomProperty.HostNickName);
        string roomCustomPropertyGameMode = EnumUtil.ToString(RoomCustomProperty.GameMode);
        string roomCustomPropertyBattleMode = EnumUtil.ToString(RoomCustomProperty.BattleMode);
        string roomCustomPropertyTimeLimit = EnumUtil.ToString(RoomCustomProperty.TimeLimit);

        string[] customRoomPropertiesForLobby = new string[4];
        customRoomPropertiesForLobby[0] = roomCustomPropertyHostNickName;
        customRoomPropertiesForLobby[1] = roomCustomPropertyGameMode;
        customRoomPropertiesForLobby[2] = roomCustomPropertyBattleMode;
        customRoomPropertiesForLobby[3] = roomCustomPropertyTimeLimit;

        Hashtable customRoomProperties = new Hashtable
        {
            { roomCustomPropertyHostNickName, GameManager.User.name },
            { roomCustomPropertyGameMode , EnumUtil.StringToEnum(gameMode)},
            { roomCustomPropertyBattleMode, EnumUtil.StringToEnum(battleMode)},
            { roomCustomPropertyTimeLimit, EnumUtil.StringToEnum(timeLimit)}
        };

        RoomOptions roomOptions = new RoomOptions
        {
            MaxPlayers = (byte)(maxNoOfPlayersDropDown.value + 2),
            PublishUserId = true,
            CustomRoomProperties = customRoomProperties,
            CustomRoomPropertiesForLobby = customRoomPropertiesForLobby,
        };

        networkConnectionManager.CreateRoom(roomName, roomOptions);
    }

    void Back()
    {
        backBtn.Disable();

        gameManager.LoadScene(GameStage.Lobby);
    }
}
