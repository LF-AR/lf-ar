using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using static Photon.Pun.PhotonNetwork;
using static Photon.Pun.UtilityScripts.PunTeams;
using TMPro;

public class SceneManagerResult : MonoBehaviourPunCallbacks
{
    public static readonly float winnerIncreaseScoreAmount = 10.0f;

    public static float remainingHP;
    public static float remainingMP;
    
    public Button lobbyButton;
    public Camera displayCamera;
    public GameObject displaySpotlight;
    public GameObject fighterDisplay;
    public GameObject fighterDisplayBox;
    public GameObject resultInfo;
    public TextMeshProUGUI money_Text;
    public TextMeshProUGUI moneyChange_Text;
    public TextMeshProUGUI score_Text;
    public TextMeshProUGUI scoreChange_Text;
    public TextMeshProUGUI exp_Text;
    public TextMeshProUGUI expChange_Text;
    public TextMeshProUGUI playerName_Text;
    public TextMeshProUGUI result_Text;
    public TextMeshProUGUI HP_Text;
    public TextMeshProUGUI MP_Text;
    public TextMeshProUGUI notification_Text;
    public Image healthPointBar;
    public Image magicPointBar;
    public Image teamColor;

    private Animator[] animators;
    
    private GameManager gameManager;
    private NetworkConnectionManager networkConnectionManager;
    private PunTeams punTeams;
    private int maxPlayers;
    private Team winningTeam;
    private List<Player> winningFreePlayers;
    private bool roomLeaving;
    private bool resultGenerated;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        networkConnectionManager = FindObjectOfType<NetworkConnectionManager>();
        punTeams = FindObjectOfType<PunTeams>();

        maxPlayers = GameManager.MaxPlayerInRoom;

        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (ReadyToGenerateResult())
        {
            resultGenerated = true;

            GenerateResult();
        }
        
        // leave room call back
        if (roomLeaving && networkConnectionManager.GetConnectionState() == ConnectionState.ConnectedMaster)
        {
            gameManager.LoadScene(GameStage.Lobby);
        }
    }

    private void InitUI()
    {
        ToggleNotification(true);
        
        notification_Text.text = "Generating result...";
        
        animators = fighterDisplay.gameObject.GetComponentsInChildren<Animator>();

        lobbyButton.onClick.AddListener(BackToLobby);
    }

    private void ToggleNotification(bool toggle)
    {
        fighterDisplayBox.SetActive(!toggle);
        resultInfo.SetActive(!toggle);
        notification_Text.gameObject.SetActive(toggle);
    }

    private bool ReadyToGenerateResult()
    {
        return (!resultGenerated && (GameManager.User.status == EnumUtil.ToString(UserStatus.EndGame)));
    }

    private UserHero FindUserHero(User user, string heroId)
    {
        return user.heros.Find(hero => hero.heroRef.Id == heroId);
    }

    private string GetChangeString(float change)
    {
        if (change > 0)
        {
            return "<color=green>(+" + change.ToString() + ")</color>";
        }
        else if (change < 0)
        {
            return "<color=red>(" + change.ToString() + ")</color>";
        }
        else
        {
            return "<color=white>(+" + change.ToString() + ")</color>";
        }
    }

    void GenerateResult()
    {
        // variables
        string playerHeroId = (string)PhotonNetwork.LocalPlayer.CustomProperties[EnumUtil.ToString(PlayerCustomProperty.FirebaseHeroId)];
        Hero playerHero = GameManager.Heroes.Find(hero => hero.id == playerHeroId);

        User currentUser = GameManager.User;
        UserHero currentUserHero = FindUserHero(currentUser, playerHeroId);

        User prevUser = GameManager.PrevUser;
        UserHero prevUserHero = FindUserHero(prevUser, playerHeroId);

        // display
        DisplayMoveTo(GameManager.FighterColor);

        // name
        playerName_Text.text = GameManager.User.name;

        // HP & MP  (image & text)
        float levelAdjustment = SceneManagerBattle.GetLevelAdjustment(currentUser, playerHero);

        float initialHealthPoint = playerHero.health_point + (levelAdjustment * ARFighter.LevelFactorHealthPoint);
        float initialMagicPoint = playerHero.magic_point + (levelAdjustment * ARFighter.LevelFactorMagicPoint);

        healthPointBar.fillAmount = remainingHP / initialHealthPoint;
        HP_Text.text = Mathf.RoundToInt(remainingHP) + " / " + Mathf.RoundToInt(initialHealthPoint);
        magicPointBar.fillAmount = remainingMP / initialMagicPoint;
        MP_Text.text = Mathf.RoundToInt(remainingMP) + " / " + Mathf.RoundToInt(initialMagicPoint);

        // team
        if (GameManager.BattleMode == BattleMode.Team)
        {
            Team team = PhotonNetwork.LocalPlayer.GetTeam();

            if (team == Team.red)
            {
                teamColor.color = Color.red;
            }
            else if (team == Team.blue)
            {
                teamColor.color = Color.blue;
            }
        }
        else
        {
            teamColor.enabled = false;
        }

        // check changes
        float moneyChange = currentUser.money - prevUser.money;
        float scoreChange = currentUser.score - prevUser.score;
        float expChange = currentUserHero.exp - prevUserHero.exp;
        
        // money & change
        money_Text.text = currentUser.money.ToString();
        moneyChange_Text.text = GetChangeString(moneyChange);

        // score & change
        score_Text.text = currentUser.score.ToString();
        scoreChange_Text.text = GetChangeString(scoreChange);
        
        // exp & change
        exp_Text.text = currentUserHero.exp.ToString();
        expChange_Text.text = GetChangeString(expChange);

        // win/draw/lose
        // use winnerIncreaseScoreAmount to determine win/draw/lose
        if (scoreChange == winnerIncreaseScoreAmount) // win
        {
            SetWin();
        }
        else if (scoreChange == 0) // draw
        {
            SetDraw();
        }
        else if (scoreChange == -winnerIncreaseScoreAmount)// lose
        {
            SetLose();
        }

        ToggleNotification(false);
    }

    void SetWin()
    {
        result_Text.text = "Winner";
        foreach (Animator animator in animators)
        {
            animator.SetBool("victory", true);
        }
    }

    void SetLose()
    {
        result_Text.text = "Defeated";
        foreach (Animator animator in animators)
        {
            animator.Play("sleep", -1);
        }
    }

    void SetDraw()
    {
        result_Text.text = "Draw";
    }

    // REMARKS: move the display camera to show the correct fighter color
    void DisplayMoveTo(FighterColor fighterColor, int index = 0)
    {
        Vector3 pos = displaySpotlight.transform.position;
        displaySpotlight.transform.position = new Vector3((float)fighterColor + 10 * (index + 1), pos.y, pos.z);

        pos = displayCamera.transform.position;
        displayCamera.transform.position = new Vector3((float)fighterColor + 10 * (index + 1), pos.y, pos.z);
    }

    void BackToLobby()
    {
        // REMARKS: reset result
        remainingHP = 0;
        remainingMP = 0;
        
        // REMARKS: reset all player's pun team
        if (GameManager.BattleMode == BattleMode.Team)
        {
            foreach (Player player in PlayerList)
            {
                player.SetTeam(Team.none);
            }

            punTeams.enabled = false;
        }

        FirebaseUtil.SetUserStatus(UserStatus.Idle);

        roomLeaving = true;

        networkConnectionManager.LeaveRoom();
    }
}
