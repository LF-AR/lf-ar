using UnityEngine;

public class SceneManagerStart : MonoBehaviour
{
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        gameManager.NextScene();
    }
}
