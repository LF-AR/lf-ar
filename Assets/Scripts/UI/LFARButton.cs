﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LFARButton : Button
{
    AudioManager audioManager;
    TextMeshProUGUI buttonText;
    Color disableColor = Color.gray;
    Color enableColor = Color.white;

    protected override void Awake()
    {
        base.Awake();

        audioManager = FindObjectOfType<AudioManager>();
        buttonText = GetComponentInChildren<TextMeshProUGUI>();

        onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        audioManager.Play("ButtonHit");
    }

    public void Disable()
    {
        interactable = false;
        buttonText.color = disableColor;
    }

    public void Enable()
    {
        interactable = true;
        buttonText.color = enableColor;
    }
}
