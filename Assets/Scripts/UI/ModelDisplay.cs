﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelDisplay : MonoBehaviour
{
    public Camera displayCamera;
    public GameObject displaySpotlight;

    public float animationSpeed;

    public List<FighterColor> userFighterColors;

    // Start is called before the first frame update
    void Start()
    {
        userFighterColors = new List<FighterColor>();

        foreach (UserHero userHero in GameManager.User.heros)
        {
            userFighterColors.Add(
                GameManager.Heroes.Find(hero =>
                    hero.id == userHero.heroRef.Id
                ).fighter_color
            );
        }

        userFighterColors.Sort((a, b) => a < b ? -1 : 1);
    }

    public void DisplayMoveTo(FighterColor fighterColor, int index = 0)
    {
        Vector3 pos;
        Vector3 newPos;

        pos = displaySpotlight.transform.position;
        newPos = new Vector3((float)fighterColor + 10 * (index + 1), pos.y, pos.z);
        StartCoroutine(LightLerpFromTo(displaySpotlight, pos, newPos, animationSpeed));

        pos = displayCamera.transform.position;
        newPos = new Vector3((float)fighterColor + 10 * (index + 1), pos.y, pos.z);
        StartCoroutine(CameraLerpFromTo(displayCamera, pos, newPos, animationSpeed));
    }

    public static IEnumerator LightLerpFromTo(GameObject spotlight, Vector3 pos1, Vector3 pos2, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            spotlight.transform.position = Vector3.Lerp(pos1, pos2, t / duration);
            yield return 0;
        }
        spotlight.transform.position = pos2;
    }

    public static IEnumerator CameraLerpFromTo(Camera camera, Vector3 pos1, Vector3 pos2, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            camera.transform.position = Vector3.Lerp(pos1, pos2, t / duration);
            yield return 0;
        }
        camera.transform.position = pos2;
    }

}
