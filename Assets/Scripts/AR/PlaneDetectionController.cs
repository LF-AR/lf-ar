﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// This example demonstrates how to toggle plane detection,
/// and also hide or show the existing planes.
/// </summary>
[RequireComponent(typeof(ARPlaneManager))]
public class PlaneDetectionController : MonoBehaviour
{
    /// <summary>
    /// Toggles plane detection.
    /// </summary>
    public void TogglePlaneDetection(bool enabled)
    {
        m_ARPlaneManager.detectionMode = enabled ? PlaneDetectionMode.Horizontal : PlaneDetectionMode.None;
        //m_ARPlaneManager.enabled = enabled;
    }

    /// <summary>
    /// Iterates over all the existing planes and activates
    /// or deactivates their <c>GameObject</c>s'.
    /// </summary>
    /// <param name="value">Each planes' GameObject is SetActive with this value.</param>
    public void SetAllPlanesActive(bool value)
    {
        foreach (var plane in m_ARPlaneManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
    }

    void Awake()
    {
        m_ARPlaneManager = GetComponent<ARPlaneManager>();
    }

    public ARPlane GetPlaneFromHit(ARRaycastHit hit)
    {
        return m_ARPlaneManager.GetPlane(hit.trackableId);
    }

    public void ShowOnlyOnePlane(TrackableId trackableId)
    {
        foreach (ARPlane plane in m_ARPlaneManager.trackables)
            plane.gameObject.SetActive(plane.trackableId == trackableId);
    }

    public void UpdatePlaneValidity(Vector3 targetPlayingFieldSize, out bool containValidPlane)
    {
        containValidPlane = false;
        foreach (ARPlane plane in m_ARPlaneManager.trackables)
        {
            MeshRenderer mesh = plane.gameObject.GetComponent<MeshRenderer>();

            if (targetPlayingFieldSize.x <= mesh.bounds.size.x && targetPlayingFieldSize.z <= mesh.bounds.size.z)
            {
                mesh.material = selectedPlaneMaterial;
                containValidPlane = true;
            }
            else
            {
                mesh.material = disabledPlaneMaterial;
            }
        }
    }

    ARPlaneManager m_ARPlaneManager;
    public Material selectedPlaneMaterial;
    public Material disabledPlaneMaterial;
}
