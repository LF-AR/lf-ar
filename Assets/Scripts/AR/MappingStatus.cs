﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

public class MappingStatus : MonoBehaviour 
{

	public ARWorldMappingStatus arWorldMappingStatus;
	public ARTrackingState arTrackingState;
    public ARTrackingStateReason arTrackingStateReason;

    //public Text mappingStatusText;

	// Use this for initialization
	void Start () 
	{
		UnityARSessionNativeInterface.ARFrameUpdatedEvent += CheckWorldMapStatus;
	}

	void CheckWorldMapStatus(UnityARCamera cam)
	{
		arWorldMappingStatus = cam.worldMappingStatus;
        arTrackingState = cam.trackingState;
        arTrackingStateReason = cam.trackingReason;

        //mappingStatusText.text = cam.worldMappingStatus.ToString() + " " + cam.trackingReason.ToString();
	}

	void OnDestroy()
	{
		UnityARSessionNativeInterface.ARFrameUpdatedEvent -= CheckWorldMapStatus;
	}

}
