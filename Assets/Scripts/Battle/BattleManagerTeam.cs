﻿using static Photon.Pun.UtilityScripts.PunTeams;
using Photon.Realtime;

public class BattleManagerTeam : BattleManager
{
    public override bool CheckEndGame()
    {
        if (CheckEndTime())
        {
            return true;
        }

        int blueRemaining = 0;
        foreach (Player player in PlayersPerTeam[Team.blue])
        {
            if (CheckFighterAlive(player))
            {
                blueRemaining++;
            }
        }

        int redRemaining = 0;
        foreach (Player player in PlayersPerTeam[Team.red])
        {
            if (CheckFighterAlive(player))
            {
                redRemaining++;
            }
        }

        return blueRemaining == 0 || redRemaining == 0;
    }
}
