﻿using static Photon.Pun.PhotonNetwork;
using Photon.Realtime;

public class BattleManagerFree : BattleManager
{
    public override bool CheckEndGame()
    {
        if (CheckEndTime())
        {
            return true;
        }

        int remaining = 0;

        foreach (Player player in PlayerList)
        {
            if (CheckFighterAlive(player))
            {
                remaining++;
            }
        }

        if (remaining <= 1)
        {
            return true;
        }

        return false;
    }
}
