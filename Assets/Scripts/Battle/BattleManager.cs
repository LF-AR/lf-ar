﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public abstract class BattleManager : MonoBehaviourPunCallbacks
{
    private float timeLeft;

    public abstract bool CheckEndGame();

    public void SetTimeLeft(float time)
    {
        timeLeft = time;
    }

    public float GetTimeLeft()
    {
        return timeLeft;
    }

    protected bool CheckEndTime()
    {
        if (timeLeft.Equals(float.PositiveInfinity))
        {
            return false;
        }

        timeLeft -= Time.deltaTime;

        if (timeLeft <= 0)
        {
            return true;
        }

        return false;
    }

    protected bool CheckFighterAlive(Player player)
    {
        // REMARKS: match player and fighter
        foreach (ARFighter fighter in FindObjectsOfType<ARFighter>())
        {
            if (fighter.photonView.Owner.Equals(player))
            {
                if (fighter.inputStr.healthPoint > 0)
                {
                    return true;
                }

                return false;
            }
        }

        return false;
    }
}