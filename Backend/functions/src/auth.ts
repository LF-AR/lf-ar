import * as admin from "firebase-admin";

export function authorizeUser(req: any, res: any, next: any) {
	admin
		.auth()
		.verifyIdToken(req.headers.authorization)
		.then((decodedToken: admin.auth.DecodedIdToken) => {
			if (req.headers.user_id === decodedToken.uid) {
				next();
			} else {
				return res.json({
					success: false,
					error: "This token does not belong to the sender",
				});
			}
		})
		.catch((e) => {
			return res.json({
				success: false,
				error: (<Error>e).message,
			});
		});
}
