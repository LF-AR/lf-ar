export enum UserStatus {
	Init = "init",
	Idle = "idle",
	InGame = "in-game",
	BuyHero = "buy-hero",
	LevelUp = "level-up",
	EndGame = "end-game",
}

export enum BattleMode {
	Free,
	Team,
}

export enum PunTeam {
	none,
	red,
	blue,
}
