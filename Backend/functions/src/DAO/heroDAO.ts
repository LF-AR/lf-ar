import { Hero } from "../model/hero";
import { firestore } from "firebase-admin";
import { getDb } from "./db";

export class HeroDAO {
	static getHeroRef(heroId: string): firestore.DocumentReference {
		return getDb().doc(`Hero/${heroId}`);
	}

	static async getHero(heroId: string): Promise<Hero> {
		const heroDoc: firestore.DocumentSnapshot = await this.getHeroRef(
			heroId
		).get();

		return Hero.fromDAO(heroId, heroDoc.data());
	}
}
