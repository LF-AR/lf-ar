import { firestore } from "firebase-admin";

let _db: firestore.Firestore;

export function initDb() {
	_db = new firestore.Firestore();
}

export function getDb() {
	if (!_db) {
		initDb();
	}
	return _db;
}
