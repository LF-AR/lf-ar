import { MagicAttack } from "../model/magicAttack";
import { getDb } from "../DAO/db";
import { firestore } from "firebase-admin";

export class MagicAttackDAO {
	static getMagicAttackRef(magicAttackId: string): firestore.DocumentReference {
		return getDb().doc(`Magic_Attack/${magicAttackId}`);
	}

	static async getMagicAttack(magicAttackId: string): Promise<MagicAttack> {
		const magicAttackDoc = await this.getMagicAttackRef(magicAttackId).get();

		return MagicAttack.fromDAO(magicAttackId, magicAttackDoc.data());
	}
}
