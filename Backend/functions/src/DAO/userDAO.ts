import { User, UserHero, Hero } from "../model/model";
import { getDb } from "../DAO/db";
import { firestore } from "firebase-admin";
import admin = require("firebase-admin");

export class UserDAO {
	static getUserRef(userId: string): firestore.DocumentReference {
		return getDb().doc(`User/${userId}`);
	}

	static getHerosCol(userId: string): firestore.CollectionReference {
		return getDb().collection(`User/${userId}/heros`);
	}

	static getUserHeroRef(
		userId: string,
		herosId: string
	): firestore.DocumentReference {
		return getDb().doc(`User/${userId}/heros/${herosId}`);
	}

	static async getUser(userId: string): Promise<User> {
		const userDoc: firestore.DocumentSnapshot = await this.getUserRef(
			userId
		).get();
		const userHeros: firestore.DocumentData[] = (
			await this.getHerosCol(userId).get()
		).docs;

		return User.fromDAO(userId, userDoc.data(), userHeros);
	}

	static async getUserHero(userId: string, herosId: string): Promise<UserHero> {
		const userHero = await this.getUserHeroRef(userId, herosId).get();

		return UserHero.fromDAO(herosId, userHero.data());
	}

	static async createUser(userRecord: admin.auth.UserRecord): Promise<User> {
		try {
			const user = User.newUser(
				userRecord.uid,
				userRecord.displayName ?? userRecord.uid
			);
			const userRef = UserDAO.getUserRef(user.id ?? userRecord.uid);
			const userObj = JSON.parse(JSON.stringify(user));
			delete userObj.heros;
			delete userObj.id;

			await userRef.set(userObj);

			const herosObj: Array<any> = JSON.parse(JSON.stringify(user.heros));
			herosObj.map((obj, i) => (obj.heroRef = user.heros[i].heroRef));
			herosObj.forEach(async (hero) => {
				await userRef.collection("heros").add(hero);
			});

			return user;
		} catch (e) {
			throw new Error(e);
		}
	}

	static async addMoney(userId: string, value: number): Promise<number> {
		let user: User;
		try {
			user = await this.getUser(userId);
			await UserDAO.getUserRef(userId).update({
				money: user.money + value,
			});
		} catch (e) {
			throw new Error(e);
		}

		return user.money + value;
	}

	static async levelUp(user_id: string, heros_id: string): Promise<UserHero> {
		try {
			const userHero: UserHero = await UserDAO.getUserHero(user_id, heros_id);
			const hero = Hero.fromDAO(
				userHero.heroRef.id,
				await userHero.heroRef.get()
			);

			if (userHero.exp < hero.maxExp) {
				throw new Error("Not enough Exp");
			} else if (userHero.level >= hero.maxLevel) {
				throw new Error("Already max level");
			} else {
				await UserDAO.getUserHeroRef(user_id, heros_id).update({
					exp: userHero.exp - hero.maxExp,
					level: userHero.level + 1,
				});
			}

			return userHero;
		} catch (e) {
			throw e;
		}
	}
}
