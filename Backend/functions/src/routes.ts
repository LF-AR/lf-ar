import { Router } from "express";
import {
	ping,
	pingPost,
	levelUp,
	buyHero,
	endGame,
} from "./endPoints/endPoints";
export const router = Router();

router.get("/ping", ping);
router.post("/pingPost", pingPost);
router.post("/endGame", endGame);
router.post("/levelUp", levelUp);
router.post("/buyHero", buyHero);
