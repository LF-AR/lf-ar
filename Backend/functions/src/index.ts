import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

import * as express from "express";
import * as cors from "cors";
import { router } from "./routes";

import { authorizeUser } from "./auth";

import { UserDAO } from "./DAO/DAO";

admin.initializeApp(functions.config().firebase);
const app = express();

app
	.use(
		cors({
			origin: true,
		})
	)
	.use(express.json())
	.use(express.urlencoded({ extended: false }))
	.use((req, res, next) => {
		res.setHeader("Connection", "close");
		next();
	})
	.use(authorizeUser)
	.use("/", router)
	.get("*", (_, res) =>
		res.status(404).json({ success: false, data: "Endpoint not found" })
	);

app.get("*", (req, res) => {
	res.send("Hello World");
});

export const api = functions.https.onRequest(app);

export const initAccount = functions.auth.user().onCreate(async (user) => {
	await UserDAO.createUser(user);
});

export { ping, levelUp, buyHero, endGame } from "./endPoints/endPoints";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
