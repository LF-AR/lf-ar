export class MagicAttack {
	constructor();
	constructor(
		id?: string,
		name?: string,
		asset_name?: string,
		magic_point?: number,
		damage?: number,
		duration?: number,
		attack_effect?: string,
		attack_effect_duration?: string,
		magic_spawn_sound?: number,
		magic_move_sound?: number,
		speed?: number
	);
	constructor(
		public id?: string,
		public name: string = "",
		public asset_name: string = "",
		public magic_point: number = 0,
		public damage: number = 0,
		public duration: number = 0,
		public attack_effect: string = "",
		public attack_effect_duration: string = "",
		public magic_spawn_sound: number = 0,
		public magic_move_sound: number = 0,
		public speed: number = 0
	) {}

	static fromJson(json: any): MagicAttack {
		const magicAttack = JSON.parse(json);
		magicAttack[Symbol.iterator] = function* () {
			for (const i of Object.keys(this)) {
				yield [i, this(i)];
			}
		};
		return new this(...magicAttack);
	}

	static fromDAO(id: string, data: any): MagicAttack {
		return new this(
			id,
			data.name,
			data.asset_name,
			data.magic_point,
			data.damage,
			data.duration,
			data.attack_effect,
			data.attack_effect_duration,
			data.magic_spawn_sound,
			data.magic_move_sound,
			data.speed
		);
	}
}
