import { firestore } from "firebase-admin";

export class Hero {
	constructor();
	constructor(
		id?: string,
		name?: string,
		healthPoint?: number,
		magicPoint?: number,
		magicPointRecoverSpeed?: number,
		moveSpeed?: number,
		attackDamage?: number,
		description?: string,
		levelFactor?: number,
		assetName?: string,
		type?: string,
		maxLevel?: number,
		maxExp?: number,
		price?: number,
		fighterColor?: number,
		magicAttackList?: Array<firestore.DocumentReference>
	);
	constructor(
		public id?: string,
		public name: string = "",
		public healthPoint: number = 100,
		public magicPoint: number = 100,
		public magicPointRecoverSpeed: number = 3,
		public moveSpeed: number = 1,
		public attackDamage: number = 25,
		public description: string = "",
		public levelFactor: number = 5,
		public assetName: string = "",
		public type: string = "",
		public maxLevel: number = 10,
		public maxExp: number = 100,
		public price: number = 100,
		public fighterColor: number = 0,
		public magicAttackList: Array<firestore.DocumentReference> = []
	) {}

	static fromJson(json: any): Hero {
		const hero = JSON.parse(json);
		hero[Symbol.iterator] = function* () {
			for (const i of Object.keys(this)) {
				yield [i, this[i]];
			}
		};
		return new this(...hero);
	}

	static fromDAO(id: string, data: any): Hero {
		return new this(
			id,
			data.name,
			data.health_point,
			data.magic_point,
			data.magic_point_recover_speed,
			data.move_speed,
			data.attack_damage,
			data.description,
			data.level_factor,
			data.asset_name,
			data.type,
			data.max_level,
			data.max_exp,
			data.price,
			data.fighter_color,
			data.magic_attack_list
		);
	}

	toDbObject(): Object {
		return {
			name: this.name,
			health_point: this.healthPoint,
			magic_point: this.magicPoint,
			magic_point_recover_speed: this.magicPointRecoverSpeed,
			move_speed: this.moveSpeed,
			attack_damage: this.attackDamage,
			description: this.description,
			level_factor: this.levelFactor,
			asset_name: this.assetName,
			type: this.type,
			max_level: this.maxLevel,
			max_exp: this.maxExp,
			price: this.price,
			fighter_color: this.fighterColor,
			magic_attack_list: this.magicAttackList,
		};
	}
}
