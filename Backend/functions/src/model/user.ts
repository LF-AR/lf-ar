import { firestore } from "firebase-admin";

import { HeroDAO } from "../DAO/DAO";
import { UserStatus } from "../enum";

export class User {
	constructor();
	constructor(name: string);
	constructor(
		id?: string,
		name?: string,
		score?: number,
		money?: number,
		status?: string,
		heros?: Array<any>
	);
	constructor(
		public id?: string,
		public name: string = "",
		public score: number = 0,
		public money: number = 100,
		public status: string = UserStatus.Init,
		public heros: Array<UserHero> = []
	) {}

	static fromJson(json: any): User {
		const user = JSON.parse(json);
		const heros = user.heros?.map((hero: any) => {
			return {
				heroRef: hero.heroRef,
				level: hero.level,
				exp: hero.exp,
			};
		});

		return new this(
			user.id,
			user.name,
			user.score,
			user.money,
			user.status,
			heros
		);
	}

	static fromDAO(id: string, data: any, heros: Array<any>): User {
		const herosArray: Array<UserHero> = heros.map((hero) => {
			const oneHero = hero.data();
			return new UserHero(hero.id, oneHero.heroRef, oneHero.level, oneHero.exp);
		});
		return new this(
			id,
			data.name,
			data.score,
			data.money,
			data.status,
			herosArray ?? []
		);
	}

	static newUser(id: string, name: string): User {
		const newUser: User = new this(id, name, 0, 1000, UserStatus.Init);
		const userHero: UserHero = new UserHero();
		newUser.heros.push(userHero);
		return newUser;
	}

	toDbObject(): Object {
		return {
			name: this.name,
			score: this.score,
			money: this.money,
			status: this.status,
		};
	}
}

export class UserHero {
	constructor();
	constructor(
		id?: string,
		heroRef?: firestore.DocumentReference,
		level?: number,
		exp?: number
	);
	constructor(
		public id: string = "",
		public heroRef: firestore.DocumentReference = HeroDAO.getHeroRef(
			"D4zZYRtDbxJTegzxEA00"
		),
		public level: number = 1,
		public exp: number = 0
	) {}

	static fromDAO(id: string, data: any): UserHero {
		return new this(id, data.heroRef, data.level, data.exp);
	}

	toDbObject(): Object {
		return {
			heroRef: this.heroRef,
			level: this.level,
			exp: this.exp,
		};
	}
}
