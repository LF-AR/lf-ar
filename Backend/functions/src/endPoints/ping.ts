import { UserDAO } from "../DAO/DAO";
import { User } from "../model/model";

/**
 * @api	{get}	ping	Request User object
 * @apiName	ping
 * @apiGroup	Ping
 *
 * @apiSuccess	{String}	success	Api status
 * @apiSuccess	(User)	user	User object
 * @apiSuccessExample	Success - Response:
 * HTTP / 1.1 200 OK
 * {
 * 	"success": true,
 * 	"user": {
 * 		"id": "8fftXJVBXZWrWSdq7fatkEMtUzy2",
 * 		"name": "thomas",
 * 		"score": 10000,
 * 		"money": 1000,
 * 		"status": "idle",
 * 		"heros": [
 * 			{
 * 				"id": "OY3P3QnhOc4pkavCzADH",
 * 				"heroRef": <heroRef>,
 * 				"level": 1,
 * 				"exp": 0
 * 			}
 * 		]
 * 	}
 * }
 *
 * @apiError	(Internal Server Error 500)	(String)	success	Api status
 * @apiError	(Internal Server Error 500)	(String)	error	Error description
 *
 * @apiErrorExample	Error-Response:
 * 	HTTP/1.1 500 Internal Server Error
 * 	{
 * 		"success": false,
 * 		"error": <error string>
 * 	}
 * */

export async function ping(req: any, res: any) {
	console.log("API calling ping");

	let user: User;

	try {
		user = await UserDAO.getUser(req.headers.user_id);

		return res.json({
			success: true,
			user,
		});
	} catch (e) {
		return res.json({
			success: false,
			error: (<Error>e).message,
		});
	}
}
