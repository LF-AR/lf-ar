import { UserDAO, HeroDAO } from "../DAO/DAO";
import { User, Hero } from "../model/model";
import { UserHero } from "../model/user";
import { UserStatus } from "../enum";

/**
 * @api	{post}	buyHero/	Add a hero to a User
 * @apiName	buyHero
 * @apiGroup	BuyHero
 *
 * @apiParam	{String}	hero_id	Hero_id of the adding Hero
 *
 * @apiParamExample	{JSON}	Request-Example:
 * {
 * 	"hero_id": "kw4drffy9o9EjDJBMCST"
 * }
 *
 * @apiSuccess	{String}	success	Api status
 * @apiSuccessExample	Success - Response:
 * HTTP / 1.1 200 OK
 * {
 * 	"success": true
 * }
 *
 * @apiError	(Internal Server Error 500)	{String}	success	Api status
 * @apiError	(Internal Server Error 500)	{String}	error	Error description
 *
 * @apiErrorExample	Error-Response:
 * 	HTTP/1.1 500 Internal Server Error
 * 	{
 * 		"success": false,
 * 		"error": <error string>
 * 	}
 * */

export async function buyHero(req: any, res: any) {
	console.log("API calling buyHero");

	try {
		const user_id = req.headers.user_id;
		const hero_id = req.body.hero_id;

		const user: User = await UserDAO.getUser(user_id);
		const hero: Hero = await HeroDAO.getHero(hero_id);

		if (user.money < hero.price) {
			throw new Error("Not enough money");
		}

		const userHero = new UserHero("", HeroDAO.getHeroRef(hero_id));

		await UserDAO.getHerosCol(user_id).add(userHero.toDbObject());
		await UserDAO.getUserRef(user_id).update({
			money: user.money - hero.price,
			status: UserStatus.BuyHero,
		});

		return res.json({
			success: true,
		});
	} catch (e) {
		return res.json({
			success: false,
			error: (<Error>e).message,
		});
	}
}
