import { UserDAO } from "../DAO/DAO";
import { UserStatus } from "../enum";

/**
 * @api	{post}	levelUp/	Level up a User's Hero
 * @apiName	levelUp
 * @apiGroup	LevelUp
 *
 * @apiParam	{String}	heros_id The firebase document id of the UserHero
 *
 * @apiParamExample	{JSON}	Request-Example:
 * {
 * 	"heros_id": "XTcBchxiHs5S3nkuox7r"
 * }
 *
 * @apiSuccess	{String}	success	Api status
 * @apiSuccessExample	Success - Response:
 * HTTP / 1.1 200 OK
 * {
 * 	"success": true
 * }
 *
 * @apiError	(Internal Server Error 500)	{String}	success	Api status
 * @apiError	(Internal Server Error 500)	{String}	error	Error description
 *
 * @apiErrorExample	Error-Response:
 * 	HTTP/1.1 500 Internal Server Error
 * 	{
 * 		"success": false,
 * 		"error": <error string>
 * 	}
 * */

export async function levelUp(req: any, res: any) {
	console.log("API calling levelUp");

	try {
		const user_id = req.headers.user_id;

		await UserDAO.levelUp(user_id, req.body.heros_id);

		await UserDAO.getUserRef(user_id).update({
			status: UserStatus.LevelUp,
		});

		return res.json({
			success: true,
		});
	} catch (e) {
		return res.json({
			success: false,
			error: (<Error>e).message,
		});
	}
}
