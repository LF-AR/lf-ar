import { UserDAO, HeroDAO } from "../DAO/DAO";
import { User, UserHero } from "../model/model";
import { PunTeam, BattleMode, UserStatus } from "../enum";

/**
 * @api	{post}	endGame/	Update user data according to the result of a finished game
 * @apiName	endGame
 * @apiGroup	EndGame
 *
 * @apiParam	{BattleMode}	battle_mode	Battle Mode of this game (Free/Team)
 * @apiParam	{Player[]}	players	Array of participated Players
 * @apiParam	{string}	player.user_id	The userId of this Player
 * @apiParam	{string}	player.hero_id	The heroId of the hero used by the Player in this game
 * @apiParam	{number}	player.health_point	The HP left of this Player's Fighter
 * @apiParam	{number}	player.magic_point	The MP left of this Player's Fighter
 * @apiParam	{PunTeams.Team}	player.team	The Player's team (none/blue/red)
 *
 * @apiParamExample	{JSON}	Request-Example:
 * {
 * 	"battle_mode": 1,
 * 	"players": [
 * 		{
 * 			"user_id": "8fftXJVBXZWrWSdq7fatkEMtUzy2",
 * 			"hero_id": "kw4drffy9o9EjDJBMCST",
 * 			"health_point": 50,
 * 			"magic_point": 20,
 * 			"team": 1
 * 		},
 * 		{
 * 			"user_id": "rdJtVhBFPHUGvcY6tsLp6mv7ziO2",
 * 			"hero_id": "D4zZYRtDbxJTegzxEA00",
 * 			"health_point": 20,
 * 			"magic_point": 80,
 * 			"team": 2
 * 		}
 * 	]
 * }
 *
 * @apiSuccess	{String}	success	Api status
 *
 * @apiSuccessExample	Success - Response:
 * HTTP / 1.1 200 OK
 * {
 * 	"success": true,
 * 	"winnerIncreaseScoreAmount": 10
 * }
 *
 * @apiError	(Internal Server Error 500)	{String}	success	Api status
 * @apiError	(Internal Server Error 500)	{String}	error	Error description
 *
 * @apiErrorExample	Error-Response:
 * 	HTTP/1.1 500 Internal Server Error
 * 	{
 * 		"success": false,
 * 		"error": <error string>
 * 	}
 * */

// type

interface Player {
	user_id: string;
	hero_id: string;
	health_point: number;
	magic_point: number;
	team: PunTeam;
}

interface PlayerRef {
	user_id: string;
	user: User;
	userHero: UserHero;
	health_point: number;
	magic_point: number;
	team: PunTeam;
}

// type (end)

// variable

const winnerIncreaseScoreAmount: number = 10;
let playerRefs: Array<PlayerRef>;
let bestFreePlayerRefs: Array<PlayerRef>;
let bestTeam: PunTeam;

// variable (end)

// function

async function GenerateResult(battleMode: BattleMode) {
	switch (battleMode) {
		case BattleMode.Free: {
			GenerateBestFreePlayers();

			await Promise.all(
				playerRefs.map(async (playerRef) => {
					// REMARKS: free mode can only have 1 winner
					if (bestFreePlayerRefs.length === 1) {
						if (bestFreePlayerRefs.includes(playerRef)) {
							await SetVictory(playerRef);
						} else {
							await SetLose(playerRef);
						}
					} else {
						if (bestFreePlayerRefs.includes(playerRef)) {
							await SetDraw(playerRef);
						} else {
							await SetLose(playerRef);
						}
					}
				})
			);

			break;
		}
		case BattleMode.Team: {
			GenerateBestTeam();

			await Promise.all(
				playerRefs.map(async (playerRef) => {
					if (playerRef.team === bestTeam) {
						await SetVictory(playerRef);
					} else if (bestTeam === PunTeam.none) {
						await SetDraw(playerRef);
					} else {
						await SetLose(playerRef);
					}
				})
			);

			break;
		}
	}
}

function GenerateBestFreePlayers() {
	// REMARKS: find the highest HP
	const hightestHP = Math.max(
		...playerRefs.map((playerRef) => playerRef.health_point)
	);

	// REMARKS: find out who has the highest HP
	bestFreePlayerRefs = playerRefs.filter(
		(playerRef) => playerRef.health_point === hightestHP
	);
}

function GenerateBestTeam() {
	const HPBlue = GenerateTeamHP(PunTeam.blue);
	const HPRed = GenerateTeamHP(PunTeam.red);

	if (HPBlue > HPRed) {
		bestTeam = PunTeam.blue;
	} else if (HPRed > HPBlue) {
		bestTeam = PunTeam.red;
	} else {
		bestTeam = PunTeam.none;
	}
}

function GenerateTeamHP(team: PunTeam): number {
	return playerRefs
		.filter((playerRef) => playerRef.team === team)
		.map((playerRef) => playerRef.health_point)
		.reduce((total, e) => total + e);
}

async function SetVictory(playerRef: PlayerRef) {
	playerRef.user.money += winnerIncreaseScoreAmount;
	playerRef.user.score += winnerIncreaseScoreAmount;
	await SetExp(playerRef, winnerIncreaseScoreAmount);
}

async function SetDraw(playerRef: PlayerRef) {
	playerRef.user.money += winnerIncreaseScoreAmount / 2;
	// REMARKS: draw does not affect score
	await SetExp(playerRef, winnerIncreaseScoreAmount / 2);
}

async function SetLose(playerRef: PlayerRef) {
	playerRef.user.money += winnerIncreaseScoreAmount / 2;
	playerRef.user.score -= winnerIncreaseScoreAmount;
	await SetExp(playerRef, winnerIncreaseScoreAmount / 2);
}

async function SetExp(playerRef: PlayerRef, increaseAmount: number) {
	const hero = await HeroDAO.getHero(playerRef.userHero.heroRef.id);
	const heroMaxExp = hero.maxExp * hero.maxLevel;
	const currentUserHeroExp =
		playerRef.userHero.exp + hero.maxExp * (playerRef.userHero.level - 1);

	let expIncreaseAmount = increaseAmount;
	let moneyIncreaseAmount = 0;

	// REMARKS:
	// if max exp, transfer the increase amount to user's money
	if (currentUserHeroExp + increaseAmount > heroMaxExp) {
		moneyIncreaseAmount = currentUserHeroExp + increaseAmount - heroMaxExp;
		expIncreaseAmount = increaseAmount - moneyIncreaseAmount;
	}

	playerRef.userHero.exp += expIncreaseAmount;
	playerRef.user.money += moneyIncreaseAmount;
}

async function UpdateDBWithResult() {
	await Promise.all(
		playerRefs.map(async (playerRef) => {
			await UserDAO.getUserHeroRef(
				playerRef.user_id,
				playerRef.userHero.id
			).update({
				exp: playerRef.userHero.exp,
			});

			await UserDAO.getUserRef(playerRef.user_id).update({
				money: playerRef.user.money,
				score: playerRef.user.score,
				status: UserStatus.EndGame,
			});
		})
	);
}

// function (end)

export async function endGame(req: any, res: any) {
	console.log("API calling endGame");

	try {
		const players: Player[] = req.body.players;
		const reqSenderUserId: string = req.headers.user_id;

		if (!players.map((player) => player.user_id).includes(reqSenderUserId)) {
			throw new Error(
				"User (" +
					reqSenderUserId +
					", who sent the endGame request) is not in the list"
			);
		}

		const playerRefsPromises = players.map(async (player: Player) => {
			const user = await UserDAO.getUser(player.user_id);

			const userHero = user.heros.find(
				(uh) => uh.heroRef.id === player.hero_id
			);

			if (userHero === undefined) {
				throw new Error("User do not have that Hero in database :(");
			}

			const playerRef: PlayerRef = {
				user_id: player.user_id,
				user: user,
				userHero: userHero,
				health_point: player.health_point,
				magic_point: player.magic_point,
				team: player.team,
			};

			return playerRef;
		});
		playerRefs = await Promise.all(playerRefsPromises);

		if (
			playerRefs.every(
				(playerRef) => playerRef.user.status === UserStatus.InGame
			)
		) {
			await GenerateResult(req.body.battle_mode);

			await UpdateDBWithResult();
		} else {
			throw new Error("Not all players are in-game :(");
		}

		return res.json({
			success: true,
			winnerIncreaseScoreAmount,
		});
	} catch (e) {
		console.log(e);

		return res.json({
			success: false,
			error: (<Error>e).message,
		});
	}
}
